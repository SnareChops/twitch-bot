# Twitch Bot

## Prerequisites

1. [Node](https://nodejs.org)
1. [Docker](https://docker.io)

## Dev instructions

Create a `.env` file at the root of the project with the following contents. Replace these values with your own.

```
BOT_USERNAME=YourUsername
AUTH_TOKEN=YourAuthToken
DB_ADDRESS=db
DB_USER=root
DB_PWORD=TheRootPassword
CROSSBAR_SECRET=Fhdi8qrdzptuMg3HWlfH2ld58AYE3jUeYJuRFGDPAvA8DvZF5rt2bGycxctn6ciGXNwn90
FIGHT_COOLDOWN=60
```
