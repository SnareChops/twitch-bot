# SnareChopsBot

A twitch chat bot now available for VERY EARLY beta testing to streamers with less than 50 average viewers.

## Features

SnareChopsBot is still very new and many unique and interesting features are planned, but everything has to start somewhere. More detailed information for features included further down.

Features included in this beta:

- Chat commands that your viewers can use to answer commonly asked questions or other information
- A surprisingly popular !fight chat game for your viewers to settle arguments or have fun with other viewers (including win/loss stats!)
- Cooldowns for chat commands to prevent bot spam from viewers
- Get or set usage information or descriptions for any chat command command
- A simple random number generator for any use (!roll)
- Many more features to come!

## Bot Name

Do you have any name suggestions for this bot? If so we'd love to hear them! [Stop by the stream](https://twitch.tv/snarechops), [log a new issue](https://gitlab.com/SnareChops/twitch-bot/issues/new), or whisper / [discord](https://discord.gg/r7Ee5yM) any name suggestions!

## Beta Signup

If you would like to signup for this limited beta [please fill out the form](). Due limited servers and bot stability only a select few channels will be accepted, though more will be added as time goes on. If you are not in the initial group and more spots become available you will be contacted with an offer through twitch whispers.

## Chat Commands

Chat commands can be added, edited, enabled, disabled, and removed by moderators or the broadcaster.

| Intention                  | Usage                                |
| -------------------------- | ------------------------------------ |
| Add or edit a chat command | !add !<command name> <response text> |
| Disable a chat command     | !disable !<command name>             |
| Enable a chat command      | !enable !<command name>              |
| Delete a chat command      | !del !<command name>                 |

Chat commands can also have cooldowns for spam prevention

```
!cooldown !<command name> <time in seconds>
```

Chat commands can also have descriptions

To view a command description or usage information use a `?` instead of `!` when running the command. To add or edit a command description use `?<command name> <description>`

## FIGHT!

!fight is a simple duel game that is included and has proven to be rather popular in initial testing. Viewers can challenge others to a duel, should the challenged !accept the fight a 50/50 coin toss is used to decide the winner.

```
!fight <viewer>
```

Viewers can see their win/loss history using `!fightstats`

> This feature has proven to be very popular with users, and has a tendency to get spammed. A default cooldown of 1 min has been added to help keep this somewhat under control, however it can still be somewhat unruly. Future versions of this bot will include the ability to change the cooldown or disable this feature altogether.

## !roll

Roll a random number between 1 and 10 using `!roll`, or choose your own maximum (example: `!roll 100`). A funny usage of this emerges for example `!roll my chances to win this round` can provide for some funny chat interactions

## Future features

Future features planned and in active development include:

- Spam protection
- Link scanning and protection
- UI for changing bot settings / commands
- Moderator chat
- And many more that are too secret to reveal at the moment!

## Disclaimer

So here's the fine print... The bot is still very new, and does have bugs and issues. By signing up for the beta program you agree to these issues and any others that may arrise. Bugfixes and patches can happen at any time during the beta period, with or without warning. There may also be extended periods of downtime again with or without warning. You may unenroll from the beta program at any time and request that it be removed from your channel. Note that this is a manual process and may take up to 24 hours to complete. You must also agree to submit any bugs or issues found within 24 hours. At the descretion of the maintainer the bot may be removed from your channel at any time, for any length of time.
