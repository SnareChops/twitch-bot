const path = require('path');

module.exports = {
  mode: 'production',

  entry: './dist/client/index.js',

  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  externals: ['autobahn']
};
