#!/usr/bin/env node
const process = require('process');
const fs = require('fs');
const path = require('path');

fs.readFile(path.join(__dirname, 'client/config.ts'), (err, configData) => {
  if (err) {
    throw err;
  }
  fs.readFile(path.join(__dirname, '.env'), (err, envData) => {
    if (err) {
      throw err;
    }

    const env = envData
      .toString()
      .split('\n')
      .reduce((result, line) => {
        const parts = line.split('=');
        result[parts[0]] = parts[1];
        return result;
      }, {});
    const config = configData
      .toString()
      .replace('%TWITCH_CLIENT_ID%', env['TWITCH_CLIENT_ID'])
      .replace('%TWITCH_REDIRECT_URI%', env['TWITCH_REDIRECT_URI']);
    fs.writeFile(path.join(__dirname, 'client/config.ts'), config, err => {
      if (err) {
        throw err;
      }
    });
  });
});
