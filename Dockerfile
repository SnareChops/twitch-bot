#-----------------#
#    Bot Build    #
#-----------------#
FROM registry.gitlab.com/snarechops/twitch-bot/base:latest as bot
COPY . /project/
WORKDIR /project
RUN rm -rf node_modules && npm install && npm run build
CMD ["npm", "start"]

#------#
# Test #
#------#
FROM bot as test
RUN npm test

#-----------------------#
#      Site Build       #
#-----------------------#
FROM pierrezemb/gostatic as site
COPY --from=bot /project/public/ /srv/http/
ENTRYPOINT ["/goStatic"]
