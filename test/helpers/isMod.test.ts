import * as assert from 'assert';
import { isMod } from '../../src/helpers/isMod';
import { Userstate } from 'tmi.js';
import { Connection } from 'typeorm';
import { container } from '../../src/container';
import { DB } from '../../src/di.tokens';
import { Mod } from '../../src/entities/Mod';

describe('isMod', () => {
  let db: Connection;
  beforeEach(() => {
    db = ({ manager: {} } as unknown) as Connection;
    container.bind(DB).toConstantValue(db);
  });

  afterEach(() => container.unbindAll());

  it('should return true if user has .mod = true', async () => {
    const result = await isMod('TestChannel', { mod: true } as Userstate);
    assert(result, 'should be true');
  });

  it('should return true if user has broadcaster badge', async () => {
    const result = await isMod('TestChannel', { badges: { broadcaster: '1' } } as Userstate);
    assert(result, 'should be true');
  });

  it('should return true if db has username in mod table', async () => {
    db.manager.findOne = (async (table: any, condition: any) => {
      assert.strictEqual(table, Mod, 'should be the mod table');
      assert.deepStrictEqual(condition, { channel: 'TestChannel', username: 'TestUser' }, 'should be the username');
      return {};
    }) as any;

    const result = await isMod('TestChannel', { badges: {}, 'display-name': 'TestUser' } as Userstate);
    assert(result, 'should be true');
  });

  it('should return false if none of the condition pass', async () => {
    db.manager.findOne = (async (table: any, condition: any): Promise<any> => {
      assert.strictEqual(table, Mod, 'should be the mod table');
      assert.deepStrictEqual(condition, { channel: 'TestChannel', username: 'TestUser' }, 'should be the username');
      return void 0;
    }) as any;

    const result = isMod('TestChannel', { badges: {}, 'display-name': 'TestUser' } as Userstate);
    assert(result, 'should be true');
  });
});
