import * as assert from 'assert';
import { Client, clientFrom } from '../src/Client';
import { client as _client } from 'tmi.js';
import { Connection } from 'typeorm';
import { CommandRunner } from '../src/features/CommandRunner';
import { DescriptionRunner } from '../src/features/DescriptionRunner';
import { MathRunner } from '../src/features/MathRunner';
import { SuperCommandRunner } from '../src/features/SuperCommandRunner';
import { MockClient } from './_fixtures/MockClient';
import { UrlScanRunner } from '../src/features/UrlScanRunner';
import { ScheduleRunner } from '../src/features/ScheduleRunner';

describe('Client', () => {
  let db: Connection;
  let commandRunner: CommandRunner;
  let descriptionRunner: DescriptionRunner;
  let mathRunner: MathRunner;
  let superRunner: SuperCommandRunner;
  let urlScanRunner: UrlScanRunner;
  let scheduleRunner: ScheduleRunner;
  let trigger: (event: string, ...args: any[]) => void;
  let instance: Client;

  beforeEach(() => {
    db = { manager: {} } as Connection;
    commandRunner = {} as CommandRunner;
    descriptionRunner = {} as DescriptionRunner;
    mathRunner = {} as MathRunner;
    superRunner = {} as SuperCommandRunner;
    urlScanRunner = {} as UrlScanRunner;
    scheduleRunner = {} as ScheduleRunner;
    instance = new (clientFrom(MockClient))(
      'botuser',
      'bot-password',
      ['#channel', '#other'],
      db,
      commandRunner,
      descriptionRunner,
      mathRunner,
      superRunner,
      urlScanRunner,
      scheduleRunner
    );
    trigger = (event, ...args) => instance['trigger'](event, ...args);
  });

  it('should parse on message received', async () => {
    let parsed = false;
    instance.parse = async (target, context, message, self) => {
      assert.strictEqual(target, '#channel', 'should be the channel');
      assert.deepStrictEqual(context, { username: 'testuser' }, 'should be the context');
      assert.strictEqual(message, 'test-message', 'should be the message');
      assert.strictEqual(self, false, 'should be self param');
      parsed = true;
    };

    let startedSchedule = false;
    scheduleRunner.start = async () => {
      startedSchedule = true;
    };

    instance.connect = async () => ['', 1];
    instance.mods = async () => [] as any;

    await instance.init();
    trigger('message', '#channel', { username: 'testuser' }, 'test-message', false);
    assert(parsed);
    assert(startedSchedule);
  });

  it('should save mod on mod received', async () => {
    let saved = false;
    instance.saveMod = async (channel, username, add) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(username, 'moduser', 'should be the username');
      assert(add, 'should be adding');
      saved = true;
    };

    let startedSchedule = false;
    scheduleRunner.start = async () => {
      startedSchedule = true;
    };

    instance.connect = async () => ['', 1];
    instance.mods = async () => [] as any;

    await instance.init();
    trigger('mod', '#channel', 'moduser');
    assert(saved);
    assert(startedSchedule);
  });

  it('should save mod on unmod received', async () => {
    let saved = false;
    instance.saveMod = async (channel, username, add) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(username, 'moduser', 'should be the username');
      assert(!add, 'should be removing');
      saved = true;
    };

    let startedSchedule = false;
    scheduleRunner.start = async () => {
      startedSchedule = true;
    };

    instance.connect = async () => ['', 1];
    instance.mods = async () => [] as any;

    await instance.init();
    trigger('unmod', '#channel', 'moduser');
    assert(saved);
    assert(startedSchedule);
  });

  // it('should scan for mod for each channel on connect', async () => {
  //   let saved = 0;
  //   instance.saveMod = async (channel, username, add) => {
  //     assert.strictEqual(channel, '#channel', 'should be the channel');
  //     assert(/moduser\d/i.test(username), 'should be the username');
  //     assert(add, 'should be adding');
  //     saved++;
  //   };

  //   instance.connect = async () => ['', 1];

  //   let fetched = 0;
  //   instance.mods = async channel => {
  //     if (fetched === 0) {
  //       assert.strictEqual(channel, '#channel', 'should be the channel');
  //     } else {
  //       assert.strictEqual(channel, '#other', 'should be the other channel');
  //     }
  //     fetched++;
  //     return ['moduser1', 'moduser2'] as any;
  //   };

  //   instance.init();
  //   trigger('mod', '#channel', 'moduser');
  //   trigger('mod', '#other', 'moduser');
  //   await new Promise(resolve => setTimeout(resolve, 5));
  //   assert.strictEqual(saved, 4);
  //   assert.strictEqual(fetched, 2);
  // });
});
