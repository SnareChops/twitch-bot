import * as assert from 'assert';
import { container } from '../../src/container';
import { Command } from '../../src/entities';
import { DescriptionRunner } from '../../src/features/DescriptionRunner';
import { VisibleBuiltIn } from '../_fixtures/VisibleBuiltIn';

describe('DescriptionRunner', () => {
  let instance: DescriptionRunner;

  beforeEach(() => {
    instance = new DescriptionRunner();
  });

  afterEach(() => {
    container.unbindAll();
  });

  it('should detect a string with a command description in the first position', () => {
    const message = '?test some args';
    const result = instance.detect(message);
    assert.strictEqual(result[0], 'test', 'should be the command name');
    assert.deepStrictEqual(result[1], ['some', 'args'], 'should be the command args');
  });

  it('should not detect a command description in any position', () => {
    const message = 'some words ?test with args';
    const result = instance.detect(message);
    assert.strictEqual(result, void 0, 'should not have found command description');
  });

  it('should not detect a command in a string without one', () => {
    const message = 'some words but no command definition';
    const result = instance.detect(message);
    assert.strictEqual(result, void 0, 'should not have found a command');
  });

  it('should run a restricted command description if executed by a mod', async () => {
    instance.isMod = async () => true;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      const result = new Command('TestChannel', 'test', 'test-response', 'ModUser');
      result.restricted = true;
      result.description = 'test-description';
      return result;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel name');
      assert.strictEqual(message, 'TestUser, test-description', 'should be the response');
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { 'display-name': 'TestUser', mod: true } as any, 'test', void 0);
    assert(found);
    assert(responded);
  });

  it('should run a non-restricted command description if executed by a mod', async () => {
    instance.isMod = async () => true;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      const result = new Command('TestChannel', 'test', 'test-response', 'ModUser');
      result.restricted = false;
      result.description = 'test-description';
      return result;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel name');
      assert.strictEqual(message, 'TestUser, test-description', 'should be the response');
      responded = true;
      return [message];
    };

    await instance.run('TestChannel', { 'display-name': 'TestUser', mod: true } as any, 'test', void 0);
    assert(found);
    assert(responded);
  });

  it('should run a non-restricted command if executed by a non-mod', async () => {
    instance.isMod = async () => false;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      const result = new Command('TestChannel', 'test', 'test-response', 'ModUser');
      result.restricted = false;
      result.description = 'test-description';
      return result;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel name');
      assert.strictEqual(message, 'TestUser, test-description', 'should be the response');
      responded = true;
      return [message];
    };

    await instance.run('TestChannel', { badges: {}, 'display-name': 'TestUser', mod: false } as any, 'test', void 0);
    assert(found);
    assert(responded);
  });

  it('should decline a restricted command description if executed by a non-mod', async () => {
    instance.isMod = async () => false;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      const result = new Command('TestChannel', 'test', 'test-response', 'ModUser');
      result.restricted = true;
      result.description = 'test-description';
      return result;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { badges: {}, 'display-name': 'TestUser', mod: false } as any, 'test', void 0);
    assert(found);
    assert(!responded);
  });

  it('Mods should be able to edit the description of a db command', async () => {
    instance.isMod = async () => true;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the correct command name');
      found = true;
      return new Command('TestChannel', 'test', 'test-value', 'Anyone');
    };

    let saved = false;
    instance.save = async (...entities: any[]) => {
      assert.strictEqual(entities.length, 1, 'should have only one');
      assert.strictEqual(entities[0].description, 'updated-description', 'should be the description');
      saved = true;
      return entities;
    };

    let responded = false;
    instance.say = async (target, message) => {
      assert.strictEqual(target, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, '!test description updated.');
      responded = true;
      return [message];
    };

    await instance.run('TestChannel', { 'display-name': 'TestUser', mod: true } as any, 'test', [
      'updated-description'
    ]);
    assert(found);
    assert(saved);
    assert(responded);
  });

  it('Non-Mods should NOT be able to edit the description of a db command', async () => {
    instance.isMod = async () => false;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the correct command name');
      found = true;
      return new Command('TestChannel', 'test', 'test-value', 'Anyone');
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser, You are not allowed to change command descriptions.');
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { badges: {}, 'display-name': 'TestUser', mod: false } as any, 'test', [
      'updated-description'
    ]);
    assert(responded);
  });

  it('Should NOT be able to edit the description of a built-in command', async () => {
    instance.isMod = async () => true;

    instance.findCommand = async () => new VisibleBuiltIn();

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser, built-in command descriptions cannot be edited.');
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { 'display-name': 'TestUser', mod: true } as any, 'test', [
      'updated-description'
    ]);
    assert(responded);
  });

  it('should NOT get the description of non existent command', async () => {
    instance.isMod = async () => false;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the correct command name');
      found = true;
      return void 0;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser, !test does not exist.');
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { badges: {}, 'display-name': 'TestUser', mod: false } as any, 'test', void 0);
    assert(found);
    assert(responded);
  });
});
