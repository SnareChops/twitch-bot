import * as assert from 'assert';
import { ScheduleRunner } from '../../src/features/ScheduleRunner';
import { Command } from '../../src/entities';
import { DateTime } from 'luxon';

describe('ScheduleRunner', () => {
  let instance: ScheduleRunner;

  beforeEach(() => {
    instance = new ScheduleRunner();
  });

  it('should load scheduled commands', async () => {
    let found = false;
    instance.find = (async (table: any, options: any) => {
      assert.strictEqual(table, Command, 'should be the command table');
      assert.deepStrictEqual(options, { where: { scheduled: true } }, 'should be the options');
      found = true;
      const command = new Command('#channel', 'scheduled', 'test-response', 'moduser');
      command.scheduled = true;
      return [command];
    }) as any;

    await instance.start();
    assert(found);
    assert.strictEqual(instance.schedule.size, 1, 'should have loaded the command');
    instance.stop();
  });

  it('should run scheduled commands', function(done) {
    let found = false;
    instance.find = (async (table: any, options: any) => {
      assert.strictEqual(table, Command, 'should be the command table');
      assert.deepStrictEqual(options, { where: { scheduled: true } }, 'should be the options');
      found = true;
      const command = new Command('#channel', 'scheduled', 'test-response', 'moduser');
      command.scheduled = true;
      command.schedule = 1 / 60;
      return [command];
    }) as any;

    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, 'test-response', 'should be the message');
      instance.stop();
      done();
      return [channel];
    };

    instance.time = 250;
    instance.start().then(() => assert(found));
  });

  it('should add a scheduled command', async () => {
    const command = new Command('#channel', 'test', 'test-response', 'moduser');

    instance.add(command);

    let found = false;
    for (const [key, value] of instance.schedule.entries()) {
      if (value.name === command.name) {
        found = true;
        break;
      }
    }
    assert(found);
  });

  it('should remove a scheduled command', async () => {
    const command = new Command('#channel', 'test', 'test-response', 'moduser');

    instance.schedule.set(DateTime.local(), command);

    instance.remove('test');

    let found = false;
    for (const [key, value] of instance.schedule.entries()) {
      if (value.name === command.name) {
        found = true;
        break;
      }
    }
    assert(!found);
  });
});
