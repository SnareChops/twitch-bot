import * as assert from 'assert';
import { CommandRunner } from '../../src/features/CommandRunner';
import { Command } from '../../src/entities';
import { DateTime } from 'luxon';
import { VisibleBuiltIn } from '../_fixtures/VisibleBuiltIn';

describe('CommandRunner', () => {
  let instance: CommandRunner;

  beforeEach(() => {
    instance = new CommandRunner();
  });

  it('should detect a string with a command in the first position', () => {
    const message = '!test some args';
    const result = instance.detect(message);
    assert.strictEqual(result[0], 'test', 'should be the command name');
    assert.deepStrictEqual(result[1], ['some', 'args'], 'should be the command args');
  });

  it('should not detect a command in any position', () => {
    const message = 'some words !test with args';
    const result = instance.detect(message);
    assert.strictEqual(result, void 0, 'should not have found command');
  });

  it('should not detect a command in a string without one', () => {
    const message = 'some words but no command';
    const result = instance.detect(message);
    assert.strictEqual(result, void 0, 'should not have found a command');
  });

  it('should run a restricted command if executed by a mod', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel name');
      assert.strictEqual(message, 'test-response', 'should be the response');
      responded = true;
      return [message];
    };

    instance.isMod = async () => true;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      const result = new Command('TestChannel', 'test', 'test-response', 'ModUser');
      result.restricted = true;
      return result;
    };

    await instance.run('TestChannel', { 'display-name': 'TestUser', mod: true } as any, 'test', void 0);
    assert(responded);
    assert(found);
  });

  it('should run a non-restricted command if executed by a mod', async () => {
    instance.isMod = async () => true;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      const result = new Command('TestChannel', 'test', 'test-response', 'ModUser');
      result.restricted = false;
      return result;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel name');
      assert.strictEqual(message, 'test-response', 'should be the response');
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { 'display-name': 'TestUser', mod: true } as any, 'test', void 0);
    assert(found);
    assert(responded);
  });

  it('should run a non-restricted command if executed by a non-mod', async () => {
    instance.isMod = async () => false;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      const result = new Command('TestChannel', 'test', 'test-response', 'ModUser');
      result.restricted = false;
      return result;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel name');
      assert.strictEqual(message, 'test-response', 'should be the response');
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { badges: {}, 'display-name': 'TestUser', mod: false } as any, 'test', void 0);
    assert(found);
    assert(responded);
  });

  it('should decline a restricted command if executed by a non-mod', async () => {
    instance.isMod = async () => false;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      const result = new Command('TestChannel', 'test', 'test-response', 'ModUser');
      result.restricted = true;
      return result;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel name');
      assert.strictEqual(
        message,
        'TestUser, you do not have permissions to run that command.',
        'should be the response'
      );
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { badges: {}, 'display-name': 'TestUser', mod: false } as any, 'test', void 0);
    assert(found);
    assert(responded);
  });

  it('should do nothing for a disabled command', async () => {
    instance.isMod = async () => true;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      const result = new Command('TestChannel', 'test', 'test-response', 'ModUser');
      result.restricted = false;
      result.enabled = false;
      return result;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { 'display-name': 'TestUser', mod: true } as any, 'test', void 0);
    assert(found);
    assert(!responded);
  });

  it('should respond with message if command is on cooldown', async () => {
    instance.isMod = async () => false;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      const result = new Command('TestChannel', 'test', 'test-response', 'ModUser');
      result.restricted = false;
      return result;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel name');
      assert.strictEqual(message, 'TestUser, command is on cooldown.', 'should be the response');
      responded = true;
      return [channel];
    };

    instance.cooldowns.set('test', DateTime.local().plus({ seconds: 5 }));

    await instance.run('TestChannel', { badges: {}, 'display-name': 'TestUser', mod: false } as any, 'test', void 0);
    assert(found);
    assert(responded);
  });

  it('should do nothing for a disabled command', async () => {
    instance.isMod = async () => true;

    const builtIn = new VisibleBuiltIn();

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      return builtIn;
    };

    let invoked = false;
    builtIn.invoke = async () => (invoked = true);

    await instance.run('TestChannel', { 'display-name': 'TestUser', mod: true } as any, 'test', void 0);
    assert(found);
    assert(invoked);
  });
});
