import * as assert from 'assert';
import { UrlScanRunner } from '../../src/features/UrlScanRunner';
import { UrlScan } from '../../src/entities/UrlScan';
import { Userstate } from 'tmi.js';
import { PermissionLevel } from '../../src/types/PermissionLevel';
import { SubUser, TestUser } from '../_fixtures/users';

describe('UrlScanRunner', () => {
  let instance: UrlScanRunner;

  beforeEach(() => {
    instance = new UrlScanRunner();
  });

  it('should detect a url in a message', async () => {
    const result = instance.detect('This is a message with a https://test.com');
    assert.deepStrictEqual(result.get('https://test.com'), { domain: 'test.com', path: void 0 });
  });

  it('should detect a multiple urls in a message', async () => {
    const result = instance.detect('This is a message with a https://test.com and then google.com');
    assert.deepStrictEqual(result.get('https://test.com'), { domain: 'test.com', path: void 0 });
    assert.deepStrictEqual(result.get('google.com'), { domain: 'google.com', path: void 0 });
  });

  it('should store the path for a url', () => {
    const result = instance.detect('This is a message with a google.com/some/path');
    assert.deepStrictEqual(result.get('google.com/some/path'), { domain: 'google.com', path: '/some/path' });
  });

  it('should not detect a message without a url', () => {
    const result = instance.detect('This is a message with no url');
    assert.strictEqual(result, void 0, 'should be undefined');
  });

  it('should not timeout a link if not enabled', async () => {
    instance.isMod = async () => false;
    instance.findOne = (async (table: any, id: any) => {
      assert.strictEqual(table, UrlScan, 'should be the UrlScan table');
      assert.strictEqual(id, '#channel', 'should be the channel');
      return {
        channel: id,
        enabled: false
      } as UrlScan;
    }) as any;

    let timed = false;
    instance.timeout = async () => {
      timed = true;
      return [] as any;
    };

    await instance.run(
      '#channel',
      { username: 'testuser' } as Userstate,
      new Map([['google.com', { domain: 'google.com' }]])
    );
    assert(!timed);
  });

  it('should timeout a link if enabled and not an authorized user (SUB)(viewer)', async () => {
    instance.isMod = async () => false;
    instance.findOne = (async (table: any, id: any) => {
      assert.strictEqual(table, UrlScan, 'should be the UrlScan table');
      assert.strictEqual(id, '#channel', 'should be the channel');
      return {
        channel: id,
        enabled: true,
        permit: PermissionLevel.SUB,
        timeout: 44
      } as UrlScan;
    }) as any;

    let timed = false;
    instance.timeout = async (channel, username, length, reason) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(username, 'testuser', 'shoudl be the user');
      assert.strictEqual(length, 44, 'should be the length');
      assert.strictEqual(reason, 'Posted url');
      timed = true;
      return [channel, username, length, reason];
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the say channel');
      assert.strictEqual(message, 'TestUser, you were timed out for posting links.', 'should be the message');
      responded = true;
      return [channel];
    };

    await instance.run(
      '#channel',
      { username: 'testuser', 'display-name': 'TestUser' } as Userstate,
      new Map([['google.com', { domain: 'google.com' }]])
    );
    assert(timed);
    assert(responded);
  });

  it('should not timeout a link if enabled and authorized (SUB)(subscriber)', async () => {
    instance.isMod = async () => false;
    instance.findOne = (async (table: any, id: any) => {
      assert.strictEqual(table, UrlScan, 'should be the UrlScan table');
      assert.strictEqual(id, '#channel', 'should be the channel');
      return {
        channel: id,
        enabled: true,
        permit: PermissionLevel.SUB,
        timeout: 44
      } as UrlScan;
    }) as any;

    let timed = false;
    instance.timeout = async (channel, username, length, reason) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(username, 'testuser', 'shoudl be the user');
      assert.strictEqual(length, 44, 'should be the length');
      assert.strictEqual(reason, 'Posted url');
      timed = true;
      return [channel, username, length, reason];
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the say channel');
      assert.strictEqual(message, 'TestUser, you were timed out for posting links.', 'should be the message');
      responded = true;
      return [channel];
    };

    await instance.run(
      '#channel',
      { username: 'testuser', 'display-name': 'TestUser', subscriber: true } as Userstate,
      new Map([['google.com', { domain: 'google.com' }]])
    );
    assert(!timed);
    assert(!responded);
  });

  it('should timeout a link if enabled and authorized but not whitelisted', async () => {
    instance.isMod = async () => false;
    let found = false;
    instance.findOne = (async (table: any, id: any) => {
      assert.strictEqual(table, UrlScan, 'should be the UrlScan table');
      assert.strictEqual(id, '#channel', 'should be the channel');
      found = true;
      return {
        channel: id,
        enabled: true,
        permit: PermissionLevel.ALL,
        timeout: 44,
        useWhitelist: true,
        whitelist: 'youtube.com'
      } as UrlScan;
    }) as any;

    let timed = false;
    instance.timeout = async (channel, username, length, reason) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(username, TestUser.username, 'should be the user');
      assert.strictEqual(length, 44, 'should be the length');
      assert.strictEqual(reason, 'Posted forbidden url');
      timed = true;
      return [channel, username, length, reason];
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the say channel');
      assert.strictEqual(
        message,
        `${TestUser['display-name']}, you were timed out for posting a forbidden link.`,
        'should be the message'
      );
      responded = true;
      return [channel];
    };

    await instance.run('#channel', TestUser, new Map([['google.com', { domain: 'google.com' }]]));
    assert(found);
    assert(timed);
    assert(responded);
  });

  it('should not timeout a link if enabled and authorized is whitelisted', async () => {
    instance.isMod = async () => false;
    let found = false;
    instance.findOne = (async (table: any, id: any) => {
      assert.strictEqual(table, UrlScan, 'should be the UrlScan table');
      assert.strictEqual(id, '#channel', 'should be the channel');
      found = true;
      return {
        channel: id,
        enabled: true,
        permit: PermissionLevel.ALL,
        timeout: 44,
        useWhitelist: true,
        whitelist: 'youtube.com'
      } as UrlScan;
    }) as any;

    let timed = false;
    instance.timeout = async (channel, username, length, reason) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(username, TestUser.username, 'should be the user');
      assert.strictEqual(length, 44, 'should be the length');
      assert.strictEqual(reason, 'Posted forbidden url');
      timed = true;
      return [channel, username, length, reason];
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the say channel');
      assert.strictEqual(
        message,
        `${TestUser['display-name']}, you were timed out for posting a forbidden link.`,
        'should be the message'
      );
      responded = true;
      return [channel];
    };

    await instance.run('#channel', TestUser, new Map([['youtube.com', { domain: 'youtube.com' }]]));
    assert(found);
    assert(!timed);
    assert(!responded);
  });

  it('should not timeout a link if enabled and authorized is whitelisted (fuzzy)', async () => {
    instance.isMod = async () => false;
    let found = false;
    instance.findOne = (async (table: any, id: any) => {
      assert.strictEqual(table, UrlScan, 'should be the UrlScan table');
      assert.strictEqual(id, '#channel', 'should be the channel');
      found = true;
      return {
        channel: id,
        enabled: true,
        permit: PermissionLevel.ALL,
        timeout: 44,
        useWhitelist: true,
        whitelist: 'youtube.com'
      } as UrlScan;
    }) as any;

    let timed = false;
    instance.timeout = async (channel, username, length, reason) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(username, TestUser.username, 'should be the user');
      assert.strictEqual(length, 44, 'should be the length');
      assert.strictEqual(reason, 'Posted forbidden url');
      timed = true;
      return [channel, username, length, reason];
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the say channel');
      assert.strictEqual(
        message,
        `${TestUser['display-name']}, you were timed out for posting a forbidden link.`,
        'should be the message'
      );
      responded = true;
      return [channel];
    };

    await instance.run('#channel', TestUser, new Map([['test.youtube.com', { domain: 'test.youtube.com' }]]));
    assert(found);
    assert(!timed);
    assert(!responded);
  });
});
