import * as assert from 'assert';
import { MathRunner } from '../../src/features/MathRunner';
import { Userstate } from 'tmi.js';

describe('MathRunner', () => {
  let instance: MathRunner;

  beforeEach(() => {
    instance = new MathRunner();
  });

  it('should detect a math equation', () => {
    const message = '1+3';
    const result = instance.detect(message);
    assert.strictEqual(result[0], 1, 'should be the left side');
    assert.strictEqual(result[1], 3, 'should be the right side');
    assert.strictEqual(result[2], '+', 'should be the operator');
  });

  it('should detect a math equation in a sentance', () => {
    const message = 'this is 1+3 some filler';
    const result = instance.detect(message);
    assert.strictEqual(result[0], 1, 'should be the left side');
    assert.strictEqual(result[1], 3, 'should be the right side');
    assert.strictEqual(result[2], '+', 'should be the operator');
  });

  it('should detect a math equation with spaces', () => {
    const message = '1     +      3';
    const result = instance.detect(message);
    assert.strictEqual(result[0], 1, 'should be the left side');
    assert.strictEqual(result[1], 3, 'should be the right side');
    assert.strictEqual(result[2], '+', 'should be the operator');
  });

  it('should detect a math equation (subtraction)', () => {
    const message = '1-3';
    const result = instance.detect(message);
    assert.strictEqual(result[0], 1, 'should be the left side');
    assert.strictEqual(result[1], 3, 'should be the right side');
    assert.strictEqual(result[2], '-', 'should be the operator');
  });

  it('should detect a math equation (multiplication)', () => {
    const message = '1*3';
    const result = instance.detect(message);
    assert.strictEqual(result[0], 1, 'should be the left side');
    assert.strictEqual(result[1], 3, 'should be the right side');
    assert.strictEqual(result[2], '*', 'should be the operator');
  });

  it('should detect a math equation (division)', () => {
    const message = '1/3';
    const result = instance.detect(message);
    assert.strictEqual(result[0], 1, 'should be the left side');
    assert.strictEqual(result[1], 3, 'should be the right side');
    assert.strictEqual(result[2], '/', 'should be the operator');
  });

  it('should detect a math equation (larger numbers)', () => {
    const message = '123  +456';
    const result = instance.detect(message);
    assert.strictEqual(result[0], 123, 'should be the left side');
    assert.strictEqual(result[1], 456, 'should be the right side');
    assert.strictEqual(result[2], '+', 'should be the operator');
  });

  it('should not detect alpha addition', () => {
    const message = 'a+b';
    const result = instance.detect(message);
    assert.strictEqual(result, void 0, 'should not be detected');
  });

  it('should not detect alpha addition (left number)', () => {
    const message = '1+b';
    const result = instance.detect(message);
    assert.strictEqual(result, void 0, 'should not be detected');
  });

  it('should not detect alpha addition (right number)', () => {
    const message = 'a+4';
    const result = instance.detect(message);
    assert.strictEqual(result, void 0, 'should not be detected');
  });

  it('should not detect alpha addition (\\d\\w)', () => {
    const message = '1a+2b';
    const result = instance.detect(message);
    assert.strictEqual(result, void 0, 'should not be detected');
  });

  it('should add two numbers', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser: 2', 'should be the response');
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { 'display-name': 'TestUser' } as Userstate, 1, 1, '+');
    assert(responded);
  });

  it('should subtract two numbers', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser: 0', 'should be the response');
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { 'display-name': 'TestUser' } as Userstate, 1, 1, '-');
    assert(responded);
  });

  it('should multiply two numbers', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser: 6', 'should be the response');
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { 'display-name': 'TestUser' } as Userstate, 2, 3, '*');
    assert(responded);
  });

  it('should divide two numbers', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser: 2', 'should be the response');
      responded = true;
      return [channel];
    };

    await instance.run('TestChannel', { 'display-name': 'TestUser' } as Userstate, 6, 3, '/');
    assert(responded);
  });
});
