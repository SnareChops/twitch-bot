import * as assert from 'assert';
import { SuperCommandRunner } from '../../src/features/SuperCommandRunner';
import { container } from '../../src/container';
import { BOT_USERNAME, SUPER_COMMANDS, DB } from '../../src/di.tokens';
import { SuperCommand } from '../../src/builtin/supers';
import { Connection } from 'typeorm';
import { SuperAdmin } from '../../src/entities';
import { Userstate } from 'tmi.js';

describe('SuperCommandRunner', () => {
  let db: Connection;
  let superCommand: SuperCommand;
  let instance: SuperCommandRunner;

  beforeEach(() => {
    db = { manager: {} } as Connection;
    container.bind(DB).toConstantValue(db);
    container.bind(BOT_USERNAME).toConstantValue('BotUser');
    superCommand = {} as SuperCommand;
    container.bind(SUPER_COMMANDS).toConstantValue(new Map([['super', superCommand]]));
    instance = new SuperCommandRunner();
  });
  it('should detect a super command', () => {
    const result = instance.detect('BotUser super arg1 arg2');
    assert.deepStrictEqual(result, ['super', ['arg1', 'arg2']]);
  });

  it('should detect a super command with @ sign', () => {
    const result = instance.detect('@BotUser super arg1 arg2');
    assert.deepStrictEqual(result, ['super', ['arg1', 'arg2']]);
  });

  it("should not detect a super command that doesn't exist", () => {
    const result = instance.detect('BotUser other arg1 arg2');
    assert.strictEqual(result, void 0, 'should not find a command');
  });

  it('should not match without the command name', () => {
    const result = instance.detect('BotUser');
    assert.strictEqual(result, void 0, 'should not find a command');
  });

  it('should run a super command', async () => {
    let found = false;
    db.manager.findOne = async (table: any, key: any) => {
      assert.strictEqual(table, SuperAdmin, 'should be the super admin table');
      assert.strictEqual(key, 'superuser', 'should be the username');
      found = true;
      return new SuperAdmin('superuser');
    };

    let ran = false;
    superCommand.invoke = async (channel, args) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.deepStrictEqual(args, ['args'], 'should be the args');
      ran = true;
    };

    await instance.run('#channel', { username: 'superuser' } as Userstate, 'super', ['args']);
    assert(found);
    assert(ran);
  });
});
