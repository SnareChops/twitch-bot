import * as assert from 'assert';
import { FeatureRunner } from '../../src/features/FeatureRunner';
import { Connection } from 'typeorm';
import { Client } from '../../src/Client';
import { container } from '../../src/container';
import { DB, BUILT_IN } from '../../src/di.tokens';
import { VisibleBuiltIn } from '../_fixtures/VisibleBuiltIn';
import { Userstate } from 'tmi.js';

class TestFeature extends FeatureRunner {
  public getDb() {
    return this.db;
  }

  public getClient() {
    return this.client;
  }
}

describe('FeatureRunner', () => {
  let db: Connection;
  let client: Client;
  let instance: TestFeature;

  beforeEach(() => {
    db = { manager: {} } as Connection;
    client = {} as Client;
    container.bind(DB).toConstantValue(db);
    container.bind(Client).toConstantValue(client);
    instance = new TestFeature();
  });

  it('should get the db connection', () => {
    assert.strictEqual(instance.getDb(), db, 'should be the db');
  });

  it('should get the client', () => {
    assert.strictEqual(instance.getClient(), client, 'should be the client');
  });

  it('should say', async () => {
    let responded = false;
    client.action = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, 'test-message', 'should be the message');
      responded = true;
      return [channel];
    };

    await instance.say('#channel', 'test-message');
    assert(responded);
  });

  it('should find a command', async () => {
    const command = new VisibleBuiltIn();
    container.bind(BUILT_IN).toConstantValue(new Map([['visible', command]]));

    const result = await instance.findCommand('#channel', 'visible');
    assert.strictEqual(result, command, 'should be the command');
  });

  it('should save a db entity', async () => {
    const entity = {};

    let saved = false;
    db.manager.save = async (entities: any) => {
      assert.strictEqual(entities[0], entity, 'should have the entitiy');
      saved = true;
      return entities;
    };

    await instance.save(entity);
    assert(saved);
  });

  it('should determine if mod', async () => {
    assert.strictEqual(await instance.isMod('#channel', { mod: true } as Userstate), true, 'should be the response');
  });
});
