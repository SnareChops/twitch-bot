import * as assert from 'assert';
import { Channel } from '../../src/entities';

describe('Channel Entity', () => {
  it('should create a new channel', () => {
    const result = new Channel('#channel', 'Test notes');
    assert.strictEqual(result.name, '#channel', 'should be the name');
    assert.strictEqual(result.notes, 'Test notes', 'should be the notes');
  });
});
