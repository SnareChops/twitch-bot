import * as assert from 'assert';
import { SuperAdmin } from '../../src/entities';

describe('SuperAdmin Entity', () => {
  it('should create a new entity', () => {
    const result = new SuperAdmin('testuser');
    assert.strictEqual(result.username, 'testuser', 'should be the user');
    assert(result.createdAt, 'should have a created date');
  });
});
