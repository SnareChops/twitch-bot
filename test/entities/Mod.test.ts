import * as assert from 'assert';
import { Mod } from '../../src/entities';

describe('Mod Entity', () => {
  it('should create a new entity', () => {
    const result = new Mod('#channel', 'moduser');
    assert.strictEqual(result.channel, '#channel', 'should be the channel');
    assert.strictEqual(result.username, 'moduser', 'should be the username');
    assert(result.createdAt, 'should have a created date');
  });
});
