import * as assert from 'assert';
import { Command } from '../../src/entities';

describe('Command Entity', () => {
  it('should create a new command', () => {
    const result = new Command('#channel', 'command-name', 'command-response', 'moduser');
    assert.strictEqual(result.channel, '#channel', 'should be the channel');
    assert.strictEqual(result.name, 'command-name', 'should be the command name');
    assert.strictEqual(result.response, 'command-response', 'should be the response');
    assert.strictEqual(result.createdBy, 'moduser', 'should be the creator');
    assert(result.createdAt, 'should have a date');
  });
});
