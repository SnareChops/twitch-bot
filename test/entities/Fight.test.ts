import * as assert from 'assert';
import { Fight } from '../../src/entities';

describe('Fight Entity', () => {
  it('should create a new entuty', () => {
    const result = new Fight('testuser');
    assert.strictEqual(result.username, 'testuser', 'should be the username');
    assert.strictEqual(result.wins, 0, 'should be 0');
    assert.strictEqual(result.losses, 0, 'should be 0');
  });
});
