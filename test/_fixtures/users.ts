import { Userstate } from 'tmi.js';

export const TestUser = { username: 'testuser', 'display-name': 'TestUser' } as Userstate;
export const SubUser = { username: 'subuser', 'display-name': 'SubUser', subscriber: true } as Userstate;
export const VipUser = ({
  username: 'vipuser',
  'display-name': 'VipUser',
  badges: { vip: '1' }
} as unknown) as Userstate;
export const ModUser = { username: 'moduser', 'display-name': 'ModUser', mod: true } as Userstate;
export const StreamUser = {
  username: 'streamer',
  'display-name': 'Streamer',
  badges: { broadcaster: '1' }
} as Userstate;
