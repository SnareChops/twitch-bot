import { Userstate } from 'tmi.js';
import { BuiltIn, BuiltInCommand } from '../../src/builtin/BuiltIn';

@BuiltIn({
  name: 'ri',
  description: 'ri-builtin',
  visible: false,
  restricted: true
})
export class RIBuiltIn extends BuiltInCommand {
  public invoke(target: string, context: Userstate, args: string[]): Promise<any> {
    throw new Error('Method not implemented.');
  }
}
