import { Userstate } from 'tmi.js';
import { BuiltIn, BuiltInCommand } from '../../src/builtin/BuiltIn';

@BuiltIn({
  name: 'invisible',
  description: 'invisible-builtin',
  visible: false
})
export class InvisibleBuiltIn extends BuiltInCommand {
  public invoke(target: string, context: Userstate, args: string[]): Promise<any> {
    throw new Error('Method not implemented.');
  }
}
