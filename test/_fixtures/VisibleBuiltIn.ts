import { Userstate } from 'tmi.js';
import { BuiltIn, BuiltInCommand } from '../../src/builtin/BuiltIn';

@BuiltIn({
  name: 'visible',
  description: 'visible-builtin',
  visible: true
})
export class VisibleBuiltIn extends BuiltInCommand {
  public async invoke(target: string, context: Userstate, args: string[]): Promise<any> {
    return 'visible';
  }
}
