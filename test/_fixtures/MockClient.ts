import { client } from 'tmi.js';

export class MockClient implements client {
  private events = new Map<string, Function>();
  public trigger(event: string, ...args: any[]): void {
    this.events.get(event)(...args);
  }

  public on(event: string, handler: Function): void {
    this.events.set(event, handler);
  }
  public getChannels(): Promise<string[]> {
    throw new Error('Method not implemented.');
  }
  public getOptions(): Promise<object> {
    throw new Error('Method not implemented.');
  }
  public getUsername(): Promise<string> {
    throw new Error('Method not implemented.');
  }
  public isMod(channel: string, username: string): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public readyState(): Promise<'CONNECTING' | 'OPEN' | 'CLOSING' | 'CLOSED'> {
    throw new Error('Method not implemented.');
  }
  public action(channel: string, message: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public ban(channel: string, username: string, reason?: string): Promise<[string, string, string?]> {
    throw new Error('Method not implemented.');
  }
  public clear(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public color(color: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public commercial(channel: string, seconds: number): Promise<[string, number]> {
    throw new Error('Method not implemented.');
  }
  public async connect(): Promise<[string, number]> {
    return ['', 1];
  }
  public disconnect(): Promise<[string, number]> {
    throw new Error('Method not implemented.');
  }
  public emoteonly(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public emoteonlyoff(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public followersonly(channel: string, length: number): Promise<[string, number]> {
    throw new Error('Method not implemented.');
  }
  public followersonlyoff(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public host(channel: string, target: string): Promise<[string, string]> {
    throw new Error('Method not implemented.');
  }
  public join(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public mod(channel: string, username: string): Promise<[any]> {
    throw new Error('Method not implemented.');
  }
  public mods(channel: string): Promise<[any]> {
    throw new Error('Method not implemented.');
  }
  public part(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public ping(): Promise<[number]> {
    throw new Error('Method not implemented.');
  }
  public r9kbeta(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public r9kbetaoff(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public raw(message: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public say(channel: string, message: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public slow(channel: string, length: number): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public slowoff(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public subscribers(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public subscribersoff(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public timeout(
    channel: string,
    username: string,
    length?: number,
    reason?: string
  ): Promise<[string, string, number, string?]> {
    throw new Error('Method not implemented.');
  }
  public unban(channel: string, username: string): Promise<[string, string]> {
    throw new Error('Method not implemented.');
  }
  public unhost(channel: string): Promise<[string]> {
    throw new Error('Method not implemented.');
  }
  public unmod(channel: string, username: string): Promise<[string, string]> {
    throw new Error('Method not implemented.');
  }
  public whisper(channel: string, message: string): Promise<[string, string]> {
    throw new Error('Method not implemented.');
  }
}
