import { Userstate } from 'tmi.js';
import { BuiltIn, BuiltInCommand } from '../../src/builtin/BuiltIn';

@BuiltIn({
  name: 'disabled',
  description: 'disabled-builtin',
  visible: true
})
export class DisabledBuiltIn extends BuiltInCommand {
  public async enabled(channel: string) {
    return false;
  }
  public async invoke(target: string, context: Userstate, args: string[]): Promise<any> {
    return 'disabled';
  }
}
