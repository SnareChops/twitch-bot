import { Userstate } from 'tmi.js';
import { BuiltIn, BuiltInCommand } from '../../src/builtin/BuiltIn';

@BuiltIn({
  name: 'restricted',
  description: 'restricted-builtin',
  visible: true,
  restricted: true
})
export class RestrictedBuiltIn extends BuiltInCommand {
  public invoke(target: string, context: Userstate, args: string[]): Promise<any> {
    throw new Error('Method not implemented.');
  }
}
