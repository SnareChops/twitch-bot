import * as assert from 'assert';
import { Client } from '../../../src/Client';
import { container } from '../../../src/container';
import { DB } from '../../../src/di.tokens';
import { Command } from '../../../src/entities/Command';
import { Disable } from '../../../src/builtin/commands/Disable';

describe('disable', () => {
  let instance: Disable;

  beforeEach(() => {
    container.bind(Client).toConstantValue({} as any);
    container.bind(DB).toConstantValue({});
    instance = new Disable();
  });

  afterEach(() => {
    container.unbindAll();
  });

  it('should disable a command', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, `!test disabled!`);
      responded = true;
    };

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the !test name');
      found = true;
      return new Command('TestChannel', 'test', 'test-response', 'ModUser');
    };

    let disabled = false;
    instance.save = (async (command: Command) => {
      assert.strictEqual(command.name, 'test');
      assert.strictEqual(command.enabled, false, 'should be disabled');
      disabled = true;
    }) as any;

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as any, ['!test']);
    assert(found);
    assert(responded);
    assert(disabled);
  });

  it("should notify if a command doesn't exist", async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, `!test does not exist.`);
      responded = true;
      return [channel];
    };

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the !test name');
      found = true;
      return void 0;
    };

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as any, ['!test']);
    assert(found);
    assert(responded);
  });
});
