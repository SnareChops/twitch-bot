import * as assert from 'assert';
import { Client } from '../../../src/Client';
import { container } from '../../../src/container';
import { DB, BUILT_IN } from '../../../src/di.tokens';
import { Command } from '../../../src/entities';
import { Add } from '../../../src/builtin/commands/Add';
import { BuiltInCommand, BuiltIn } from '../../../src/builtin/BuiltIn';
import { Userstate } from 'tmi.js';

@BuiltIn({
  name: 'test',
  description: 'test-description'
})
class TestBuiltIn extends BuiltInCommand {
  public invoke(target: string, context: Userstate, args: string[]): Promise<any> {
    throw new Error('Method not implemented.');
  }
}

describe('add', () => {
  let instance: Add;

  beforeEach(() => {
    container.bind(Client).toConstantValue({} as any);
    container.bind(DB).toConstantValue({});
    instance = new Add();
  });

  afterEach(() => {
    container.unbindAll();
  });

  it('should add a command', async () => {
    let responded = false;

    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the test channel');
      assert.strictEqual(message, `!test added/updated.`);
      responded = true;
    };

    let stored = false;
    instance.save = async (command: Command) => {
      assert.strictEqual(command.name, 'test');
      assert.strictEqual(command.response, 'Test Response');
      assert.strictEqual(command.createdBy, 'TestUser');
      assert(!!command.createdAt);
      stored = true;
    };

    instance.findCommand = async () => void 0;

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as any, ['!test', 'Test', 'Response']);
    assert(stored);
    assert(responded);
  });

  it('should update an existing command', async () => {
    // setup existing command
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, `!test added/updated.`);
      responded = true;
    };

    let stored = false;
    instance.save = async (command: Command) => {
      assert.strictEqual(command.name, 'test');
      assert.strictEqual(command.response, 'Test Response');
      assert.strictEqual(command.createdBy, 'TestUser');
      assert(!!command.createdAt);
      stored = true;
    };

    instance.findCommand = async () => new Command('TestChannel', 'test', 'test-response', 'ModUser');

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as any, ['!test', 'Test', 'Response']);
    assert(responded);
    assert(stored);
  });

  it('should not add a command without a ! at the beginning of the name', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, `TestUser, Command name must start with !`);
      responded = true;
    };

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as any, ['test', 'Test', 'Response']);
    assert(responded);
  });

  it('should not add a command without a letter in the command name', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, `TestUser, Command name should contain at least 1 letter or number.`);
      responded = true;
    };
    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as any, ['!?&*%', 'Test', 'Response']);
    assert(responded);
  });

  it('should not add a command with additional special characters', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, `TestUser, Command name should not include any additional special characters.`);
      responded = true;
    };
    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as any, ['!test!', 'Test', 'Response']);
    assert(responded);
  });

  it('should not add a command without response', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, `TestUser, Command should have a response.`);
      responded = true;
    };
    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as any, ['!test']);
    assert(responded);
  });

  it('should not overwrite an existing built-in command', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, `TestUser, Cannot overwrite built-in commands.`);
      responded = true;
    };

    instance.findCommand = async () => new TestBuiltIn();
    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as any, ['!del', 'Test', 'Response']);
    assert(responded);
  });
});
