import * as assert from 'assert';
import { FindManyOptions } from 'typeorm';
import { Client } from '../../../src/Client';
import { container } from '../../../src/container';
import { DB, BUILT_IN } from '../../../src/di.tokens';
import { Commands } from '../../../src/builtin/commands/Commands';
import { BuiltInCommand } from '../../../src/builtin/BuiltIn';
import { RestrictedBuiltIn } from '../../_fixtures/RestrictedBuiltIn';
import { InvisibleBuiltIn } from '../../_fixtures/InvisibleBuiltIn';
import { RIBuiltIn } from '../../_fixtures/RIBuiltIn';
import { VisibleBuiltIn } from '../../_fixtures/VisibleBuiltIn';
import { DisabledBuiltIn } from '../../_fixtures/DisabledBuiltIn';
import { Command } from '../../../src/entities';

describe('commands', () => {
  let instance: Commands;

  beforeEach(() => {
    const test = new VisibleBuiltIn();
    const invis = new InvisibleBuiltIn();
    const rest = new RestrictedBuiltIn();
    const ri = new RIBuiltIn();
    const disabled = new DisabledBuiltIn();
    container.bind(BUILT_IN).toConstantValue(new Map());
    container.get<Map<string, BuiltInCommand>>(BUILT_IN).set('visible', test);
    container.get<Map<string, BuiltInCommand>>(BUILT_IN).set('invisible', invis);
    container.get<Map<string, BuiltInCommand>>(BUILT_IN).set('restricted', rest);
    container.get<Map<string, BuiltInCommand>>(BUILT_IN).set('ri', ri);
    container.get<Map<string, BuiltInCommand>>(BUILT_IN).set('disabled', disabled);

    container.bind(Client).toConstantValue({} as any);
    container.bind(DB).toConstantValue({});
    instance = new Commands();
  });

  afterEach(() => {
    container.unbindAll();
  });

  it('should list visible commands for non-mod users', async () => {
    instance.isMod = async () => false;

    let found = false;
    instance.find = (async (table: any, options: FindManyOptions<Command>) => {
      assert.strictEqual(table, Command, 'should be the command table');
      assert.deepEqual(
        options,
        { where: { channel: '#channel', enabled: true, scheduled: false, restricted: false } },
        'should be the filter'
      );
      found = true;
      return [new Command('#channel', 'db', 'response', 'ModUser')];
    }) as any;

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, `Available commands: !visible !db`);
      responded = true;
    };

    await instance.invoke('#channel', { badges: {}, 'display-name': 'TestUser', mod: false } as any);
    assert(found);
    assert(responded);
  });

  it('should list visible commands for mod users', async () => {
    instance.isMod = async () => true;

    let found = false;
    instance.find = async (table, options) => {
      assert.strictEqual(table, Command, 'should be the command table');
      assert.deepStrictEqual(
        options,
        { where: { channel: '#channel', enabled: true, scheduled: false } },
        'should be the filter'
      );
      found = true;
      return [];
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, `Available commands: !visible !restricted`);
      responded = true;
    };

    await instance.invoke('#channel', { 'display-name': 'TestUser', mod: true } as any);
    assert(found);
    assert(responded);
  });

  it('should list visible commands even without any db commands', async () => {
    instance.isMod = async () => false;

    let found = false;
    instance.find = async (table, options) => {
      assert.strictEqual(table, Command, 'should be the command table');
      assert.deepStrictEqual(
        options,
        { where: { channel: '#channel', enabled: true, scheduled: false, restricted: false } },
        'should be the filter'
      );
      found = true;
      return [];
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, `Available commands: !visible`);
      responded = true;
    };

    await instance.invoke('#channel', { 'display-name': 'TestUser', mod: false } as any);
    assert(found);
    assert(responded);
  });

  it('should list visible commands even without built in commands', async () => {
    instance.isMod = async () => false;

    let found = false;
    instance.find = (async (table: any, options: FindManyOptions<Command>) => {
      assert.strictEqual(table, Command, 'should be the command table');
      assert.deepEqual(
        options,
        { where: { channel: '#channel', enabled: true, scheduled: false, restricted: false } },
        'should be the filter'
      );
      found = true;
      return [new Command('#channel', 'db', 'response', 'ModUser')];
    }) as any;

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, `Available commands: !db`);
      responded = true;
    };

    container.unbind(BUILT_IN);
    container.bind(BUILT_IN).toConstantValue(void 0);
    await instance.invoke('#channel', { badges: {}, 'display-name': 'TestUser', mod: false } as any);
    assert(found);
    assert(responded);
  });

  it('should not list commands if disabled', async () => {
    instance.isMod = async () => false;

    let found = false;
    instance.find = (async (table: any, options: FindManyOptions<Command>) => {
      assert.strictEqual(table, Command, 'should be the command table');
      assert.deepEqual(
        options,
        { where: { channel: '#channel', enabled: true, scheduled: false, restricted: false } },
        'should be the filter'
      );
      found = true;
      return [new Command('#channel', 'db', 'response', 'ModUser')];
    }) as any;

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, `Available commands: !visible !db`);
      responded = true;
    };

    await instance.invoke('#channel', { badges: {}, 'display-name': 'TestUser', mod: false } as any);
    assert(found);
    assert(responded);
  });
});
