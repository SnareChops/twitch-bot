import * as assert from 'assert';
import { Userstate } from 'tmi.js';
import { Client } from '../../../../src/Client';
import { fights, rematches } from '../../../../src/builtin/commands/fight/state';
import { Fight } from '../../../../src/entities/Fight';
import { container } from '../../../../src/container';
import { DB } from '../../../../src/di.tokens';
import { Accept } from '../../../../src/builtin/commands/fight/Accept';

describe('accept', () => {
  let instance: Accept;
  beforeEach(() => {
    fights.clear();
    rematches.clear();
    container.bind(Client).toConstantValue({} as any);
    container.bind(DB).toConstantValue({});
    instance = new Accept();
  });

  afterEach(() => container.unbindAll());

  it('should accept a fight', async () => {
    let responded = 0;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert(
        /(otheruser|testuser)\s+wins!\s+(otheruser|testuser)\s+"!rematch"\?/.test(message),
        'should be the response'
      );
      assert(
        rematches.get('testuser') === 'otheruser' || rematches.get('otheruser') === 'testuser',
        'should have saved the rematch'
      );
      responded++;
    };

    let saved = 0;
    instance.saveFight = async (winner, loser) => {
      assert(winner !== loser, 'should not be the same user');
      assert(winner === 'testuser' || winner === 'otheruser', 'winner should be one of the users');
      assert(loser === 'testuser' || loser === 'otheruser', 'loser should be one of the users');
      saved++;
      return void 0;
    };

    fights.set('otheruser', 'testuser');

    await Promise.all(
      Array(20)
        .fill(1)
        .map(async (_, i) => {
          fights.set('otheruser', 'testuser');
          await instance.invoke('TestChannel', { username: 'otheruser' } as Userstate, void 0);
        })
    );
    assert.strictEqual(saved, 20, 'should have saved');
    assert.strictEqual(responded, 20, 'should have responded');
  });

  it('should do nothing if there is not a challenger', async () => {
    fights.clear();
    await instance.invoke('#channel', { username: 'testuser' } as Userstate, ['OtherUser']);
    // should do nothing
  });
});
