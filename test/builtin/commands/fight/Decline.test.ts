import * as assert from 'assert';
import { Userstate } from 'tmi.js';
import { Client } from '../../../../src/Client';
import { fights } from '../../../../src/builtin/commands/fight/state';
import { Decline } from '../../../../src/builtin/commands/fight/Decline';
import { container } from '../../../../src/container';
import { DB } from '../../../../src/di.tokens';

describe('decline', () => {
  let instance: Decline;

  beforeEach(() => {
    container.bind(Client).toConstantValue({} as any);
    container.bind(DB).toConstantValue({});
    fights.clear();
    instance = new Decline();
  });

  it('should decline a fight', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'Maybe next time testuser', 'should be the response');
      assert(!fights.has('otheruser'), 'should removed the fight');
      responded = true;
    };

    fights.set('otheruser', 'testuser');

    await instance.invoke('TestChannel', { 'display-name': 'OtherUser' } as Userstate, void 0);
    assert(responded);
  });
});
