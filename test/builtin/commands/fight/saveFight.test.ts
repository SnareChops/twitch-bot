import * as assert from 'assert';
import { Connection } from 'typeorm';
import { saveFight } from '../../../../src/builtin/commands/fight/saveFight';
import { container } from '../../../../src/container';
import { DB } from '../../../../src/di.tokens';
import { Fight } from '../../../../src/entities/Fight';
import { Accept } from '../../../../src/builtin/commands/fight/Accept';

describe('saveFight', () => {
  let db: Connection;

  beforeEach(() => {
    db = { manager: {} } as any;
    container.bind(DB).toConstantValue(db);
  });

  afterEach(() => container.unbindAll());

  it('should save a fight result', async () => {
    db.manager.findOne = (async (table: any, key: string) => {
      assert.strictEqual(table, Fight, 'should be the fight table');
      if (key === 'testloser') {
        const result = new Fight('testloser');
        result.losses = 2;
        return result;
      } else {
        return void 0;
      }
    }) as any;

    let created = false;
    let updated = false;
    db.manager.save = (async (fights: Fight[]) => {
      assert.strictEqual(fights[0].username, 'testwinner', 'should be the winner');
      assert.strictEqual(fights[0].wins, 1, 'should have a win');
      created = true;
      assert.strictEqual(fights[1].username, 'testloser', 'should be the loser');
      assert.strictEqual(fights[1].losses, 3, 'should have an additional loss');
      updated = true;
      return fights;
    }) as any;

    await saveFight('testwinner', 'testloser');
    assert(created);
    assert(updated);
  });

  it('should proxy to save fight in Accept BuiltInCommand', async () => {
    db.manager.findOne = (async (table: any, key: string) => {
      assert.strictEqual(table, Fight, 'should be the fight table');
      if (key === 'testloser') {
        const result = new Fight('testloser');
        result.losses = 2;
        return result;
      } else {
        return void 0;
      }
    }) as any;

    let created = false;
    let updated = false;
    db.manager.save = (async (fights: Fight[]) => {
      assert.strictEqual(fights[0].username, 'testwinner', 'should be the winner');
      assert.strictEqual(fights[0].wins, 1, 'should have a win');
      created = true;
      assert.strictEqual(fights[1].username, 'testloser', 'should be the loser');
      assert.strictEqual(fights[1].losses, 3, 'should have an additional loss');
      updated = true;
      return fights;
    }) as any;

    await new Accept().saveFight('testwinner', 'testloser');
    assert(created);
    assert(updated);
  });
});
