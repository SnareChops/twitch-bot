import * as assert from 'assert';
import { Client } from '../../../../src/Client';
import { fights, rematches } from '../../../../src/builtin/commands/fight/state';
import { Userstate } from 'tmi.js';
import { container } from '../../../../src/container';
import { DB } from '../../../../src/di.tokens';
import { Rematch } from '../../../../src/builtin/commands/fight/Rematch';

describe('rematch', () => {
  let instance: Rematch;

  beforeEach(() => {
    container.bind(Client).toConstantValue({} as any);
    container.bind(DB).toConstantValue({});
    fights.clear();
    rematches.clear();
    instance = new Rematch();
  });

  it('should perform a rematch', async () => {
    let responded = false;
    instance.say = async (target, message) => {
      assert.strictEqual(target, 'TestChannel', 'should be the channel');
      assert.strictEqual(
        message,
        'testuser, you have been challenged to a rematch by otheruser, "!accept" or "!decline"?',
        'should be the response'
      );
      assert(!rematches.has('otheruser'), 'should have removed the rematch');
      assert.strictEqual(fights.get('testuser'), 'otheruser', 'should have created the new fight');
      responded = true;
    };

    rematches.set('otheruser', 'testuser');

    await instance.invoke('TestChannel', { 'display-name': 'OtherUser' } as Userstate, void 0);
    assert(responded);
  });

  it('should should do nothing for a non existent rematch', async () => {
    let responded = false;
    instance.say = async (target, message) => {
      responded = true;
    };

    rematches.clear();

    await instance.invoke('TestChannel', { 'display-name': 'OtherUser' } as Userstate, void 0);
    assert(!responded);
  });
});
