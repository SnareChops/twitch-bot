import * as assert from 'assert';
import { Userstate } from 'tmi.js';
import { container } from '../../../../src/container';
import { DB } from '../../../../src/di.tokens';
import { Client } from '../../../../src/Client';
import { Fight } from '../../../../src/entities/Fight';
import { FightStats } from '../../../../src/builtin/commands/fight/Fightstats';
import { FightConfig } from '../../../../src/entities';

describe('fightstats', () => {
  let instance: FightStats;

  beforeEach(() => {
    container.bind(Client).toConstantValue({} as any);
    container.bind(DB).toConstantValue({});
    instance = new FightStats();
  });

  afterEach(() => container.unbindAll());

  it('should be disabled if not fightEnabled', async () => {
    let found = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, FightConfig, 'should be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      found = true;
      return { channel: '#channel', enabled: false } as FightConfig;
    }) as any;

    const result = await instance.enabled('#channel');
    assert(!result);
    assert(found);
  });

  it('should do nothing if not enabled', async () => {
    let checked = false;
    instance.enabled = async channel => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      checked = true;
      return false;
    };

    let responded = false;
    instance.say = async () => (responded = true);

    await instance.invoke('#channel', {} as Userstate);
    assert(checked);
    assert(!responded);
  });

  it('should display fight stats', async () => {
    let foundConfig = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, FightConfig, 'should be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      foundConfig = true;
      return { channel: '#channel', enabled: true } as FightConfig;
    }) as any;

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, 'TestUser your win/loss is 3/7');
      responded = true;
    };

    let found = 0;
    instance.findOne = (async (table: any, conditions: any) => {
      if (found === 0) {
        assert.strictEqual(table, FightConfig, 'should be the FightConfig table');
        assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
        foundConfig = true;
        found++;
        return { channel: '#channel', enabled: true } as FightConfig;
      } else {
        assert.strictEqual(table, Fight, 'should be the fight table');
        assert.deepStrictEqual(conditions, { username: 'testuser' }, 'should be the username');
        const result = new Fight(conditions.username);
        result.wins = 3;
        result.losses = 7;
        found++;
        return result;
      }
    }) as any;

    await instance.invoke('#channel', { username: 'testuser', 'display-name': 'TestUser' } as Userstate);
    assert.strictEqual(found, 2);
    assert(foundConfig);
  });
});
