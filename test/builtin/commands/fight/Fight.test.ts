import * as assert from 'assert';
import { Userstate } from 'tmi.js';
import { DateTime } from 'luxon';
import { Fight } from '../../../../src/builtin/commands/fight/Fight';
import { Client } from '../../../../src/Client';
import { fights, cooldowns } from '../../../../src/builtin/commands/fight/state';
import { container } from '../../../../src/container';
import { FIGHT_COOLDOWN } from '../../../../src/di.tokens';
import { DB } from '../../../../src/di.tokens';
import { FightConfig } from '../../../../src/entities';

describe('fight', () => {
  let instance: Fight;

  beforeEach(() => {
    fights.clear();
    container.bind(FIGHT_COOLDOWN).toConstantValue(1);
    container.bind(Client).toConstantValue({} as any);
    container.bind(DB).toConstantValue({});
    instance = new Fight();
  });

  afterEach(() => {
    cooldowns.clear();
    container.unbindAll();
  });

  it('should be disabled if not fightEnabled', async () => {
    let found = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, FightConfig, 'should be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      found = true;
      return { enabled: false } as FightConfig;
    }) as any;

    const result = await instance.enabled('#channel');
    assert(!result);
    assert(found);
  });

  it('should do nothing if not enabled', async () => {
    let checked = false;
    instance.enabled = async channel => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      checked = true;
      return false;
    };

    let responded = false;
    instance.say = async () => (responded = true);

    await instance.invoke('#channel', {} as Userstate, void 0);
    assert(checked);
    assert(!responded);
  });

  it('should create a fight', async () => {
    let foundConfig = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, FightConfig, 'shoudl be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      foundConfig = true;
      return { channel: '#channel', enabled: true } as FightConfig;
    }) as any;

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(
        message,
        'otheruser, you have been challenged to a fight by testuser, do you "!accept" or "!decline"?',
        'should be the response'
      );
      assert.strictEqual(fights.get('otheruser'), 'testuser', 'should have saved the fight');
      responded = true;
    };

    await instance.invoke('#channel', { username: 'testuser' } as Userstate, ['OtherUser']);
    assert(responded);
    assert(foundConfig);
  });

  it('should create a fight for a target with @', async () => {
    let foundConfig = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, FightConfig, 'should be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      foundConfig = true;
      return { channel: '#channel', enabled: true } as FightConfig;
    }) as any;

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(
        message,
        'otheruser, you have been challenged to a fight by testuser, do you "!accept" or "!decline"?',
        'should be the response'
      );
      assert.strictEqual(fights.get('otheruser'), 'testuser', 'should have saved the fight');
      responded = true;
    };

    await instance.invoke('#channel', { username: 'testuser' } as Userstate, ['@OtherUser']);
    assert(responded);
    assert(foundConfig);
  });

  it('should not create a fight if no opponenet selected', async () => {
    let foundConfig = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, FightConfig, 'should be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      foundConfig = true;
      return { channel: '#channel', enabled: true } as FightConfig;
    }) as any;

    let responded = false;
    instance.say = async (target, message) => {
      assert.strictEqual(target, '#channel', 'should be the channel');
      assert.strictEqual(message, 'TestUser, You must choose someone to fight.', 'should be the response');
      responded = true;
    };

    await instance.invoke('#channel', { 'display-name': 'TestUser' } as Userstate, void 0);
    assert(responded);
    assert(foundConfig);
  });

  it('should not create a fight if the opponent is the same user', async () => {
    let foundConfig = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, FightConfig, 'should be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      foundConfig = true;
      return { channel: '#channel', enabled: true } as FightConfig;
    }) as any;

    let responded = false;
    instance.say = async (target, message) => {
      assert.strictEqual(target, '#channel', 'should be the channel');
      assert.strictEqual(message, 'TestUser, You cannot fight yourself.', 'should be the response');
      responded = true;
    };

    await instance.invoke('#channel', { 'display-name': 'TestUser', username: 'testuser' } as Userstate, ['TestUser']);
    assert(responded);
    assert(foundConfig);
  });

  it('should not create a fight if on cooldown', async () => {
    let foundConfig = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, FightConfig, 'should be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      foundConfig = true;
      return { channel: '#channel', enabled: true } as FightConfig;
    }) as any;

    let responded = false;
    cooldowns.set('testuser', DateTime.local().plus({ seconds: 2 }));
    instance.say = async (target, message) => {
      assert.strictEqual(target, '#channel', 'should be the channel');
      assert.strictEqual(message, 'TestUser, Command is on cooldown.', 'should be the response');
      responded = true;
    };

    await instance.invoke('#channel', { 'display-name': 'TestUser', username: 'testuser' } as Userstate, ['OtherUser']);
    assert(responded);
    assert(foundConfig);
  });
});
