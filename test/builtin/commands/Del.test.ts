import * as assert from 'assert';
import { Client } from '../../../src/Client';
import { BuiltInCommand } from '../../../src/builtin/BuiltIn';
import { container } from '../../../src/container';
import { DB, BUILT_IN } from '../../../src/di.tokens';
import { Command } from '../../../src/entities';
import { Del } from '../../../src/builtin/commands/Del';
import { VisibleBuiltIn } from '../../_fixtures/VisibleBuiltIn';

describe('Del', () => {
  let instance: Del;

  beforeEach(() => {
    container.bind(BUILT_IN).toConstantValue(new Map());
    container.get<Map<string, BuiltInCommand>>(BUILT_IN).set('visible', new VisibleBuiltIn());
    container.bind(Client).toConstantValue({} as any);
    container.bind(DB).toConstantValue({ manager: {} });
    instance = new Del();
  });

  afterEach(() => {
    container.unbindAll();
  });

  it('should delete a command', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, `!test deleted!`);
      responded = true;
    };

    let deleted = false;
    instance['db'].manager.delete = (async (type: any, options: any) => {
      assert.strictEqual(type, Command, 'should be the Command type');
      assert.deepStrictEqual(options, { channel: '#channel', name: 'test' });
      deleted = true;
    }) as any;

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      return new Command('#channel', 'test', 'test-response', 'ModUser');
    };

    await instance.invoke('#channel', { 'display-name': 'TestUser' } as any, ['!test']);
    assert(found);
    assert(responded);
    assert(deleted);
  });

  it('should not delete an existing built-in command', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, `TestUser, Cannot delete built-in commands.`);
      responded = true;
    };

    await instance.invoke('#channel', { 'display-name': 'TestUser' } as any, ['!visible']);
    assert(responded);
  });
});
