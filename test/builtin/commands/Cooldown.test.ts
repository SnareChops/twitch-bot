import * as assert from 'assert';
import { Userstate } from 'tmi.js';
import { container } from '../../../src/container';
import { DB, BUILT_IN } from '../../../src/di.tokens';
import { Command } from '../../../src/entities/Command';
import { Cooldown } from '../../../src/builtin/commands/Cooldown';
import { Client } from '../../../src/Client';
import { VisibleBuiltIn } from '../../_fixtures/VisibleBuiltIn';

describe('Cooldown', () => {
  let instance: Cooldown;
  beforeEach(() => {
    container.bind(Client).toConstantValue({} as any);
    container.bind(DB).toConstantValue({});
    container.bind(BUILT_IN).toConstantValue(new Map());
    instance = new Cooldown();
  });

  afterEach(() => container.unbindAll());

  it('should set a command cooldown', async () => {
    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      return new Command('TestChannel', 'test', 'test-response', 'TestUser');
    };

    let saved = false;
    instance.save = async (command: Command) => {
      assert.strictEqual(command.cooldown, 111, 'should have set the cooldown time');
      saved = true;
      return [command];
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, '!test cooldown set!', 'should be the response');
      responded = true;
    };

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as Userstate, ['!test', '111']);
    assert(found);
    assert(saved);
    assert(responded);
  });

  it('should not set a cooldown for a non existent command', async () => {
    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      return void 0;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser, Command does not exist');
      responded = true;
      return [channel];
    };

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as Userstate, ['!test', '111']);
    assert(found);
    assert(responded);
  });

  it('should require a cooldown time', async () => {
    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      return new Command('TestChannel', 'test', 'test-response', 'TestUser');
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser, Must provide a cooldown time.');
      responded = true;
      return [channel];
    };

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as Userstate, ['!test']);
    assert(found);
    assert(responded);
  });

  it('cooldown time should be a number', async () => {
    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      return new Command('TestChannel', 'test', 'test-response', 'TestUser');
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser, Cooldown time must be a number.');
      responded = true;
      return [channel];
    };

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as Userstate, ['!test', 'abc']);
    assert(found);
    assert(responded);
  });

  it('cooldown time should be 0 or greater', async () => {
    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the command name');
      found = true;
      return new Command('TestChannel', 'test', 'test-response', 'TestUser');
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser, Cooldown time must be 0 or greater.');
      responded = true;
      return [channel];
    };

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as Userstate, ['!test', '-4']);
    assert(found);
    assert(responded);
  });

  it('should not set a built in command cooldown', async () => {
    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'visible', 'should be the command name');
      found = true;
      return new VisibleBuiltIn();
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'TestUser, Cannot set cooldowns on built-in commands.');
      responded = true;
      return [channel];
    };

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as Userstate, ['!visible', '111']);
    assert(found);
    assert(responded);
  });
});
