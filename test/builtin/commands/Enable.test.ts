import * as assert from 'assert';
import { Client } from '../../../src/Client';
import { container } from '../../../src/container';
import { DB, BUILT_IN } from '../../../src/di.tokens';
import { Command } from '../../../src/entities/Command';
import { Enable } from '../../../src/builtin/commands/Enable';

describe('Enable', () => {
  let instance: Enable;

  beforeEach(() => {
    container.bind(Client).toConstantValue({} as any);
    container.bind(DB).toConstantValue({});
    container.bind(BUILT_IN).toConstantValue(new Map());
    instance = new Enable();
  });

  afterEach(() => {
    container.unbindAll();
  });

  it('should enable a command', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, `!test enabled!`);
      responded = true;
    };

    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the !test name');
      found = true;
      return new Command('TestChannel', 'test', 'test-response', 'ModUser');
    };

    let enabled = false;
    instance.save = (async (command: Command) => {
      assert.strictEqual(command.name, 'test');
      assert.strictEqual(command.enabled, true, 'should be enabled');
      enabled = true;
    }) as any;

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as any, ['!test']);
    assert(found);
    assert(responded);
    assert(enabled);
  });

  it("should not enable a command that doesn't exist", async () => {
    let found = false;
    instance.findCommand = async (channel, name) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(name, 'test', 'should be the !test name');
      found = true;
      return void 0;
    };

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(message, '!test does not exist.');
      responded = true;
      return [channel];
    };

    await instance.invoke('TestChannel', { 'display-name': 'TestUser' } as any, ['!test']);
    assert(found);
    assert(responded);
  });
});
