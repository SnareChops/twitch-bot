import * as assert from 'assert';
import { Userstate } from 'tmi.js';
import { Roll } from '../../../src/builtin/commands/Roll';
import { Client } from '../../../src/Client';
import { container } from '../../../src/container';
import { RollConfig } from '../../../src/entities';

describe('Roll', () => {
  let client: Client;
  let instance: Roll;

  beforeEach(() => {
    client = { commandRunner: { cooldowns: new Map() } } as Client;
    container.bind(Client).toConstantValue(client);
    instance = new Roll();
  });

  it('should be disabled if not rollEnabled', async () => {
    let found = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, RollConfig, 'should be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      found = true;
      return { channel: '#channel', enabled: false } as RollConfig;
    }) as any;

    const result = await instance.enabled('#channel');
    assert(found);
    assert(!result);
  });

  it('should roll a number between 1 & 10', async () => {
    let foundConfig = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, RollConfig, 'should be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      foundConfig = true;
      return { channel: '#channel', enabled: true } as RollConfig;
    }) as any;

    let count = 0;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      const match = /TestUser:\s(\d+)/.exec(message);
      assert(match);
      assert(+match[1] <= 10, 'should be less than or equal to 10');
      assert(+match[1] > 0, 'should be greater than 0');
      count++;
      return [channel];
    };

    await Promise.all(
      Array.from({ length: 100 }).map(() =>
        instance.invoke('#channel', { 'display-name': 'TestUser' } as Userstate, void 0)
      )
    );
    assert.strictEqual(count, 100, 'should have run 10 times');
    assert(foundConfig);
    assert(client.commandRunner.cooldowns.has('roll'));
  });

  it('should roll a number between 1 & 100', async () => {
    let foundConfig = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, RollConfig, 'should be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      foundConfig = true;
      return { channel: '#channel', enabled: true } as RollConfig;
    }) as any;

    let count = 0;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      const match = /TestUser:\s(\d+)/.exec(message);
      assert(match);
      assert(+match[1] <= 100, 'should be less than or equal to 10');
      assert(+match[1] > 0, 'should be greater than 0');
      count++;
      return [channel];
    };

    await Promise.all(
      Array.from({ length: 100 }).map(() =>
        instance.invoke('#channel', { 'display-name': 'TestUser' } as Userstate, ['100'])
      )
    );
    assert.strictEqual(count, 100, 'should have run 10 times');
    assert(foundConfig);
  });

  it('should roll 1', async () => {
    let foundConfig = false;
    instance.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, RollConfig, 'should be the FightConfig table');
      assert.deepStrictEqual(conditions, { channel: '#channel' }, 'should be the channel');
      foundConfig = true;
      return { channel: '#channel', enabled: true } as RollConfig;
    }) as any;

    let count = 0;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      const match = /TestUser:\s(\d+)/.exec(message);
      assert(match);
      assert(+match[1] === 1, 'should always be 1');
      count++;
      return [channel];
    };

    await Promise.all(
      Array.from({ length: 100 }).map(() =>
        instance.invoke('#channel', { 'display-name': 'TestUser' } as Userstate, ['1'])
      )
    );
    assert.strictEqual(count, 100, 'should have run 10 times');
    assert(foundConfig);
  });

  it('should not roll if disabled', async () => {
    instance.enabled = async () => false;

    let responded = false;
    instance.say = async () => (responded = true);

    await instance.invoke('#channel', {} as Userstate, void 0);
    assert(!responded);
  });
});
