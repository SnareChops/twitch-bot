import * as assert from 'assert';
import { BuiltInCommand, BuiltIn } from '../../src/builtin/BuiltIn';
import { Userstate } from 'tmi.js';
import { container } from '../../src/container';
import { DB, IS_MOD, BUILT_IN } from '../../src/di.tokens';
import { Connection } from 'typeorm';
import { Client } from '../../src/Client';
import { Mod, Command } from '../../src/entities';
import { VisibleBuiltIn } from '../_fixtures/VisibleBuiltIn';

class TestBuiltIn extends BuiltInCommand {
  public getDb() {
    return this.db;
  }

  public getClient() {
    return this.client;
  }

  public invoke(target: string, context: Userstate, args: string[]): Promise<any> {
    throw new Error('Method not implemented.');
  }
}
describe('BuiltIn', () => {
  let client: Client;
  let db: Connection;
  let instance: TestBuiltIn;

  beforeEach(() => {
    Reflect.defineMetadata('builtin:metadata', {}, TestBuiltIn);
    client = {} as Client;
    db = { manager: {} } as any;
    container.bind(Client).toConstantValue(client);
    container.bind(DB).toConstantValue(db);
    instance = new TestBuiltIn();
  });

  it('should get the database', () => {
    const result = instance.getDb();
    assert.strictEqual(result, db, 'should be the db connection');
  });

  it('should get the client', () => {
    const result = instance.getClient();
    assert.strictEqual(result, client, 'should be the client');
  });

  it('should find an entity', async () => {
    const opts = {};
    let found = false;
    db.manager.find = (async (table: any, options: any) => {
      assert.strictEqual(table, Mod, 'should be the table');
      assert.strictEqual(options, opts, 'should be the options');
      found = true;
      return [new Mod('TestChannel', 'moduser')];
    }) as any;

    const result = await instance.find(Mod, opts);
    assert(result[0] instanceof Mod);
    assert(found);
  });

  it('should find one entity', async () => {
    let found = false;
    db.manager.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, Mod, 'should be the table');
      assert.deepStrictEqual(conditions, { channel: 'TestChannel', username: 'moduser' }, 'should be the conditions');
      found = true;
      return new Mod('TestChannel', 'moduser');
    }) as any;

    const result = await instance.findOne(Mod, { channel: 'TestChannel', username: 'moduser' });
    assert(result instanceof Mod);
    assert(found);
  });

  it('should save an entity', async () => {
    const item = new Mod('TestChannel', 'moduser');

    let saved = false;
    db.manager.save = (async (items: any[]) => {
      assert.strictEqual(items[0], item, 'should be the entity to save');
      saved = true;
      return items;
    }) as any;

    const result = await instance.save(item);
    assert.strictEqual(result[0], item, 'should be the item');
    assert(saved);
  });

  it('should say something', async () => {
    let said = false;
    client.action = async (channel, message) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(message, 'Test-Message', 'should be the message');
      said = true;
      return [channel];
    };

    const result = await instance.say('TestChannel', 'Test-Message');
    assert.strictEqual(result[0], 'TestChannel', 'should be the channel');
    assert(said);
  });

  it('should check if a user is a mod', async () => {
    const fake = { 'display-name': 'TestUser' } as Userstate;
    let ran = false;
    container.bind(IS_MOD).toConstantValue((channel: string, context: Userstate) => {
      assert.strictEqual(channel, 'TestChannel', 'should be the channel');
      assert.strictEqual(context, fake, 'should be the context');
      ran = true;
      return true;
    });

    const result = await instance.isMod('TestChannel', fake);
    assert(result);
    assert(ran);
  });

  it('should find a builtin command', async () => {
    const command = new VisibleBuiltIn();
    container.bind(BUILT_IN).toConstantValue(new Map([['visible', command]]));

    const result = await instance.findCommand('TestChannel', 'visible');
    assert.strictEqual(result, command, 'should be the builtin command');
  });

  it('should find a db command', async () => {
    const command = new Command('TestChannel', 'test', 'response', 'ModUser');
    container.bind(BUILT_IN).toConstantValue(new Map());
    db.manager.findOne = (async (table: any, conditions: any) => {
      assert.strictEqual(table, Command, 'should be the command table');
      assert.deepStrictEqual(conditions, { channel: 'TestChannel', name: 'test' }, 'should be the key');
      return command;
    }) as any;

    const result = await instance.findCommand('TestChannel', 'test');
    assert.strictEqual(result, command, 'should be the db command');
  });
});
