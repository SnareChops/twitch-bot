import * as assert from 'assert';
import { SuperCommand } from '../../../src/builtin/supers';
import { container } from '../../../src/container';
import { Client } from '../../../src/Client';

class TestSuper extends SuperCommand {
  public async invoke(target: string, args: string[]): Promise<any> {
    return [target, args];
  }

  public getClient() {
    return this.client;
  }
}

describe('SuperCommand', () => {
  let client: Client;
  let instance: TestSuper;

  beforeEach(() => {
    client = {} as Client;
    container.bind(Client).toConstantValue(client);
    instance = new TestSuper();
  });

  it('should have get the client', async () => {
    const result = instance.getClient();
    assert.strictEqual(result, client, 'should be the client');
  });

  it('should say a response', async () => {
    let responded = false;
    client.action = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, 'test-message', 'should be the message');
      responded = true;
      return [channel];
    };

    await instance.say('#channel', 'test-message');
    assert(responded);
  });

  it('should invoke', async () => {
    const result = await instance.invoke('channel', ['test', 'args']);
    assert.deepStrictEqual(result, ['channel', ['test', 'args']], 'should be the args');
  });
});
