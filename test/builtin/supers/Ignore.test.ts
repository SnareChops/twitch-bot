import * as assert from 'assert';
import { Ignore } from '../../../src/builtin/supers';
import { Client } from '../../../src/Client';
import { container } from '../../../src/container';

describe('Ignore', () => {
  let client: Client;
  let instance: Ignore;

  beforeEach(() => {
    client = {} as Client;
    container.bind(Client).toConstantValue(client);
    instance = new Ignore();
  });

  afterEach(() => container.unbindAll());

  it('should ignore a channel', async () => {
    client.ignoreList = new Set();

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, 'Added #tobeignored to channel ignore list', 'should be the message');
      responded = true;
      return [channel];
    };

    await instance.invoke('#channel', ['add', '#tobeignored']);
    assert(responded);
    assert(client.ignoreList.has('#tobeignored'));
  });

  it('should list ignored channels', async () => {
    client.ignoreList = new Set(['#achannel', '#bchannel']);

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, 'Ignored channels: #achannel, #bchannel');
      responded = true;
      return [channel];
    };

    await instance.invoke('#channel', ['list', '#tobeignored']);
    assert(responded);
  });

  it('should remove a channel from ignored list', async () => {
    client.ignoreList = new Set(['#achannel']);

    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, 'Removed #achannel from channel ignore list', 'should be the message');
      responded = true;
      return [channel];
    };

    await instance.invoke('#channel', ['remove', '#achannel']);
    assert(responded);
    assert(!client.ignoreList.has('#achannel'));
  });

  it('should reject an unknown subcommand', async () => {
    let responded = false;
    instance.say = async (channel, message) => {
      assert.strictEqual(channel, '#channel', 'should be the channel');
      assert.strictEqual(message, 'Unrecognized subcommand unknown', 'should be the message');
      responded = true;
      return [channel];
    };

    await instance.invoke('#channel', ['unknown', '#achannel']);
    assert(responded);
  });
});
