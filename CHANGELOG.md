# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.5.1] 19 Apr 2019

- Fixed issue with toolbar avatar image

## [0.5.0] 19 Apr 2019

- Fixed a bug with deleting commands
- Added scheduled commands support
- Added url timeout with whitelist support
- Added super command to make channel adding easier

## [0.4.3] 6 Apr 2019

- [client] Set redirect_uri to production uri

## [0.4.2] 6 Apr 2019

- Added site production build to CI config

## [0.4.1] 6 Apr 2019

- Added site build to CI config

## [0.4.0] 6 Apr 2019

Beta 0.4.x Release

- Added client UI for managing commands and config
- Added ability to disable fight commands
- Added ability to disable roll commands
- Added Mod Chat feature to client UI

## [0.3.2] 23 Mar 2019

Beta 0.3.x Bugfixes

- Fixed bug where !commands would return commands from other channels
- Added quotes around hints in fight command responses

## [0.3.1] 22 Mar 2019

Beta 0.3.x Release

- Added ignore super command for bot super admins to ignore channels
- Added Multi-tennant support allowing for the bot to serve multiple channels at the same time

## [0.2.0] ???

Alpha 0.2.x Release

- Added !cooldown command `!cooldown !<command name> <time in seconds>`
- - Sets a cooldown for a command (spam prevention)
- Added !fightstats command `!fightstats`
- - Shows win / loss stats for fights participated in
- Added description feature for commands
- - Start any command with a ? instead of a ! to see a description of the command
- - Use `?<command name> <description text>` to update a description
- Added a cooldown setting for !fight (spam prevention)
- Changed commands to only recognized at the beginning of messages
- Added more helpful error messages to guide users through command usage

Bugfixes:

- Fixed an issue that prevented broadcaster not being considered a mod in their own channel
- Fixed an issue where users could !fight themselves
- Fixed an issue where commands that use @ in their targets would prevent them from being responded to
- Fixed an issue that prevented users without badges from using commands
- Fixed an issue with !disable being used without a command to disable

## [0.1.0] 22 Feb 2019

Alpha 0.1.x Release Includes

- Add / update simple response commands `!add !<command name> <command response>`
- Delete simple response commands `!del !<command name>`
- Disable response commands `!disable !<command name>`
- Enable disabled response commands `!enable !<command name>`
- Get a list of commands users can run `!commands`
- Simple coin flip game for viewers `!fight <username>`
- Simple random number generator `!roll` or `!roll <max>`
