import './init';
import { Connection } from 'typeorm';
import { WampClient } from 'mumba-wamp';
import { client } from 'tmi.js';
import { Client, clientFrom } from './Client';
import { container } from './container';
import { setupDb, Channel } from './entities';
import { DB, AUTH_TOKEN, BOT_USERNAME, CROSSBAR_SECRET } from './di.tokens';
import { makeWampClient } from './WampClient';
import { registerActions } from './actions';
import { CommandRunner } from './features/CommandRunner';
import { DescriptionRunner } from './features/DescriptionRunner';
import { MathRunner } from './features/MathRunner';
import { SuperCommandRunner } from './features/SuperCommandRunner';
import { UrlScanRunner } from './features/UrlScanRunner';
import { ScheduleRunner } from './features/ScheduleRunner';

// tslint:disable-next-line
console.log('SANITY');
(async () => {
  // tslint:disable-next-line
  console.log('Setting up DB connection...');
  await setupDb().then(connection => container.bind(DB).toConstantValue(connection));
  // tslint:disable-next-line
  console.log('Loading builtin commands...');
  import('./builtin/init');
  const username = container.get<string>(BOT_USERNAME);
  const token = container.get<string>(AUTH_TOKEN);
  const connection = container.get<Connection>(DB);
  const crossbarSecret = container.get<string>(CROSSBAR_SECRET);
  const channels = await container.get<Connection>(DB).manager.find(Channel);
  // tslint:disable-next-line
  console.log('Creating client...');
  container
    .bind(Client)
    .toConstantValue(
      new (clientFrom(client))(
        username,
        token,
        channels.map(x => x.name.replace('#', '')),
        connection,
        new CommandRunner(),
        new DescriptionRunner(),
        new MathRunner(),
        new SuperCommandRunner(),
        new UrlScanRunner(),
        new ScheduleRunner()
      )
    );
  // tslint:disable-next-line
  console.log('CALLING INIT');
  await container.get<Client>(Client).init();
  // tslint:disable-next-line
  console.log('AFTER INIT');
  container.bind(WampClient).toConstantValue(makeWampClient(crossbarSecret));
  registerActions();
})();
