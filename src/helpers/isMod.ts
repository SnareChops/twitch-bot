import { Mod } from '../entities/Mod';
import { db } from './db';
import { Userstate } from 'tmi.js';

export async function isMod(channel: string, context: Userstate): Promise<boolean> {
  if (context.mod) {
    return true;
  }
  if (!!context.badges && !!context.badges.broadcaster) {
    return true;
  }
  return await db()
    .manager.findOne(Mod, { channel, username: context['display-name'] })
    .then(x => !!x);
}
