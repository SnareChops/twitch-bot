import { Connection } from 'typeorm';
import { container } from '../container';
import { DB } from '../di.tokens';

export function db(): Connection {
  return container.get<Connection>(DB);
}
