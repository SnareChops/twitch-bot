import { container } from '../container';
import { IS_MOD } from '../di.tokens';
import { isMod } from './isMod';

container.bind(IS_MOD).toConstantValue(isMod);
