import { BUILT_IN } from '../di.tokens';
import { container } from '../container';
import { BuiltInCommand } from '../builtin/BuiltIn';
import { Command } from '../entities';
import { db } from './db';

export async function command<T = BuiltInCommand | Command>(channel: string, name: string): Promise<T> {
  const result = container.get<Map<string, BuiltInCommand>>(BUILT_IN).get(name);
  if (!!result) {
    return (result as unknown) as T;
  }
  return ((await db().manager.findOne(Command, { channel, name })) as unknown) as T;
}
