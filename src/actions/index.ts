import { WampClient } from 'mumba-wamp';
import { container } from '../container';
import { authenticate } from './Authenticate';
import {
  GetCommands,
  GetCommandById,
  UpdateCommand,
  DeleteCommand,
  DisableCommand,
  EnableCommand,
  RestrictCommand,
  UnrestrictCommand,
  CreateCommand,
  UnscheduleCommand,
  ScheduleCommand
} from './Commands';
import { log } from '../helpers/log';
import { GetHistory, Send } from './ModChat';
import { GetFightConfigByChannel, UpdateFightConfig, CreateFightConfig } from './FightConfig';
import { GetRollConfigByChannel, UpdateRollConfig, CreateRollConfig } from './RollConfig';
import { GetUrlScanByChannel, UpdateUrlScan, CreateUrlScan } from './UrlScan';

const actions = [
  // Auth
  authenticate,
  // Commands
  GetCommands,
  GetCommandById,
  CreateCommand,
  UpdateCommand,
  DeleteCommand,
  DisableCommand,
  EnableCommand,
  RestrictCommand,
  UnrestrictCommand,
  ScheduleCommand,
  UnscheduleCommand,

  // Fight Config
  GetFightConfigByChannel,
  UpdateFightConfig,
  CreateFightConfig,

  // Roll Config
  GetRollConfigByChannel,
  UpdateRollConfig,
  CreateRollConfig,

  // Url Scan
  GetUrlScanByChannel,
  UpdateUrlScan,
  CreateUrlScan,

  // Mod Chat
  GetHistory,
  Send
];

export function registerActions() {
  log('registering actions', actions);
  const wamp = container.get(WampClient);
  for (const action of actions) {
    action(wamp);
  }
}
