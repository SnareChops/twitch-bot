import { WampClient } from 'mumba-wamp';
import * as got from 'got';
import { db } from '../helpers/db';
import { Mod } from '../entities/Mod';

export function authenticate(wampClient: WampClient) {
  wampClient.register(
    'chatbot.authenticate',
    async (
      [realm, username, { ticket, authextra } = { ticket: void 0, authextra: void 0 }]: [string, string, any],
      kwargs: {},
      details: any
    ) => {
      if (username === 'access_token') {
        const { body } = await got(`https://id.twitch.tv/oauth2/validate`, {
          headers: { Authorization: `OAuth ${ticket}` },
          json: true
        }).catch(x => {
          throw new Error(`invalid: ${x}`);
        });

        const users = await db().manager.find(Mod, { where: { username: body.login } });
        if (!users || users.length === 0) {
          throw new Error('unauthorized');
        }
        return {
          realm,
          role: 'authenticated',
          authid: body.login,
          extra: {
            access_token: ticket,
            channels: users.map(x => x.channel)
          }
        };
      }
      throw new Error('unrecognized login');
    }
  );
}
