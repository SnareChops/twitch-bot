import { WampClient } from 'mumba-wamp';
import { db } from '../helpers/db';
import { ModChatMessage } from '../entities/ModChatMessage';

function manager() {
  return db().manager;
}

export function GetHistory(wampClient: WampClient) {
  wampClient.register('public.modchat.history', ([channel, skip, take]: [string, number, number]) => {
    return manager().find(ModChatMessage, { where: { channel }, skip, take, order: { timestamp: 'DESC' } });
  });
}
export function Send(wampClient: WampClient) {
  wampClient.register('public.modchat.send', async ([message]: [ModChatMessage]) => {
    message = new ModChatMessage(message.channel, message.message, message.name);
    [message] = await manager().save([message]);
    wampClient.publish(`public.modchat.message.${message.channel.replace('#', '')}`, [message]);
  });
}
