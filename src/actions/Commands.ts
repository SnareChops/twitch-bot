import { WampClient } from 'mumba-wamp';
import { Command } from '../entities';
import { db } from '../helpers/db';
import { Invocation } from '../WampClient';
import { container } from '../container';
import { Client } from '../Client';

function manager() {
  return db().manager;
}

export function GetCommands(wampClient: WampClient) {
  wampClient.register('public.command.get', ([channel]: [string]) => {
    return manager().find(Command, { where: { channel } });
  });
}
export function GetCommandById(wampClient: WampClient) {
  wampClient.register('public.command.getById', ([channel, name]: [string, string]) => {
    return manager()
      .findOne(Command, { channel, name })
      .then(x => x[0]);
  });
}

export function CreateCommand(wampClient: WampClient) {
  wampClient.register('public.command.create', ([command]: [Command], _: any, { caller_authid }: Invocation) => {
    return manager().save([Command.from(command, caller_authid)]);
  });
}

export function UpdateCommand(wampClient: WampClient) {
  wampClient.register('public.command.update', async ([command]: [Command]) => {
    async function fetch() {
      return manager().findOne(Command, { channel: command.channel, name: command.name });
    }
    const old = await fetch();
    if (old.schedule !== command.schedule) {
      container.get<Client>(Client).scheduleRunner.add(command);
    }
    return manager()
      .update(Command, { channel: command.channel, name: command.name }, command)
      .then(() => fetch());
  });
}

export function DeleteCommand(wampClient: WampClient) {
  wampClient.register('public.command.delete', ([channel, name]: [string, string]) => {
    const result = manager().delete(Command, { channel, name });
    container.get<Client>(Client).scheduleRunner.remove(name);
    return result;
  });
}

export function EnableCommand(wampClient: WampClient) {
  wampClient.register('public.command.enable', async ([channel, name]: [string, string]) => {
    const m = manager();
    const command = await m.findOne(Command, { channel, name });
    command.enabled = true;
    return m.save([command]).then(x => x[0]);
  });
}

export function DisableCommand(wampClient: WampClient) {
  wampClient.register('public.command.disable', async ([channel, name]: [string, string]) => {
    const m = manager();
    const command = await m.findOne(Command, { channel, name });
    command.enabled = false;
    return m.save([command]).then(x => x[0]);
  });
}

export function RestrictCommand(wampClient: WampClient) {
  wampClient.register('public.command.restrict', async ([channel, name]: [string, string]) => {
    const m = manager();
    const command = await m.findOne(Command, { channel, name });
    command.restricted = true;
    return m.save([command]).then(x => x[0]);
  });
}

export function UnrestrictCommand(wampClient: WampClient) {
  wampClient.register('public.command.unrestrict', async ([channel, name]: [string, string]) => {
    const m = manager();
    const command = await m.findOne(Command, { channel, name });
    command.restricted = false;
    return m.save([command]).then(x => x[0]);
  });
}

export function ScheduleCommand(wampClient: WampClient) {
  wampClient.register('public.command.schedule', async ([channel, name]: [string, string]) => {
    const m = manager();
    const command = await m.findOne(Command, { channel, name });
    command.scheduled = true;
    const result = await m.save([command]).then(x => x[0]);
    container.get<Client>(Client).scheduleRunner.add(result);
    return result;
  });
}

export function UnscheduleCommand(wampClient: WampClient) {
  wampClient.register('public.command.unschedule', async ([channel, name]: [string, string]) => {
    const m = manager();
    const command = await m.findOne(Command, { channel, name });
    command.scheduled = false;
    const result = await m.save([command]).then(x => x[0]);
    container.get<Client>(Client).scheduleRunner.remove(name);
    return result;
  });
}
