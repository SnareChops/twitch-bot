import { db } from '../helpers/db';
import { WampClient } from '../../client/services/WampClient';
import { Invocation } from '../WampClient';
import { UrlScan } from '../entities/UrlScan';

function manager() {
  return db().manager;
}

export function GetUrlScanByChannel(wampClient: WampClient) {
  wampClient.register('public.urlscan.getByChannel', ([channel]: [string]) => {
    return manager().findOne(UrlScan, channel);
  });
}

export function UpdateUrlScan(wampClient: WampClient) {
  wampClient.register('public.urlscan.update', ([config]: [UrlScan], kwargs: any, { caller_authid }: Invocation) => {
    config = UrlScan.fromObject(config, caller_authid);
    return manager().save(config);
  });
}

export function CreateUrlScan(wampClient: WampClient) {
  wampClient.register('public.urlscan.create', ([channel]: [string], kwargs: any, { caller_authid }: Invocation) => {
    const config = new UrlScan(channel, caller_authid);
    return manager().save(config);
  });
}
