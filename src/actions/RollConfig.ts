import { db } from '../helpers/db';
import { WampClient } from '../../client/services/WampClient';
import { Invocation } from '../WampClient';
import { RollConfig } from '../entities';

function manager() {
  return db().manager;
}

export function GetRollConfigByChannel(wampClient: WampClient) {
  wampClient.register('public.roll.config.getByChannel', ([channel]: [string]) => {
    return manager().findOne(RollConfig, channel);
  });
}

export function UpdateRollConfig(wampClient: WampClient) {
  wampClient.register(
    'public.roll.config.update',
    ([config]: [RollConfig], kwargs: any, { caller_authid }: Invocation) => {
      config = RollConfig.fromObject(config, caller_authid);
      return manager().save(config);
    }
  );
}

export function CreateRollConfig(wampClient: WampClient) {
  wampClient.register(
    'public.roll.config.create',
    ([channel]: [string], kwargs: any, { caller_authid }: Invocation) => {
      const config = new RollConfig(channel, caller_authid);
      return manager().save(config);
    }
  );
}
