import { db } from '../helpers/db';
import { WampClient } from '../../client/services/WampClient';
import { Invocation } from '../WampClient';
import { FightConfig } from '../entities';

function manager() {
  return db().manager;
}

export function GetFightConfigByChannel(wampClient: WampClient) {
  wampClient.register('public.fight.config.getByChannel', ([channel]: [string]) => {
    return manager().findOne(FightConfig, channel);
  });
}

export function UpdateFightConfig(wampClient: WampClient) {
  wampClient.register(
    'public.fight.config.update',
    ([config]: [FightConfig], kwargs: any, { caller_authid }: Invocation) => {
      config = FightConfig.fromObject(config, caller_authid);
      return manager().save(config);
    }
  );
}

export function CreateFightConfig(wampClient: WampClient) {
  wampClient.register(
    'public.fight.config.create',
    ([channel]: [string], kwargs: any, { caller_authid }: Invocation) => {
      const config = new FightConfig(channel, caller_authid);
      return manager().save(config);
    }
  );
}
