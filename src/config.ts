export const WampOptions = {
  url: 'ws://crossbar:8080/ws',
  realm: 'chatbot',
  authid: '1',
  authmethods: ['wampcra'],
  onchallenge: (session: any, method: string, extra?: any): string => {
    return '';
  }
};
