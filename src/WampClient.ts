import { WampClient, WampChallenge } from 'mumba-wamp';
import { WampOptions } from './config';
import { log } from './helpers/log';

export interface Invocation {
  procedure: string;
  caller: number;
  caller_authid: string;
  caller_authrole: string;
}

function createWampOptions(secret: string): any {
  const challenge = new WampChallenge();
  const options = { ...WampOptions };
  options.onchallenge = challenge.createWampCra(secret);
  return options;
}

export function makeWampClient(secret: string): WampClient {
  const wampClient = new WampClient(createWampOptions(secret));
  wampClient.onOpen(() => log('WampClient Connected'));
  wampClient.onClose((...args) => log('WampClient closed', ...args));
  wampClient.onError((...args) => log('Error:', ...args));
  wampClient.onRegister(details => log('Registered endpoint', details.procedure));
  log('Opening socket connection');
  wampClient.openConnection();
  return wampClient;
}
