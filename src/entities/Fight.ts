import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity()
export class Fight {
  @PrimaryColumn()
  public username: string;

  @Column({ type: 'int', default: 0 })
  public wins: number;

  @Column({ type: 'int', default: 0 })
  public losses: number;

  constructor(username: string) {
    this.username = username;
    this.wins = 0;
    this.losses = 0;
  }
}
