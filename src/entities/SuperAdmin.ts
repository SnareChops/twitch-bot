import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity()
export class SuperAdmin {
  @PrimaryColumn()
  public username: string;

  @Column({ type: 'text', nullable: true })
  public notes?: string;

  @Column()
  public createdAt: Date;

  constructor(username: string) {
    this.username = username;
    this.createdAt = new Date();
  }
}
