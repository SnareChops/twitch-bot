import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity()
export class ModChatMessage {
  @PrimaryColumn({ type: 'int', generated: true })
  public id: number;

  @PrimaryColumn()
  public channel: string;

  @Column()
  public message: string;

  @Column()
  public name: string;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  public timestamp: Date;

  constructor(channel: string, message: string, name: string) {
    this.channel = channel;
    this.message = message;
    this.name = name;
  }
}
