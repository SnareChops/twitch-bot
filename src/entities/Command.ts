import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity()
export class Command {
  public static from(command: Command, user: string) {
    const result = new Command(command.channel, command.name, command.response, user);
    for (const key in command) {
      result[key] = command[key];
    }
    return result;
  }

  @PrimaryColumn()
  public channel: string;

  @PrimaryColumn()
  public name: string;

  @Column()
  public response: string;

  @Column({ type: Boolean, default: true })
  public enabled: boolean = true;

  @Column({ type: Boolean, default: false })
  public restricted: boolean = false;

  @Column({ type: 'int', default: 0 })
  public cooldown: number = 0; // seconds

  @Column({ nullable: true })
  public description?: string;

  @Column({ type: Boolean, default: false })
  public scheduled: boolean = false;

  @Column({ type: 'int', default: 15 })
  public schedule: number = 15;

  @Column()
  public createdBy: string;

  @Column()
  public createdAt: Date;

  constructor(channel: string, name: string, response: string, createdBy: string) {
    this.channel = channel;
    this.name = name;
    this.response = response;
    this.createdBy = createdBy;
    this.createdAt = new Date();
  }
}
