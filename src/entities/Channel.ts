import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity()
export class Channel {
  @PrimaryColumn()
  public name: string;

  @Column({ type: 'text', nullable: true })
  public notes?: string;

  @Column({ type: 'date', default: /* istanbul ignore next */ () => 'CURRENT_TIMESTAMP' })
  public createdAt: Date;

  constructor(name: string, notes?: string) {
    this.name = name;
    this.notes = notes;
  }
}
