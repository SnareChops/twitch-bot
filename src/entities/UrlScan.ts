import { Entity, PrimaryColumn, Column } from 'typeorm';
import { PermissionLevel } from '../types/PermissionLevel';

@Entity()
export class UrlScan {
  public static fromObject(config: UrlScan, user: string) {
    const result = new UrlScan(config.channel, user);
    for (const key in config) {
      result[key] = config[key];
    }
    return result;
  }
  @PrimaryColumn()
  public channel: string;

  @Column({ type: Boolean, default: true })
  public enabled: boolean = true;

  @Column({ type: 'int', default: PermissionLevel.MOD })
  public permit: PermissionLevel = PermissionLevel.MOD;

  @Column({ type: Boolean, default: true })
  public useWhitelist: boolean = true;

  @Column({ type: 'varchar', default: 'youtube.com,youtu.be,spotify.com,twitch.tv' })
  public whitelist: string = 'youtube.com,youtu.be,spotify.com,twitch.tv';

  @Column({ type: 'int', default: 5 })
  public timeout: number = 5;

  @Column({ type: 'date', default: /* istanbul ignore next */ () => 'CURRENT_TIMESTAMP' })
  public createdAt: Date;

  @Column()
  public updatedBy: string;

  @Column({ type: 'date', default: /* istanbul ignore next */ () => 'CURRENT_TIMESTAMP' })
  public updatedAt: Date;

  constructor(channel: string, actor: string) {
    this.channel = channel;
    this.updatedBy = actor;
  }
}
