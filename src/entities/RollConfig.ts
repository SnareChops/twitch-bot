import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity()
export class RollConfig {
  public static fromObject(config: RollConfig, user: string) {
    const result = new RollConfig(config.channel, user);
    for (const key in config) {
      result[key] = config[key];
    }
    return result;
  }

  @PrimaryColumn()
  public channel: string;

  @Column({ type: Boolean, default: true })
  public enabled: boolean;

  @Column({ type: 'int', default: 60 })
  public cooldown: number; // seconds

  @Column({ type: 'date', default: /* istanbul ignore next */ () => 'CURRENT_TIMESTAMP' })
  public createdAt: Date;

  @Column()
  public updatedBy: string;

  @Column({ type: 'date', default: /* istanbul ignore next */ () => 'CURRENT_TIMESTAMP' })
  public updatedAt: Date;

  constructor(channel: string, actor: string) {
    this.channel = channel;
    this.updatedBy = actor;
  }
}
