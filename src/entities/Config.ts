import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity()
export class Config {
  public static fromObject(config: Config, user: string) {
    const result = new Config(config.channel, user);
    for (const key in config) {
      result[key] = config[key];
    }
    return result;
  }

  @PrimaryColumn()
  public channel: string;

  @Column({ type: Boolean, default: true })
  public fightEnabled: boolean;

  @Column({ type: 'int', default: 60 })
  public fightCooldown: number; // seconds

  @Column({ type: Boolean, default: true })
  public rollEnabled: boolean;

  @Column({ type: 'int', default: 60 })
  public rollCooldown: number; // seconds

  @Column({ type: 'date', default: /* istanbul ignore next */ () => 'CURRENT_TIMESTAMP' })
  public createdAt: Date;

  @Column()
  public updatedBy: string;

  @Column({ type: 'date', default: /* istanbul ignore next */ () => 'CURRENT_TIMESTAMP' })
  public updatedAt: Date;

  constructor(channel: string, actor: string) {
    this.channel = channel;
    this.updatedBy = actor;
  }
}
