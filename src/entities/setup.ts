import { Connection, createConnection } from 'typeorm';
import { Command } from './Command';
import { Mod } from './Mod';
import { ModChatMessage } from './ModChatMessage';
import { Fight } from './Fight';
import { container } from '../container';
import { DB_USER, DB_PWORD, DB_ADDRESS } from '../di.tokens';
import { Config } from './Config';
import { Channel } from './Channel';
import { SuperAdmin } from './SuperAdmin';
import { UrlScan } from './UrlScan';
import { log } from '../helpers/log';
import { FightConfig } from './FightConfig';
import { RollConfig } from './RollConfig';

export async function setupDb(): Promise<Connection> {
  /* istanbul ignore next */
  const connection = await createConnection({
    type: 'mysql',
    host: container.get<string>(DB_ADDRESS),
    port: 3306,
    username: container.get<string>(DB_USER),
    password: container.get<string>(DB_PWORD),
    database: 'typeorm',
    entities: [Channel, Command, Mod, ModChatMessage, Fight, FightConfig, RollConfig, UrlScan, Config, SuperAdmin],
    synchronize: false,
    migrationsRun: true,
    migrations: ['dist/src/migrations/**/*.js'],
    logging: false
  });
  /* istanbul ignore next */
  log('Running migrations...');
  /* istanbul ignore next */
  await connection.runMigrations();
  /* istanbul ignore next */
  log('Migrations complete...');
  /* istanbul ignore next */
  return connection;
}
