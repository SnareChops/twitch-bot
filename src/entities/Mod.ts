import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity()
export class Mod {
  @PrimaryColumn()
  public channel: string;

  @PrimaryColumn()
  public username: string;

  @Column()
  public createdAt: Date;

  constructor(channel: string, username: string) {
    this.channel = channel;
    this.username = username;
    this.createdAt = new Date();
  }
}
