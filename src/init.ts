import { container } from './container';
import { BOT_USERNAME, AUTH_TOKEN, DB_USER, DB_PWORD, DB_ADDRESS, CROSSBAR_SECRET, FIGHT_COOLDOWN } from './di.tokens';
import './helpers/init';

container.bind(BOT_USERNAME).toConstantValue(process.env.BOT_USERNAME);
container.bind(AUTH_TOKEN).toConstantValue(process.env.AUTH_TOKEN);
container.bind(DB_ADDRESS).toConstantValue(process.env.DB_ADDRESS);
container.bind(DB_USER).toConstantValue(process.env.DB_USER);
container.bind(DB_PWORD).toConstantValue(process.env.DB_PWORD);
container.bind(CROSSBAR_SECRET).toConstantValue(process.env.CROSSBAR_SECRET);
