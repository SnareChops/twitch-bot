declare module 'tmi.js' {
  export interface Userstate {
    badges: {
      broadcaster: string;
      mod: string;
      [key: string]: string;
    };
    color: string;
    'display-name': string;
    emotes: { [key: string]: string[] };
    mod: boolean;
    'room-id': string;
    subscriber: boolean;
    turbo: boolean;
    'user-id': string;
    'emotes-raw': string;
    'badges-raw': string;
    username: string;
    'message-type': string;
  }

  export interface ClientOptions {
    identity: {
      username: string;
      password: string;
    };
    channels: string[];
  }

  export class client {
    constructor(options: ClientOptions);

    getChannels(): Promise<string[]>;
    getOptions(): Promise<object>;
    getUsername(): Promise<string>;
    isMod(channel: string, username: string): Promise<boolean>;
    readyState(): Promise<'CONNECTING' | 'OPEN' | 'CLOSING' | 'CLOSED'>;
    on(event: 'action', callback: (channel: string, userstate: Userstate, message: string, self: boolean) => any): void;
    on(event: 'ban', callback: (channel: string, username: string, reason: string | null) => any): void;
    on(event: 'chat', callback: (channel: string, userstate: Userstate, message: string, self: boolean) => any): void;
    on(event: 'cheer', callback: (channel: string, userstate: Userstate, message: string) => any): void;
    on(event: 'clearchat', callback: (channel: string) => any): void;
    on(event: 'connected', callback: (address: string, port: number) => any): void;
    on(event: 'disconnected', callback: (reason: string) => any): void;
    on(event: 'emoteonly', callback: (channel: string, enabled: boolean) => any): void;
    on(event: 'emotesets', callback: (sets: string, obj: object) => any): void;
    on(event: 'followersonly', callback: (channel: string, enabled: boolean, length: number) => any): void;
    on(event: 'hosted', callback: (channel: string, username: string, viewers: number, autohost: boolean) => any): void;
    on(event: 'hosting', callback: (channel: string, target: string, viewers: number) => any): void;
    on(event: 'join', callback: (channel: string, username: string, self: boolean) => any): void;
    on(event: 'logon', callback: () => any): void;
    on(
      event: 'message',
      callback: (channel: string, userstate: Userstate, message: string, self: boolean) => any
    ): void;
    on(event: 'mod', callback: (channel: string, username: string) => any): void;
    on(event: 'notice', callback: (channel: string, msgid: string, message: string) => any): void;
    on(event: 'part', callback: (channel: string, username: string, self: boolean) => any): void;
    on(event: 'ping', callback: () => any): void;
    on(event: 'pong', callback: (latency: number) => any): void;
    on(event: 'r9kbeta', callback: (channel: string, enabled: boolean) => any): void;
    on(event: 'reconnect', callback: () => any): void;
    on(
      event: 'resub',
      callback: (
        channel: string,
        username: string,
        months: number,
        message: string,
        userstate: Userstate,
        methods: any
      ) => any
    ): void;
    on(event: 'roomstate', callback: (channel: string, state: any) => any): void;
    on(event: 'serverchange', callback: (channel: string) => any): void;
    on(event: 'slowmode', callback: (channel: string, enabled: boolean, length: number) => any): void;
    on(event: 'subscribers', callback: (channel: string, enabled: boolean) => any): void;
    on(
      event: 'subscription',
      callback: (channel: string, username: string, method: any, message: string, userstate: Userstate) => any
    ): void;
    on(event: 'timeout', callback: (channel: string, username: string, reason: string, duration: number) => any): void;
    on(event: 'unhost', callback: (channel: string, viewers: number) => any): void;
    on(event: 'unmod', callback: (channel: string, username: string) => any): void;
    on(event: 'whisper', callback: (from: string, userstate: Userstate, message: string, self: boolean) => any): void;
    action(channel: string, message: string): Promise<[string]>; // [channel]
    ban(channel: string, username: string, reason?: string): Promise<[string, string, string?]>; // [channel, username, reason]
    clear(channel: string): Promise<[string]>; // [channel]
    color(color: string): Promise<[string]>; // [color]
    commercial(channel: string, seconds: number): Promise<[string, number]>; // [channel, seconds]
    connect(): Promise<[string, number]>; // [server, port]
    disconnect(): Promise<[string, number]>; // [server, port]
    emoteonly(channel: string): Promise<[string]>; // [channel]
    emoteonlyoff(channel: string): Promise<[string]>; // [channel]
    followersonly(channel: string, length: number): Promise<[string, number]>; // [channel, minutes]
    followersonlyoff(channel: string): Promise<[string]>; // [channel]
    host(channel: string, target: string): Promise<[string, string]>; // [channel, target]
    join(channel: string): Promise<[string]>; // [channel]
    mod(channel: string, username: string): Promise<[any]>; // [channel, username]
    mods(channel: string): Promise<[any]>; // [moderators]
    part(channel: string): Promise<[string]>; // [channel]
    ping(): Promise<[number]>; // latency
    r9kbeta(channel: string): Promise<[string]>; // [channel]
    r9kbetaoff(channel: string): Promise<[string]>; // [channel]
    raw(message: string): Promise<[string]>; // [message]
    say(channel: string, message: string): Promise<[string]>; // [channel]
    slow(channel: string, length: number): Promise<[string]>; // [channel]
    slowoff(channel: string): Promise<[string]>; // [channel]
    subscribers(channel: string): Promise<[string]>; // [channel]
    subscribersoff(channel: string): Promise<[string]>; // [channel]
    timeout(
      channel: string,
      username: string,
      length?: number,
      reason?: string
    ): Promise<[string, string, number, string?]>; // [channel, username, seconds, reason]
    unban(channel: string, username: string): Promise<[string, string]>; // [channel, username]
    unhost(channel: string): Promise<[string]>; // [channel]
    unmod(channel: string, username: string): Promise<[string, string]>; // [channel, username]
    whisper(channel: string, message: string): Promise<[string, string]>; // [channel, message]
  }
}
