import { DateTime } from 'luxon';
import { FeatureRunner } from './FeatureRunner';
import { Command } from '../entities/Command';

export class ScheduleRunner extends FeatureRunner {
  public schedule = new Map<DateTime, Command>();
  public interval: NodeJS.Timeout;
  public time: number = 1000 * 30;

  public async start() {
    await this.load();
    this.run();
  }

  public stop() {
    clearInterval(this.interval);
    this.schedule.clear();
  }

  public add(command: Command) {
    this.remove(command.name);
    this.schedule.set(DateTime.local().plus({ minutes: command.schedule }), command);
  }

  public remove(name: string) {
    for (const [key, value] of this.schedule.entries()) {
      if (name === value.name) {
        this.schedule.delete(key);
      }
    }
  }

  private async load() {
    this.schedule.clear();
    const scheduled = await this.find(Command, { where: { scheduled: true } });
    scheduled.forEach(x => {
      this.schedule.set(DateTime.local().plus({ minutes: Math.random() * x.schedule }), x);
    });
  }

  private async run() {
    this.interval = setInterval(async () => {
      for (const [key, value] of this.schedule.entries()) {
        if (key < DateTime.local()) {
          this.schedule.delete(key);
          this.schedule.set(DateTime.local().plus({ minutes: value.schedule }), value);
          await this.speak(value);
        }
      }
    }, this.time);
  }

  private async speak(command: Command) {
    return this.say(command.channel, command.response);
  }
}
