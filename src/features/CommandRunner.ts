import { DateTime } from 'luxon';
import { Userstate } from 'tmi.js';
import { Command } from '../entities';
import { FeatureRunner } from './FeatureRunner';

export class CommandRunner extends FeatureRunner {
  public cooldowns = new Map<string, DateTime>();

  /**
   * Runs a chat command
   *
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string} name
   * @param {string[]} args
   * @returns
   */
  public async run(target: string, context: Userstate, name: string, args: string[]) {
    const username = context['display-name'];
    const instance = await this.findCommand(target, name);
    if (!instance) {
      return;
    }
    const mod = await this.isMod(target, context);
    if (!instance.restricted || mod) {
      if (this.cooldowns.has(name) && this.cooldowns.get(name) > DateTime.local()) {
        return this.say(target, `${username}, command is on cooldown.`);
      }
      if (instance instanceof Command) {
        this.cooldowns.set(name, DateTime.local().plus({ seconds: instance.cooldown }));
        return this.execute(instance, target, context, args);
      }
      return !!instance.invoke ? instance.invoke(target, context, args) : /* istanbul ignore next */ void 0;
    }
    return this.say(target, `${username}, you do not have permissions to run that command.`);
  }

  /**
   * Returns [command, args]
   *
   * @export
   * @param {string} message
   * @returns {([string, string] | void)}
   */
  public detect(message: string): [string, string[]] | void {
    const match = /^(![\w-]*)(.*)/i.exec(message);
    if (!match) {
      return void 0;
    }
    const name = match[1]
      .trim()
      .slice(1)
      .toLowerCase();
    const args = !!match[2] ? match[2].trim().split(' ') : /* istanbul ignore next */ void 0;

    return !!name ? [name, args] : /* istanbul ignore next */ void 0;
  }

  private execute(command: Command, target: string, context: Userstate, args: string[]) {
    if (command.enabled) {
      const message = this.replace(command.response, context, args);
      return this.say(target, message);
    }
  }

  private replace(message: string, context: Userstate, args: string[]): string {
    // TODO: Test this
    return message.replace(/\$user/, !!args && !!args[0] ? args[0] : context['display-name']);
  }
}
