import { Userstate } from 'tmi.js';
import { db } from '../helpers/db';
import { container } from '../container';
import { Client } from '../Client';
import { command } from '../helpers/command';
import { isMod } from '../helpers/isMod';
import { ObjectType, ObjectID, FindManyOptions } from 'typeorm';
import { PermissionLevel } from '../types/PermissionLevel';

export abstract class FeatureRunner {
  protected get db() {
    return db();
  }

  protected get client() {
    return container.get<Client>(Client);
  }

  public say(channel: string, message: string) {
    return this.client.action(channel, message);
  }

  public timeout(channel: string, username: string, length: number, reason: string) {
    return this.client.timeout(channel, username, length, reason);
  }

  public async findCommand(channel: string, name: string) {
    return command(channel, name);
  }

  public async find<T>(table: ObjectType<T>, options: FindManyOptions<T>): Promise<T[]> {
    return this.db.manager.find(table, options);
  }

  public async findOne<T>(table: ObjectType<T>, id?: string | number | Date | ObjectID): Promise<T> {
    return this.db.manager.findOne(table, id);
  }

  public async save(...entities: any[]) {
    return this.db.manager.save(entities);
  }

  public isMod(channel: string, context: Userstate) {
    return isMod(channel, context);
  }

  public async getPermissionLevel(context: Userstate): Promise<PermissionLevel> {
    if (!!context.badges && !!context.badges.broadcaster) {
      return PermissionLevel.BROADCASTER;
    }
    if (context.mod) {
      return PermissionLevel.MOD;
    }
    if (!!context.badges && !!context.badges.vip) {
      return PermissionLevel.VIP;
    }
    if (context.subscriber) {
      return PermissionLevel.SUB;
    }
    return PermissionLevel.ALL;
  }

  public async hasPermission(context: Userstate, minimum: PermissionLevel): Promise<boolean> {
    const level = await this.getPermissionLevel(context);
    return level >= minimum;
  }
}
