import { Userstate } from 'tmi.js';
import { FeatureRunner } from './FeatureRunner';
import { UrlScan } from '../entities/UrlScan';

type UrlMap = Map<string, { domain: string; path?: string }>;

export class UrlScanRunner extends FeatureRunner {
  /**
   * Gets a description for a chat command
   *
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string} name
   * @param {string[]} args
   * @returns
   */
  public async run(channel: string, context: Userstate, urls: UrlMap) {
    if (await this.isMod(channel, context)) {
      return;
    }
    const config = await this.findOne(UrlScan, channel);
    if (config.enabled) {
      if (!(await this.hasPermission(context, config.permit))) {
        this.timeout(channel, context.username, config.timeout, 'Posted url');
        return this.say(channel, `${context['display-name']}, you were timed out for posting links.`);
      }
      if (config.useWhitelist) {
        if (!this.checkWhitelist(config, urls)) {
          this.timeout(channel, context.username, config.timeout, 'Posted forbidden url');
          return this.say(channel, `${context['display-name']}, you were timed out for posting a forbidden link.`);
        }
      }
    }
  }

  /**
   * Returns [command, args]
   *
   * @export
   * @param {string} message
   * @returns {([string, string] | void)}
   */
  public detect(message: string): UrlMap {
    const urls = new Map() as UrlMap;

    const regex = /(?:http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?([a-z0-9]+(?:[\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5})(?::[0-9]{1,5})?(\/.*)?/gi;
    let match: RegExpExecArray;
    // tslint:disable-next-line
    while ((match = regex.exec(message))) {
      urls.set(match[0], { domain: match[1], path: match[2] });
    }

    if (urls.size === 0) {
      return void 0;
    }

    return urls;
  }

  private checkWhitelist(config: UrlScan, urls: UrlMap): boolean {
    const whitelist = config.whitelist.split(',').map(x => x.trim());
    // loop through all found urls in chat message
    for (const [key, value] of urls.entries()) {
      // loop through all whitelisted domains
      for (const item of whitelist) {
        // check for match
        if (value.domain.indexOf(item) !== -1) {
          return true;
        }
      }
    }
    return false;
  }
}
