import { Userstate } from 'tmi.js';
import { Command } from '../entities';
import { BuiltInCommand } from '../builtin/BuiltIn';
import { FeatureRunner } from './FeatureRunner';

export class DescriptionRunner extends FeatureRunner {
  /**
   * Gets a description for a chat command
   *
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string} name
   * @param {string[]} args
   * @returns
   */
  public async run(target: string, context: Userstate, name: string, args: string[]) {
    const username = context['display-name'];
    const instance = await this.findCommand(target, name);
    const mod = await this.isMod(target, context);
    if (!!instance && (mod || !instance.restricted)) {
      if (!args) {
        return this.display(target, username, instance);
      } else {
        if (!mod) {
          return this.say(target, `${username}, You are not allowed to change command descriptions.`);
        } else {
          return this.edit(target, username, name, instance, args);
        }
      }
    }
    if (!instance) {
      return this.say(target, `${username}, !${name} does not exist.`);
    }
  }

  /**
   * Returns [command, args]
   *
   * @export
   * @param {string} message
   * @returns {([string, string] | void)}
   */
  public detect(message: string): [string, string[]] | void {
    const match = /^(\?[\w-]*)(.*)/i.exec(message);
    if (!match) {
      return void 0;
    }
    const name = match[1]
      .trim()
      .slice(1)
      .toLowerCase();
    const args = !!match[2] ? match[2].trim().split(' ') : /* istanbul ignore next */ void 0;
    return !!name ? [name, args] : /* istanbul ignore next */ void 0;
  }

  private async display(target: string, username: string, instance: BuiltInCommand | Command) {
    if (!!instance.description) {
      return this.say(target, `${username}, ${instance.description}`);
    }
  }

  private async edit(
    target: string,
    username: string,
    name: string,
    instance: BuiltInCommand | Command,
    args: string[]
  ) {
    if (instance instanceof Command) {
      // Save command description to db
      instance.description = args.join(' ');
      await this.save(instance);

      return this.say(target, `!${name} description updated.`);
    }
    return this.say(target, `${username}, built-in command descriptions cannot be edited.`);
  }
}
