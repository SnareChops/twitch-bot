import { Userstate } from 'tmi.js';
import { FeatureRunner } from './FeatureRunner';

export type Operators = '+' | '-' | '*' | '/';
export class MathRunner extends FeatureRunner {
  /**
   * Responds with the answer to the math equation entered
   *
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {number} left
   * @param {number} right
   * @param {Operators} operator
   * @returns
   */
  public async run(target: string, context: Userstate, left: number, right: number, operator: Operators) {
    const username = context['display-name'];
    return this.say(target, `${username}: ${this.calculate(left, right, operator)}`);
  }

  /**
   * Returns [left, right, operator] if a math statement is detected
   *
   * @export
   * @param {string} message
   * @returns {([number, number, string] | void)}
   */
  public detect(message: string): [number, number, string] | void {
    const match = /(\d+)\s*([+\-*/])\s*(\d+)/.exec(message);
    return !!match ? [+match[1].trim(), +match[3].trim(), match[2].trim()] : void 0;
  }

  public calculate(left: number, right: number, operator: Operators): number {
    switch (operator) {
      case '+':
        return left + right;
      case '-':
        return left - right;
      case '*':
        return left * right;
      case '/':
        return left / right;
    }
  }
}
