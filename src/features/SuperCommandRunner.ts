import { Userstate } from 'tmi.js';
import { SuperAdmin } from '../entities';
import { FeatureRunner } from './FeatureRunner';
import { BOT_USERNAME, SUPER_COMMANDS } from '../di.tokens';
import { container } from '../container';
import { SuperCommand } from '../builtin/supers/SuperCommand';

export class SuperCommandRunner extends FeatureRunner {
  public superCommands = container.get<Map<string, SuperCommand>>(SUPER_COMMANDS);

  /**
   * Runs a super command
   *
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string} name
   * @param {string[]} args
   * @returns
   */
  public async run(target: string, context: Userstate, name: string, args: string[]) {
    const allowed = !!(await this.db.manager.findOne(SuperAdmin, context.username));
    if (allowed) {
      return this.superCommands.get(name).invoke(target, args);
    }
  }

  /**
   * Returns [name, args]
   *
   * @export
   * @param {string} message
   * @returns {([string, string[]] | void)}
   */
  public detect(message: string): [string, string[]] | void {
    const regex = new RegExp(`^@?${container.get<string>(BOT_USERNAME).toLowerCase()}\\s+(\\w+)\\s+(.*)`, 'i');
    const match = regex.exec(message);
    if (!match) {
      return void 0;
    }
    if (!match[1] || (!!match[1] && !this.superCommands.has(match[1]))) {
      return void 0;
    }
    const args = !!match[2] ? match[2].trim().split(' ') : /* istanbul ignore next */ void 0;
    return !!match[1] ? [match[1], args] : /* istanbul ignore next */ void 0;
  }
}
