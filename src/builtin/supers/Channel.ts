import { injectable } from 'inversify';
import { Super, SuperCommand } from './SuperCommand';
import { Channel as ChannelModel, Config, FightConfig, RollConfig, Mod, UrlScan } from '../../entities';

@Super({
  name: 'channel'
})
@injectable()
export class Channel extends SuperCommand {
  public async invoke(target: string, args: string[]): Promise<any> {
    switch (args[0]) {
      case 'add':
        return this.add(target, args.slice(1));
      case 'remove':
        return this.remove(target, args.slice(1));
      default:
        return this.say(target, `Unrecognized subcommand ${args[0]}`);
    }
  }

  public async add(target: string, args: string[]) {
    const name = args[0];
    const invalid = this.validate(name);
    if (!!invalid) {
      return this.say(target, invalid);
    }
    // add channel to Channel
    const channel = new ChannelModel(name);
    // Add default fight_config
    const fightConfig = new FightConfig(name, 'SuperAdmin');
    // Add default roll_config
    const rollConfig = new RollConfig(name, 'SuperAdmin');
    // Add defult url scan config
    const urlScan = new UrlScan(name, 'SuperAdmin');
    // Add channel owner as mod
    const mod = new Mod(name, name.replace('#', ''));

    try {
      await this.save([channel, fightConfig, rollConfig, urlScan, mod]);
      await this.say(target, `Channel ${name} added.`);
    } catch (e) {
      await this.say(target, e.message);
    }
  }

  private async remove(target: string, args: string[]) {
    const name = args[0];
    const invalid = this.validate(name);
    if (!!invalid) {
      return this.say(target, invalid);
    }

    try {
      const channel = await this.findOne(ChannelModel, name);
      const fightConfig = await this.findOne(FightConfig, name);
      const rollConfig = await this.findOne(RollConfig, name);
      const urlScan = await this.findOne(UrlScan, name);
      const mods = await this.find(Mod, { where: { channel: name } });

      await this.removeAll([...mods, urlScan, rollConfig, fightConfig, channel]);
      return this.say(target, `Channel ${name} removed.`);
    } catch (e) {
      return this.say(target, e.message);
    }
  }

  private validate(name: string): string {
    if (!name) {
      return 'No channel name specified.';
    }
    if (name.indexOf('#') !== 0) {
      return 'Channel name must start with #';
    }
    return void 0;
  }
}
