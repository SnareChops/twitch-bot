import { container } from '../../container';
import { SUPER_COMMANDS } from '../../di.tokens';
import { Client } from '../../Client';
import { injectable } from 'inversify';
import { log } from '../../helpers/log';
import { Connection, SaveOptions, ObjectType, ObjectID, FindOneOptions, RemoveOptions, FindManyOptions } from 'typeorm';
import { db } from '../../helpers/db';

export interface SuperCommandMetadata {
  name: string;
}

export function Super(metadata: SuperCommandMetadata): ClassDecorator {
  return (target: Function) => {
    Reflect.defineMetadata('super:metadata', metadata, target);
    container.bind(target).toSelf();
    if (!container.isBound(SUPER_COMMANDS)) {
      container.bind(SUPER_COMMANDS).toConstantValue(new Map());
    }
    log('Registering super command', metadata);
    container.get<Map<string, SuperCommand>>(SUPER_COMMANDS).set(metadata.name, container.get(target));
  };
}

@injectable()
export abstract class SuperCommand {
  protected get db() {
    return db();
  }

  protected get client() {
    return container.get<Client>(Client);
  }

  public abstract async invoke(target: string, args: string[]): Promise<any>;

  public async say(channel: string, message: string) {
    return this.client.action(channel, message);
  }

  public async findOne<T>(
    table: ObjectType<T>,
    id?: string | number | Date | ObjectID,
    options?: FindOneOptions
  ): Promise<T> {
    return this.db.manager.findOne<T>(table, id, options);
  }

  public async find<T>(table: ObjectType<T>, options?: FindManyOptions): Promise<T[]> {
    return this.db.manager.find<T>(table, options);
  }

  public async save<T>(entities: T[], options?: SaveOptions): Promise<T[]> {
    return this.db.manager.save(entities, options);
  }

  public async removeEntity<T>(entity: T, options?: RemoveOptions): Promise<T> {
    return this.db.manager.remove(entity, options);
  }

  public async removeAll<T>(entities: T[], options?: RemoveOptions): Promise<T[]> {
    return Promise.all(entities.map(x => this.removeEntity(x, options)));
  }
}
