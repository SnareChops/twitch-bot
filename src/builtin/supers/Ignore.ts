import { injectable } from 'inversify';
import { SuperCommand, Super } from './SuperCommand';

@Super({
  name: 'ignore'
})
@injectable()
export class Ignore extends SuperCommand {
  public async invoke(target: string, args: string[]): Promise<any> {
    switch (args[0]) {
      case 'add':
        return this.add(target, args.slice(1));
      case 'list':
        return this.list(target);
      case 'remove':
        return this.remove(target, args.slice(1));
      default:
        return this.say(target, `Unrecognized subcommand ${args[0]}`);
    }
  }

  public async add(target: string, args: string[]) {
    this.client.ignoreList.add(args[0]);
    return this.say(target, `Added ${args[0]} to channel ignore list`);
  }

  public async list(target: string) {
    return this.say(target, `Ignored channels: ${Array.from(this.client.ignoreList.keys()).join(', ')}`);
  }

  public async remove(target: string, args: string[]) {
    this.client.ignoreList.delete(args[0]);
    return this.say(target, `Removed ${args[0]} from channel ignore list`);
  }
}
