import { Userstate } from 'tmi.js';
import { Client } from '../Client';

export class CommandDefinition {
  constructor(
    public value: string | FunctionalCommand,
    public restricted = false,
    public visible = true,
    public cooldown = 0,
    public description?: string
  ) {}
}

export type FunctionalCommand = (client: Client, target: string, context: Userstate, args: string[]) => any;
export type BuiltIn = Map<string, CommandDefinition>;
