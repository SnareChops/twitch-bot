import { Userstate } from 'tmi.js';
import { injectable } from 'inversify';
import { Command } from '../../entities';
import { BuiltInCommand, BuiltIn } from '../BuiltIn';

@injectable()
@BuiltIn({
  name: 'add',
  description: 'Adds or updates a command. Usage: !add !<command name> <response text>',
  restricted: true
})
export class Add extends BuiltInCommand {
  /**
   * !add <command name> <response text>
   * Adds / Updates a chat command
   * @export
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   */
  public async invoke(target: string, context: Userstate, args: string[]) {
    const creator = context['display-name'];
    const invalid = await this.validate(target, args);
    if (!!invalid) {
      return this.say(target, `${creator}, ${invalid}`);
    }
    const name = args[0].slice(1).toLowerCase();
    const response = args.slice(1).join(' ');
    await this.save(new Command(target, name, response, creator));
    await this.say(target, `!${name} added/updated.`);
  }

  /**
   * Validates usage of the !add command
   * Returns reason if not a valid usage
   *
   * @param {string[]} args
   * @returns {[boolean, string]}
   */
  public async validate(channel: string, args: string[]): Promise<string> {
    if (!args[0].startsWith('!')) {
      return 'Command name must start with !';
    }
    if (!/!\w+/.test(args[0])) {
      return 'Command name should contain at least 1 letter or number.';
    }
    if (/!.*[!?.,[\]{}%$#@^&*()_+\-=/|\/<>:;'"~`]+.*/.test(args[0])) {
      return 'Command name should not include any additional special characters.';
    }
    if (args.length === 1) {
      return 'Command should have a response.';
    }
    const existing = await this.findCommand(channel, args[0].slice(1));
    if (!!existing && !(existing instanceof Command)) {
      return 'Cannot overwrite built-in commands.';
    }
  }
}
