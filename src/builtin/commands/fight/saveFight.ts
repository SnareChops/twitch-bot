import { db } from '../../../helpers/db';
import { Fight } from '../../../entities';

/**
 * Saves a fight result into the fight history
 *
 * @param winner
 * @param loser
 */
export async function saveFight(winner: string, loser: string): Promise<any> {
  const win = await findOrCreate(winner);
  const loss = await findOrCreate(loser);
  win.wins += 1;
  loss.losses += 1;
  return db().manager.save([win, loss]);
}

async function findOrCreate(username: string): Promise<Fight> {
  let result = await db().manager.findOne(Fight, username);
  if (!result) {
    result = new Fight(username);
  }
  return result;
}
