import { injectable } from 'inversify';
import { Userstate } from 'tmi.js';
import { rematches, fights } from './state';
import { BuiltIn, BuiltInCommand } from '../../BuiltIn';

@injectable()
@BuiltIn({
  name: 'rematch',
  description: void 0,
  visible: false
})
export class Rematch extends BuiltInCommand {
  /**
   * !rematch
   * Converts a finished fight into a new pending fight
   *
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   * @returns
   */
  public async invoke(target: string, context: Userstate, args: string[]) {
    const rematcher = context['display-name'].toLowerCase();
    const challenged = rematches.get(rematcher);
    if (!!challenged) {
      rematches.delete(rematcher);
      fights.set(challenged, rematcher);
      return this.say(
        target,
        `${challenged}, you have been challenged to a rematch by ${rematcher}, "!accept" or "!decline"?`
      );
    }
  }
}
