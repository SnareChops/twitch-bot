import { DateTime } from 'luxon';
import { Userstate } from 'tmi.js';
import { injectable } from 'inversify';
import { fights, cooldowns } from './state';
import { BuiltIn, BuiltInCommand } from '../../BuiltIn';
import { FightConfig } from '../../../entities';

@injectable()
@BuiltIn({
  name: 'fight',
  description: 'Challenge someone to a fight! Usage: !fight <username>'
})
export class Fight extends BuiltInCommand {
  /**
   * Allows disabling of fighting
   *
   * @param channel
   */
  public async enabled(channel: string): Promise<boolean> {
    return (await this.findOne(FightConfig, { channel })).enabled;
  }

  /**
   * !fight <username>
   * Starts a pending fight with <username>
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   * @returns
   */
  public async invoke(channel: string, context: Userstate, args: string[]) {
    if (!(await this.enabled(channel))) {
      return;
    }
    const invalid = this.validate(args, context);
    if (!!invalid) {
      return this.say(channel, `${context['display-name']}, ${invalid}`);
    }
    const challenged = args[0].toLowerCase().replace('@', '');
    const challenger = context.username;

    fights.set(challenged, challenger);
    const config = await this.findOne(FightConfig, { channel });
    cooldowns.set(challenger, DateTime.local().plus({ seconds: config.cooldown }));
    return this.say(
      channel,
      `${challenged}, you have been challenged to a fight by ${challenger}, do you "!accept" or "!decline"?`
    );
  }

  public validate(args: string[], context: Userstate): string {
    if (!args) {
      return 'You must choose someone to fight.';
    }
    if (args[0].toLowerCase() === context.username) {
      return 'You cannot fight yourself.';
    }
    if (cooldowns.has(context.username) && cooldowns.get(context.username) > DateTime.local()) {
      return 'Command is on cooldown.';
    }
  }
}
