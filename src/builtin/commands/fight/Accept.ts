import { Userstate } from 'tmi.js';
import { fights, rematches } from './state';
import { saveFight } from './saveFight';
import { injectable } from 'inversify';
import { BuiltIn, BuiltInCommand } from '../../BuiltIn';

@injectable()
@BuiltIn({
  name: 'accept',
  description: void 0,
  visible: false
})
export class Accept extends BuiltInCommand {
  /**
   * !accept
   * Accepts a pending fight
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   * @returns
   */
  public async invoke(target: string, context: Userstate, args: string[]) {
    const self = this;
    const challenged = context.username;
    const challenger = fights.get(challenged);
    if (!!challenger) {
      return Math.random() > 0.5 ? win(challenger, challenged) : win(challenged, challenger);
    }
    async function win(winner: string, loser: string) {
      fights.delete(challenged);
      rematches.set(loser, winner);
      return Promise.all([self.say(target, `${winner} wins! ${loser} "!rematch"?`), self.saveFight(winner, loser)]);
    }
  }

  public async saveFight(winner: string, loser: string): Promise<any> {
    return saveFight(winner, loser);
  }
}
