import { Userstate } from 'tmi.js';
import { injectable } from 'inversify';
import { fights } from './state';
import { BuiltIn, BuiltInCommand } from '../../BuiltIn';

@injectable()
@BuiltIn({
  name: 'decline',
  description: void 0,
  visible: false
})
export class Decline extends BuiltInCommand {
  /**
   * !decline
   * Declines a pending fight
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   * @returns
   */
  public async invoke(target: string, context: Userstate, args: string[]) {
    const challenged = context['display-name'].toLowerCase();
    const challenger = fights.get(challenged);
    if (!!challenger) {
      fights.delete(challenged);
      return this.say(target, `Maybe next time ${challenger}`);
    }
  }
}
