import { Userstate } from 'tmi.js';
import { injectable } from 'inversify';
import { Fight, FightConfig } from '../../../entities';
import { BuiltIn, BuiltInCommand } from '../../BuiltIn';

@injectable()
@BuiltIn({
  name: 'fightstats',
  description: 'See your win/loss fight stats.'
})
export class FightStats extends BuiltInCommand {
  /**
   * Allows disabling of fighting
   *
   * @param channel
   */
  public async enabled(channel: string): Promise<boolean> {
    return (await this.findOne(FightConfig, { channel })).enabled;
  }

  /**
   * !fightstats
   * Shows the user's win/loss stats
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   * @returns
   */
  public async invoke(target: string, context: Userstate) {
    if (!(await this.enabled(target))) {
      return;
    }
    const username = context.username;
    const fight = await this.findOne(Fight, { username });
    return this.say(target, `${context['display-name']} your win/loss is ${fight.wins}/${fight.losses}`);
  }
}
