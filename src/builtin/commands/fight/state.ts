import { DateTime } from 'luxon';

// <challengee, challenger>
export const fights = new Map<string, string>();
// <loser, winner>
export const rematches = new Map<string, string>();
// <user, cooldown>
export const cooldowns = new Map<string, DateTime>();
