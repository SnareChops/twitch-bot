import { Userstate } from 'tmi.js';
import { Command } from '../../entities';
import { injectable } from 'inversify';
import { BuiltIn, BuiltInCommand } from '../BuiltIn';

@injectable()
@BuiltIn({
  name: 'enable',
  description: 'Enables a command. Usage: !enable !<command name>',
  restricted: true
})
export class Enable extends BuiltInCommand {
  /**
   * !enable <command name>
   * Enables a chat command
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   */
  public async invoke(target: string, context: Userstate, args: string[]) {
    // Get the command name
    const name = args[0].slice(1).toLowerCase();
    // Set the commands to enabled = true in db
    const c = (await this.findCommand(target, name)) as Command;
    if (!c) {
      return this.say(target, `!${name} does not exist.`);
    }
    c.enabled = true;
    await this.save(c);
    // Respond
    return this.say(target, `!${name} enabled!`);
  }
}
