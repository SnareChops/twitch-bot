import { Userstate } from 'tmi.js';
import { Command } from '../../entities/Command';
import { injectable } from 'inversify';
import { BuiltIn, BuiltInCommand } from '../BuiltIn';

@injectable()
@BuiltIn({
  name: 'disable',
  description: 'Disables a command. Usage: !disable !<command name>',
  restricted: true
})
export class Disable extends BuiltInCommand {
  /**
   * !disable <command name>
   * Disables a chat command
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   */
  public async invoke(target: string, context: Userstate, args: string[]) {
    // Get the command name
    const name = args[0].slice(1).toLowerCase();
    // Set the commands to enabled = false in db
    const c = (await this.findCommand(target, name)) as Command;
    if (!c) {
      return this.say(target, `!${name} does not exist.`);
    }
    c.enabled = false;
    await this.save(c);
    // Respond
    return this.say(target, `!${name} disabled!`);
  }
}
