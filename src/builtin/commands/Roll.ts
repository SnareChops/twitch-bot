import { Userstate } from 'tmi.js';
import { injectable } from 'inversify';
import { BuiltIn, BuiltInCommand } from '../BuiltIn';
import { DateTime } from 'luxon';
import { RollConfig } from '../../entities';

@injectable()
@BuiltIn({
  name: 'roll',
  description: 'Rolls a random number between 1 and <number> (or 1 - 10 by default). Usage: !roll <number>'
})
export class Roll extends BuiltInCommand {
  /**
   * Allows disabling of rolls
   *
   * @param channel
   */
  public async enabled(channel: string): Promise<boolean> {
    return (await this.findOne(RollConfig, { channel })).enabled;
  }

  /**
   * !roll <number>
   * Rolls a random number between 1 and <number> (or 1 - 10 by default)
   *
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   * @returns
   */
  public async invoke(channel: string, context: Userstate, args: string[]) {
    if (!(await this.enabled(channel))) {
      return;
    }
    const requestor = context['display-name'];
    let high = 10;
    if (!!args && !!args[0] && !isNaN(+args[0])) {
      high = +args[0];
    }
    const number = Math.floor(Math.random() * high);
    const config = await this.findOne(RollConfig, { channel });
    this.client.commandRunner.cooldowns.set('roll', DateTime.local().plus({ seconds: config.cooldown || 0 }));
    return this.say(channel, `${requestor}: ${number > 0 ? number : 1}`);
  }
}
