import { Userstate } from 'tmi.js';
import { BuiltIn, BuiltInCommand, getBuiltInMeta, BuiltInMetadata } from '../BuiltIn';
import { BUILT_IN } from '../../di.tokens';
import { container } from '../../container';
import { Command } from '../../entities/Command';

@BuiltIn({ name: 'commands', description: 'Lists all commands available to the user.' })
export class Commands extends BuiltInCommand {
  private get builtIns(): Map<string, BuiltInCommand> {
    return container.get(BUILT_IN) || new Map();
  }

  /**
   * !commands
   * Gets a list of available commands
   * List is filtered by users access level
   * @export
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   * @returns
   */
  public async invoke(target: string, context: Userstate) {
    const list = await this.getVisibleCommandNames(target, await this.isMod(target, context));
    return this.say(target, `Available commands: ${list.map(x => `!${x}`).join(' ')}`);
  }

  private async getVisibleCommandNames(channel: string, mod: boolean = false): Promise<string[]> {
    return [...(await this.getBuiltInCommands(channel, mod)), ...(await this.getDBCommands(channel, mod))];
  }

  private async getBuiltInCommands(channel: string, mod: boolean): Promise<string[]> {
    return (await this.getEnabledCommands(channel))
      .map<[string, BuiltInMetadata]>(([_, value]) => [_, getBuiltInMeta(value.constructor)])
      .filter(([_, value]) => value.visible)
      .filter(([_, value]) => mod || !value.restricted)
      .map(([key]) => key);
  }

  private async getDBCommands(channel: string, mod: boolean): Promise<string[]> {
    if (mod) {
      return (await this.find(Command, { where: { channel, enabled: true, scheduled: false } })).map(x => x.name);
    } else {
      return (await this.find(Command, { where: { channel, enabled: true, scheduled: false, restricted: false } })).map(
        x => x.name
      );
    }
  }

  private async getEnabledCommands(channel: string): Promise<[string, BuiltInCommand][]> {
    const result: [string, BuiltInCommand][] = [];
    for (const [_, command] of Array.from(this.builtIns.entries())) {
      if (await command.enabled(channel)) {
        result.push([_, command]);
      }
    }
    return result;
  }
}
