import { Userstate } from 'tmi.js';
import { injectable } from 'inversify';
import { BuiltIn, BuiltInCommand } from '../BuiltIn';
import { Command } from '../../entities/Command';

@injectable()
@BuiltIn({
  name: 'cooldown',
  description: 'Sets a command cooldown. Usage: !cooldown !<command name> <time in seconds>',
  restricted: true
})
export class Cooldown extends BuiltInCommand {
  /**
   * !cooldown <command name> <time in seconds>
   * Sets a chat command cooldown
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   */
  public async invoke(target: string, context: Userstate, args: string[]) {
    const user = context['display-name'];
    const invalid = await this.validate(target, args);
    if (!!invalid) {
      return this.say(target, `${user}, ${invalid}`);
    }
    // Get the command name
    const name = args[0].slice(1).toLowerCase();
    const cooldown = +args[1];
    // Set the command cooldown
    const command = (await this.findCommand(target, name)) as Command;
    if (!!command) {
      command.cooldown = cooldown;
      await this.save(command);
      // Respond
      await this.say(target, `!${name} cooldown set!`);
    }
  }

  /**
   * Validates usage of the !cooldown command
   * Returns reason if not a valid usage
   *
   * @param {string[]} args
   * @returns {[boolean, string]}
   */
  private async validate(channel: string, args: string[]): Promise<string> {
    const existing = await this.findCommand(channel, args[0].slice(1));
    if (!existing) {
      return 'Command does not exist';
    }

    if (typeof args[1] === 'undefined') {
      return 'Must provide a cooldown time.';
    }

    if (isNaN(+args[1])) {
      return 'Cooldown time must be a number.';
    }

    if (+args[1] < -1) {
      return 'Cooldown time must be 0 or greater.';
    }

    if (existing instanceof BuiltInCommand) {
      return 'Cannot set cooldowns on built-in commands.';
    }
  }
}
