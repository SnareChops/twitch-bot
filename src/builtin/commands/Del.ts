import { injectable } from 'inversify';
import { Userstate } from 'tmi.js';
import { Command } from '../../entities/Command';
import { BuiltInCommand, BuiltIn } from '../BuiltIn';

@injectable()
@BuiltIn({
  name: 'del',
  description: 'Deletes a command. Usage: !del !<command name>',
  restricted: true
})
export class Del extends BuiltInCommand {
  /**
   * !del <command name>
   * Deletes a chat command
   * @export
   * @param {Client} client
   * @param {string} target
   * @param {Userstate} context
   * @param {string[]} args
   */
  public async invoke(channel: string, context: Userstate, args: string[]) {
    const invalid = await this.validate(channel, args);
    if (!!invalid) {
      return this.say(channel, `${context['display-name']}, ${invalid}`);
    }
    const name = args[0].slice(1).toLowerCase();
    await this.db.manager.delete(Command, { channel, name });
    await this.say(channel, `!${name} deleted!`);
  }

  /**
   * Validates usage of the !add command
   * Returns reason if not a valid usage
   *
   * @param {string[]} args
   * @returns {[boolean, string]}
   */
  public async validate(channel: string, args: string[]): Promise<string> {
    if (!args || args.length === 0) {
      return 'Must specify command to delete';
    }
    if (!args[0].startsWith('!')) {
      return 'Command name must start with !';
    }
    const command = await this.findCommand(channel, args[0].slice(1));
    if (!command) {
      return "Command doesn't exist";
    }

    if (command instanceof BuiltInCommand) {
      return 'Cannot delete built-in commands.';
    }
  }
}
