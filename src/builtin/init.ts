import './commands/Add';
import './commands/Commands';
import './commands/Cooldown';
import './commands/Del';
import './commands/Disable';
import './commands/Enable';
import './commands/Roll';

import './commands/fight/Accept';
import './commands/fight/Decline';
import './commands/fight/Fight';
import './commands/fight/Fightstats';
import './commands/fight/Rematch';

import './supers/Ignore';
import './supers/Channel';
