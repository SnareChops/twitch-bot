import { injectable } from 'inversify';
import { Userstate } from 'tmi.js';
import { FindManyOptions, ObjectType, FindConditions } from 'typeorm';
import { db } from '../helpers/db';
import { container } from '../container';
import { Client } from '../Client';
import { BUILT_IN, IS_MOD } from '../di.tokens';
import { Command } from '../entities/Command';

export interface BuiltInMetadata {
  name: string;
  description: string;
  restricted?: boolean; // default: false
  visible?: boolean; // default: true
}

export function BuiltIn(metadata: BuiltInMetadata): ClassDecorator {
  return (target: Function) => {
    Reflect.defineMetadata('builtin:metadata', { restricted: false, visible: true, ...metadata }, target);
    container.bind(target).toSelf();
    if (!container.isBound(BUILT_IN)) {
      container.bind(BUILT_IN).toConstantValue(new Map());
    }
    container.get<Map<string, BuiltInCommand>>(BUILT_IN).set(metadata.name, container.get(target));
  };
}

export function getBuiltInMeta(item: any): BuiltInMetadata {
  return Reflect.getMetadata('builtin:metadata', item);
}

@injectable()
export abstract class BuiltInCommand {
  public restricted?: boolean;
  public visible?: boolean;
  public description?: string;
  protected get db() {
    return db();
  }
  protected get client() {
    return container.get<Client>(Client);
  }

  constructor() {
    const meta = getBuiltInMeta(this.constructor);
    this.restricted = meta.restricted;
    this.visible = meta.visible;
    this.description = meta.description;
  }

  public abstract async invoke(target: string, context: Userstate, args: string[]): Promise<any>;
  public async enabled(channel: string): Promise<boolean> {
    return true;
  }

  public async find<T>(entity: ObjectType<T>, options?: FindManyOptions<T>): Promise<T[]> {
    return this.db.manager.find(entity, options);
  }

  public async findOne<T>(entity: ObjectType<T>, conditions?: FindConditions<T>): Promise<T> {
    return this.db.manager.findOne(entity, conditions);
  }

  public async save(...items: any[]): Promise<any> {
    return this.db.manager.save([...items]);
  }

  public async say(channel: string, message: string): Promise<any> {
    return this.client.action(channel, message);
  }

  public async isMod(channel: string, context: Userstate): Promise<boolean> {
    return container.get<(x: string, c: Userstate) => Promise<boolean>>(IS_MOD)(channel, context);
  }

  public async findCommand(channel: string, name: string): Promise<Command | BuiltInCommand> {
    const result = container.get<Map<string, BuiltInCommand>>(BUILT_IN).get(name);
    if (!!result) {
      return result;
    }
    return await db().manager.findOne(Command, { channel, name });
  }
}
