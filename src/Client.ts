import { client, Userstate } from 'tmi.js';
import { Connection } from 'typeorm';
import { Mod } from './entities/Mod';
import { log } from './helpers/log';
import { CommandRunner } from './features/CommandRunner';
import { DescriptionRunner } from './features/DescriptionRunner';
import { MathRunner } from './features/MathRunner';
import { SuperCommandRunner } from './features/SuperCommandRunner';
import { UrlScanRunner } from './features/UrlScanRunner';
import { ScheduleRunner } from './features/ScheduleRunner';

export interface Client extends client {
  channels: string[];
  connection: Connection;
  commandRunner: CommandRunner;
  descriptionRunner: DescriptionRunner;
  mathRunner: MathRunner;
  superRunner: SuperCommandRunner;
  urlScanRunner: UrlScanRunner;
  scheduleRunner: ScheduleRunner;
  ignoreList: Set<string>;

  init(): void;
  parse(target: string, context: Userstate, message: string, self: boolean): Promise<any>;
  connect(): Promise<[string, number]>;
  saveMod(channel: string, username: string, add: boolean): Promise<any>;
}
export const Client = Symbol('Client');

export function clientFrom(base: typeof client) {
  return class _Client extends base implements Client {
    public ignoreList = new Set<string>();

    constructor(
      username: string,
      password: string,
      public channels: string[],
      public connection: Connection,
      public commandRunner: CommandRunner,
      public descriptionRunner: DescriptionRunner,
      public mathRunner: MathRunner,
      public superRunner: SuperCommandRunner,
      public urlScanRunner: UrlScanRunner,
      public scheduleRunner: ScheduleRunner
    ) {
      super({ identity: { username, password }, channels });
    }

    public async init(): Promise<this> {
      this.on('message', (channel, context, message, self) => this.parse(channel, context, message, self));
      this.on('mod', (channel, username) => this.saveMod(channel, username, true));
      this.on('unmod', (channel, username) => this.saveMod(channel, username, false));
      this.on('connected', (address, port) => log(`Connected to ${address}:${port}`));
      await this.connect();
      this.channels.forEach(channel => {
        this.mods(channel).then(result => result.forEach(username => this.saveMod(channel, username, true)));
      });
      await this.scheduleRunner.start();
      return this;
    }

    public async parse(target: string, context: Userstate, message: string, self: boolean) {
      if (self) {
        return;
      }
      const detectedUrls = this.urlScanRunner.detect(message);
      if (!!detectedUrls) {
        await this.urlScanRunner.run(target, context, detectedUrls);
      }

      if (!this.ignoreList.has(target)) {
        // Check for commands
        const detectedCommand = this.commandRunner.detect(message);
        if (detectedCommand) {
          return this.commandRunner.run(target, context, detectedCommand[0], detectedCommand[1]);
        }

        // Check for descriptions
        const detectedDescription = this.descriptionRunner.detect(message);
        if (detectedDescription) {
          return this.descriptionRunner.run(target, context, detectedDescription[0], detectedDescription[1]);
        }

        // Check for math
        const detectedMath = this.mathRunner.detect(message);
        if (detectedMath) {
          return this.mathRunner.run(target, context, detectedMath[0], detectedMath[1], detectedMath[2] as any);
        }
      }
      // Check for super commands
      const detectedSuper = this.superRunner.detect(message);
      if (detectedSuper) {
        return this.superRunner.run(target, context, detectedSuper[0], detectedSuper[1]);
      }
    }

    public async connect(): Promise<[string, number]> {
      return super.connect().catch(console.error) as any;
    }

    public async saveMod(channel: string, username: string, add: boolean) {
      if (add) {
        return this.connection.manager.save(new Mod(channel, username));
      } else {
        return this.connection.manager.delete<Mod>(Mod, username);
      }
    }
  };
}
