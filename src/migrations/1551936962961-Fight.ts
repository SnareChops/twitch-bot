import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class Fight1551936962961 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'fight',
        columns: [
          {
            name: 'username',
            type: 'varchar',
            isPrimary: true
          },
          {
            name: 'wins',
            type: 'int',
            default: 0
          },
          {
            name: 'losses',
            type: 'int',
            default: 0
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    queryRunner.dropTable('fight');
  }
}
