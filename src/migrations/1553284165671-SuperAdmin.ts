import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class SuperAdmin1553284165671 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'super_admin',
        columns: [
          {
            name: 'username',
            type: 'varchar',
            isPrimary: true
          },
          {
            name: 'notes',
            type: 'text',
            isNullable: true
          },
          {
            name: 'createdAt',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('super_admin');
  }
}
