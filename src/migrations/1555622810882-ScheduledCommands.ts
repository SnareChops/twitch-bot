import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const columns = [
  new TableColumn({
    name: 'scheduled',
    type: 'bool',
    isNullable: false,
    default: false
  }),
  new TableColumn({
    name: 'schedule',
    type: 'int',
    isNullable: false,
    default: 15
  })
];

export class ScheduledCommands1555622810882 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.addColumns('command', columns);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropColumns('command', columns);
  }
}
