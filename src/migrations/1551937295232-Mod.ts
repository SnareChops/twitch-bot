import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';

export class Mod1551937295232 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'mod',
        columns: [
          {
            name: 'channel',
            type: 'varchar',
            isPrimary: true
          },
          {
            name: 'username',
            type: 'varchar',
            isPrimary: true
          },
          {
            name: 'createdAt',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
    await queryRunner.createForeignKey(
      'mod',
      new TableForeignKey({
        columnNames: ['channel'],
        referencedColumnNames: ['name'],
        referencedTableName: 'channel',
        onDelete: 'CASCADE'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('mod');
  }
}
