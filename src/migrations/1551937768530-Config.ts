import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';

export class Config1551937768530 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'config',
        columns: [
          {
            name: 'channel',
            type: 'varchar',
            isPrimary: true
          },
          {
            name: 'fightEnabled',
            type: 'bool',
            default: true
          },
          {
            name: 'fightCooldown',
            type: 'int',
            default: 60
          },
          {
            name: 'rollEnabled',
            type: 'bool',
            default: true
          },
          {
            name: 'rollCooldown',
            type: 'int',
            default: 60
          },
          {
            name: 'createdAt',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updatedBy',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'updatedAt',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
    await queryRunner.createForeignKey(
      'config',
      new TableForeignKey({
        columnNames: ['channel'],
        referencedColumnNames: ['name'],
        referencedTableName: 'channel',
        onDelete: 'CASCADE'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('config');
  }
}
