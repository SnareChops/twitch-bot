import { MigrationInterface, QueryRunner } from 'typeorm';
import { SuperAdmin } from '../entities';

export class SuperAdminSeed1553289214548 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.manager.save(new SuperAdmin('snarechops'));
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.manager.remove(await queryRunner.manager.findOne(SuperAdmin, 'snarechops'));
  }
}
