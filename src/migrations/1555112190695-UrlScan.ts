import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
import { PermissionLevel } from '../types/PermissionLevel';

export class UrlScan1555112190695 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'url_scan',
        columns: [
          {
            name: 'channel',
            type: 'varchar',
            isPrimary: true
          },
          {
            name: 'enabled',
            type: 'bool',
            isNullable: false,
            default: true
          },
          {
            name: 'permit',
            type: 'int',
            isNullable: false,
            default: PermissionLevel.MOD
          },
          {
            name: 'useWhitelist',
            type: 'bool',
            isNullable: false,
            default: true
          },
          {
            name: 'whitelist',
            type: 'text',
            isNullable: false
            // default: 'youtube.com,youtu.be,spotify.com,twitch.tv'
          },
          {
            name: 'timeout',
            type: 'int',
            isNullable: false,
            default: 5
          },
          {
            name: 'createdAt',
            type: 'datetime',
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updatedBy',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'updatedAt',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP',
            isNullable: false
          }
        ]
      }),
      true
    );
    await queryRunner.createForeignKey(
      'url_scan',
      new TableForeignKey({
        columnNames: ['channel'],
        referencedColumnNames: ['name'],
        referencedTableName: 'channel',
        onDelete: 'CASCADE'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('url_scan');
  }
}
