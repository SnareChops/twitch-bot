import { MigrationInterface, QueryRunner } from 'typeorm';
import { Channel } from '../entities/Channel';

export class SnareChops1552856996268 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.manager.save(new Channel('#snarechops'));
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    const channel = queryRunner.manager.findOne(Channel, '#snarechops');
    await queryRunner.manager.remove(channel);
  }
}
