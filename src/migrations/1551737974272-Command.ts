import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';

export class Command1551737974272 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'command',
        columns: [
          {
            name: 'channel',
            type: 'varchar',
            isPrimary: true
          },
          {
            name: 'name',
            type: 'varchar',
            isPrimary: true
          },
          {
            name: 'response',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'enabled',
            type: 'bool',
            default: true,
            isNullable: false
          },
          {
            name: 'restricted',
            type: 'bool',
            default: false,
            isNullable: false
          },
          {
            name: 'cooldown',
            type: 'int',
            default: 0,
            isNullable: false
          },
          {
            name: 'description',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'createdBy',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'createdAt',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP',
            isNullable: false
          }
        ]
      }),
      true
    );
    await queryRunner.createForeignKey(
      'command',
      new TableForeignKey({
        columnNames: ['channel'],
        referencedColumnNames: ['name'],
        referencedTableName: 'channel',
        onDelete: 'CASCADE'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('command');
  }
}
