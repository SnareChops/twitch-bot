import { MigrationInterface, QueryRunner, Table } from 'typeorm';
import { Config, FightConfig, RollConfig } from '../entities';

export class SeparateConfig1555199974777 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'fight_config',
        columns: [
          {
            name: 'channel',
            type: 'varchar',
            isPrimary: true
          },
          {
            name: 'enabled',
            type: 'bool',
            isNullable: false,
            default: true
          },
          {
            name: 'cooldown',
            type: 'int',
            isNullable: true,
            default: 60
          },
          {
            name: 'createdAt',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updatedBy',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'updatedAt',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );

    await queryRunner.createTable(
      new Table({
        name: 'roll_config',
        columns: [
          {
            name: 'channel',
            type: 'varchar',
            isPrimary: true
          },
          {
            name: 'enabled',
            type: 'bool',
            isNullable: false,
            default: true
          },
          {
            name: 'cooldown',
            type: 'int',
            isNullable: true,
            default: 60
          },
          {
            name: 'createdAt',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updatedBy',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'updatedAt',
            type: 'datetime',
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );

    const items = await queryRunner.manager.find(Config);
    for (const item of items) {
      const fightConfig = new FightConfig(item.channel, item.updatedBy);
      fightConfig.enabled = item.fightEnabled;
      fightConfig.cooldown = item.fightCooldown;
      fightConfig.createdAt = item.createdAt;
      fightConfig.updatedAt = item.updatedAt;
      await queryRunner.manager.save(fightConfig);

      const rollConfig = new RollConfig(item.channel, item.updatedBy);
      rollConfig.enabled = item.rollEnabled;
      rollConfig.cooldown = item.rollCooldown;
      rollConfig.createdAt = item.createdAt;
      rollConfig.updatedAt = item.updatedAt;
      await queryRunner.manager.save(rollConfig);
    }

    await queryRunner.dropTable('config', true, true, true);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
