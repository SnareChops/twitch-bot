import { MenuItem } from '../client/lib';

let activeStubs: (string | Function)[] = [];

function save(item: string | Function) {
  activeStubs.push(item);
}

export function stub<T = any>(diToken: string, name?: string): T {
  const obj = {} as T;
  if (!!name ? Neon.container.isBoundNamed(diToken, name) : Neon.bound(diToken)) {
    Neon.unbind(diToken);
  }
  if (!!name) {
    Neon.bind(diToken)
      .toConstantValue(obj)
      .whenTargetNamed(name);
  } else {
    Neon.bind(diToken).toConstantValue(obj);
  }
  save(diToken);
  return obj;
}

export function stubConstant(token: string, value: any): void {
  Neon.bind(token).toConstantValue(value);
  save(token);
}

export function unstub() {
  activeStubs.forEach(item => (typeof item === 'string' ? Neon.unbind(item) : item()));
  activeStubs = [];
}

export function stubNavigate(
  stub: (element: string, attributes?: { [key: string]: string | number | boolean }) => any
) {
  const original = Neon.navigate;
  Neon.navigate = (element, attributes) => {
    save(() => (Neon.navigate = original));
    return stub(element, attributes);
  };
}

export function stubBack(stub: () => void) {
  const original = Neon.back;
  Neon.back = () => {
    save(() => (Neon.back = original));
    stub();
  };
}

export function stubHandleError(stub: (error: string | Error, messages: string | MessageTokens) => string) {
  const original = Neon.handleError;
  Neon.handleError = (error, messages) => {
    save(() => (Neon.handleError = original));
    return stub(error, messages);
  };
}

export function stubAlertError(stub: (message: string) => string) {
  const original = Neon.alert.error;
  Neon.alert.error = message => {
    save(() => (Neon.alert.error = original));
    return stub(message);
  };
}

export function stubAlertWarn(stub: (message: string) => string) {
  const original = Neon.alert.warn;
  Neon.alert.warn = message => {
    save(() => (Neon.alert.warn = original));
    return stub(message);
  };
}

export function stubAlertInfo(stub: (message: string) => string) {
  const original = Neon.alert.info;
  Neon.alert.info = message => {
    save(() => (Neon.alert.info = original));
    return stub(message);
  };
}

export function stubAlertSuccess(stub: (message: string) => string) {
  const original = Neon.alert.success;
  Neon.alert.success = message => {
    save(() => (Neon.alert.success = original));
    return stub(message);
  };
}

export function stubLogError(stub: (...args: any[]) => void) {
  const original = Neon.logger.error;
  Neon.logger.error = (...args) => {
    save(() => (Neon.logger.error = original));
    return stub(...args);
  };
}

export function stubLogWarn(stub: (...args: any[]) => void) {
  const original = Neon.logger.warn;
  Neon.logger.warn = (...args) => {
    save(() => (Neon.logger.warn = original));
    return stub(...args);
  };
}

export function stubLogInfo(stub: (...args: any[]) => void) {
  const original = Neon.logger.info;
  Neon.logger.info = (...args) => {
    save(() => (Neon.logger.info = original));
    return stub(...args);
  };
}

export function stubLogDebug(stub: (...args: any[]) => void) {
  const original = Neon.logger.debug;
  Neon.logger.debug = (...args) => {
    save(() => (Neon.logger.debug = original));
    return stub(...args);
  };
}

export function stubHref(stub: (url: string) => void) {
  const original = Neon.href;
  Neon.href = url => {
    save(() => (Neon.href = original));
    return stub(url);
  };
}
