import '../../client/elements/login/choose-channel';
import * as assert from 'assert';
import { trender, telementAll, click, content } from '../_lib';
import { html } from 'lit-html';
import { channels, channel } from '../../client/helpers/channel';
import { Toolbar } from '../../client/elements/bricks/toolbar';
import { stubNavigate } from '../_stubs';

describe('choose-channel', () => {
  let toolbar: Toolbar;
  beforeEach(() => {
    channels(['#channel1', '#channel2']);
    toolbar = Object.create(Toolbar.prototype);
    Neon.bind('Toolbar').toConstantValue(toolbar);
  });

  it('should display a button for each channel', async () => {
    await trender(
      html`
        <choose-channel></choose-channel>
      `
    );

    assert.strictEqual(content('button:nth-of-type(1)'), 'channel1', 'should be the first channel');
    assert.strictEqual(content('button:nth-of-type(2)'), 'channel2', 'should be the second channel');
  });

  it('should set channel, show toolbar, and navigate home on click of button', async () => {
    await trender(
      html`
        <choose-channel></choose-channel>
      `
    );

    let showed = false;
    toolbar.show = () => (showed = true);

    let navigated = false;
    stubNavigate(element => {
      assert.strictEqual(element, 'home-', 'should be the home element');
      navigated = true;
    });

    await click('button');
    assert(showed, 'should have shown');
    assert(navigated);
    assert.strictEqual(channel(), '#channel1', 'should be the selected channel');
  });
});
