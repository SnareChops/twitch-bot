import '../../client/elements/commands/command-home';
import * as assert from 'assert';
import { html } from 'lit-html';
import { trender, click, telement, wait } from '../_lib';
import { CommandList } from '../../client/elements/commands/command-list';
import { stub } from '../_stubs';
import { CommandService } from '../../client/services/CommandService';

describe('command-home', () => {
  beforeEach(() => {
    stub<CommandService>('CommandService').get = async () => [];
  });

  it('clicking on new command should show the command-new form', async () => {
    await trender(
      html`
        <command-home></command-home>
      `
    );

    await click('button[type="button"]');
    assert(telement('command-new'), 'should show the command-new form');
  });

  it('command-new @done should trigger a refresh on the command-list', async () => {
    await trender(
      html`
        <command-home .creating=${true}></command-home>
      `
    );
    const list = telement<CommandList>('command-list');
    let refreshed = false;
    list.refresh = async () => {
      refreshed = true;
    };

    telement('command-new').dispatchEvent(new Event('done'));
    await wait();
    assert(refreshed);
    assert(telement('button[type="button"]'));
  });
});
