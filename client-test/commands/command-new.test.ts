import '../../client/elements/commands/command-new';
import * as assert from 'assert';
import { trender, telement, click, submit } from '../_lib';
import { html } from 'lit-html';
import { TextInput } from '../../client/elements/bricks/text-input';
import { CommandService } from '../../client/services/CommandService';
import { stub, stubHandleError } from '../_stubs';
import { CommandNew } from '../../client/elements/commands/command-new';

describe('command-new', () => {
  let service: CommandService;

  beforeEach(() => {
    service = stub('CommandService');
  });

  it('should create a new command', async () => {
    let created = false;
    service.create = async item => {
      created = true;
      return item;
    };

    let doned = false;
    function done() {
      doned = true;
    }

    await trender(
      html`
        <command-new @done=${done}></command-new>
      `
    );

    telement<TextInput>('text-input:nth-of-type(1)').trigger('change', 'test');
    telement<TextInput>('text-input:nth-of-type(2)').trigger('change', 'test-response');
    telement<TextInput>('text-input:nth-of-type(3)').trigger('change', 'test-description');
    // telement<NumberInput>('number-input:nth-of-type(3)').trigger('change', 55);

    await submit('form');
    assert(created, 'should have created');
    assert(doned, 'should have called done');
  });

  it('should not throw an error if no command name', async () => {
    let errored = false;
    stubHandleError((error: Error) => {
      assert.strictEqual(error.message, 'Command name is required.', 'should be the error message');
      errored = true;
      return error.message;
    });

    await trender(
      html`
        <command-new></command-new>
      `
    );

    telement<TextInput>('text-input:nth-of-type(2)').trigger('change', 'test-response');
    telement<TextInput>('text-input:nth-of-type(3)').trigger('change', 'test-description');
    // telement<NumberInput>('number-input:nth-of-type(3)').trigger('change', 55);

    await submit('form');
    assert(errored, 'should have errored');
  });

  it('should not throw an error if command name has invalid characters', async () => {
    let errored = false;
    stubHandleError((error: Error) => {
      assert.strictEqual(
        error.message,
        'Command name must not contain any special characters.',
        'should be the error message'
      );
      errored = true;
      return error.message;
    });

    await trender(
      html`
        <command-new></command-new>
      `
    );

    telement<TextInput>('text-input:nth-of-type(1)').trigger('change', 'test-invalid');
    telement<TextInput>('text-input:nth-of-type(2)').trigger('change', 'test-response');
    telement<TextInput>('text-input:nth-of-type(3)').trigger('change', 'test-description');
    // telement<NumberInput>('number-input:nth-of-type(3)').trigger('change', 55);

    await submit('form');
    assert(errored, 'should have errored');
  });

  it('should not throw an error if command name has invalid characters', async () => {
    let errored = false;
    stubHandleError((error: Error) => {
      assert.strictEqual(error.message, 'Command response is required.', 'should be the error message');
      errored = true;
      return error.message;
    });

    await trender(
      html`
        <command-new></command-new>
      `
    );

    telement<TextInput>('text-input:nth-of-type(1)').trigger('change', 'test');
    telement<TextInput>('text-input:nth-of-type(3)').trigger('change', 'test-description');
    // telement<NumberInput>('number-input:nth-of-type(3)').trigger('change', 55);

    await submit('form');
    assert(errored, 'should have errored');
  });

  it('should not throw an error if command name has invalid characters', async () => {
    let doned = false;
    function done() {
      doned = true;
    }

    await trender(
      html`
        <command-new @done=${done}></command-new>
      `
    );

    const instance = telement<CommandNew>('command-new');

    telement<TextInput>('text-input:nth-of-type(1)').trigger('change', 'test');
    telement<TextInput>('text-input:nth-of-type(2)').trigger('change', 'test-response');
    telement<TextInput>('text-input:nth-of-type(3)').trigger('change', 'test-description');
    // telement<NumberInput>('number-input:nth-of-type(3)').trigger('change', 55);

    await click('button[type="button"]');
    assert.deepStrictEqual(instance.command, {}, 'should be empty');
    assert(doned, 'should have called done');
  });
});
