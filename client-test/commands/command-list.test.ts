import '../../client/elements/commands/command-list';
import * as assert from 'assert';
import { CommandService } from '../../client/services/CommandService';
import { stub } from '../_stubs';
import { trender, telementAll } from '../_lib';
import { html } from 'lit-html';
import { Command } from '../../src/entities';

describe('command-list', () => {
  let service: CommandService;

  beforeEach(() => {
    service = stub('CommandService');
  });

  it('should have command items', async () => {
    let fetched = false;
    service.get = async () => {
      fetched = true;
      return [{}, {}] as Command[];
    };

    await trender(
      html`
        <command-list></command-list>
      `
    );

    assert.strictEqual(telementAll('command-item').length, 2, 'should have 2 list items');
    assert(fetched);
  });
});
