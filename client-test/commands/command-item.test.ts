import '../../client/elements/commands/command-item';
import * as assert from 'assert';
import { trender, content, telement, wait, click } from '../_lib';
import { html } from 'lit-html';
import { CheckBox } from '../../client/elements/bricks/check-box';
import { Command } from '../../src/entities';
import { TextEdit } from '../../client/elements/bricks/text-edit';
import { CommandService } from '../../client/services/CommandService';
import { stub } from '../_stubs';
import { CommandItem } from '../../client/elements/commands/command-item';
import { NumberEdit } from '../../client/elements/bricks/number-edit';

describe('command-item', () => {
  let service: CommandService;
  let item: Command;
  beforeEach(() => {
    item = {
      name: 'test',
      response: 'test-response',
      description: 'test-description',
      cooldown: 22,
      createdBy: 'moduser',
      enabled: true,
      restricted: false
    } as Command;
    service = stub('CommandService');
  });

  it('should display', async () => {
    await trender(
      html`
        <command-item .item=${item}></command-item>
      `
    );

    assert.strictEqual(content('item-name'), '!test', 'should be the name');
    assert.strictEqual(content('item-response text-edit'), 'test-response', 'should be the response');
    assert.strictEqual(content('item-description text-edit'), 'test-description', 'should be the description');
    assert.strictEqual(content('item-cooldown number-edit'), '22', 'should be the cooldown');
    assert.strictEqual(content('item-creator'), 'Created By: moduser', 'should be the creator');
    assert(telement<CheckBox>('action-item:nth-of-type(1) check-box').value, 'should be checked');
    assert(!telement<CheckBox>('action-item:nth-of-type(2) check-box').value, 'should not be checked');
  });

  it('should save a change to the response', async () => {
    let saved = false;
    service.update = async item => {
      assert.strictEqual(item.response, 'new-value', 'should be the new value');
      saved = true;
      return item;
    };

    await trender(
      html`
        <command-item .item=${item}></command-item>
      `
    );

    telement<TextEdit>('item-response text-edit').trigger('commit', 'new-value');
    await wait();
    assert(saved);
  });

  it('should save a change to the description', async () => {
    let saved = false;
    service.update = async item => {
      assert.strictEqual(item.description, 'new-value', 'should be the new value');
      saved = true;
      return item;
    };

    await trender(
      html`
        <command-item .item=${item}></command-item>
      `
    );

    telement<TextEdit>('item-description text-edit').trigger('commit', 'new-value');
    await wait();
    assert(saved);
  });

  it('should save a change to the cooldown', async () => {
    let saved = false;
    service.update = async item => {
      assert.strictEqual(item.cooldown, 44, 'should be the new value');
      saved = true;
      return item;
    };

    await trender(
      html`
        <command-item .item=${item}></command-item>
      `
    );

    telement<NumberEdit>('item-cooldown number-edit').trigger('commit', 44);
    await wait();
    assert(saved);
  });

  it('should save a change to enabled', async () => {
    let saved = false;
    service.toggleEnabled = async i => {
      assert.strictEqual(i.enabled, true, 'should be the old value');
      saved = true;
      i.enabled = false;
      return i;
    };

    await trender(
      html`
        <command-item .item=${item}></command-item>
      `
    );

    telement<CheckBox>('action-item:nth-of-type(1) check-box').trigger('change', false);
    await wait();
    assert(saved);
    assert(!item.enabled);
    assert(telement('command-item').classList.contains('disabled'));
  });

  it('should save a change to restricted', async () => {
    let saved = false;
    service.toggleRestricted = async i => {
      assert.strictEqual(i.restricted, false, 'should be the old value');
      saved = true;
      i.restricted = true;
      return i;
    };

    await trender(
      html`
        <command-item .item=${item}></command-item>
      `
    );

    telement<CheckBox>('action-item:nth-of-type(2) check-box').trigger('change', true);
    await wait();
    assert(saved);
    assert(item.restricted);
  });

  it('should delete on delete click', async () => {
    let deleted = false;
    service.delete = async name => {
      assert.strictEqual(name, 'test', 'should be the command name');
      deleted = true;
    };

    await trender(
      html`
        <command-item .item=${item}></command-item>
      `
    );

    const instance = telement<CommandItem>('command-item');
    let removed = false;
    instance.remove = () => (removed = true);
    await click('button');
    assert(deleted);
    assert(removed);
  });

  it('should not have disabled class if enabled', async () => {
    await trender(
      html`
        <command-item .item=${item}></command-item>
      `
    );

    const instance = telement<CommandItem>('command-item');
    assert(!instance.classList.contains('disabled'), 'should not be disabled');
  });

  it('should have disabled class if not enabled', async () => {
    await trender(
      html`
        <command-item .item=${{ ...item, enabled: false }}></command-item>
      `
    );

    const instance = telement<CommandItem>('command-item');
    assert(instance.classList.contains('disabled'), 'should be disabled');
  });
});
