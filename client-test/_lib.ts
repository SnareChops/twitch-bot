import { render, html, TemplateResult } from 'lit-html';

const TEST_CONTAINER = document.querySelector('test-container');

export function wait(waitms?: number): Promise<void> {
  return new Promise(resolve => setTimeout(resolve, waitms));
}

export async function trender(tmpl: TemplateResult, waitms?: number) {
  render(tmpl, TEST_CONTAINER);
  await wait(waitms);
}

export function treset() {
  render(html``, TEST_CONTAINER);
}

export function telement<T = HTMLElement>(element: string): T {
  return (TEST_CONTAINER.querySelector(element) as unknown) as T;
}

export function telementAll<T = HTMLElement>(element: string): T[] {
  return (Array.from((TEST_CONTAINER.querySelectorAll(element) as unknown) as NodeListOf<Node>) as unknown) as T[];
}

export async function click(element: string | HTMLElement, waitms?: number): Promise<void> {
  if (typeof element === 'string') {
    element = telement(element);
  }
  if (!element) {
    throw new Error(`Cannot find '${element}' to click`);
  }
  (element as HTMLElement).dispatchEvent(new Event('click'));
  await wait(waitms);
}

export async function submit(form: string | HTMLElement, waitms?: number): Promise<void> {
  if (typeof form === 'string') {
    form = telement(form);
  }
  if (!form) {
    throw new Error(`Cannot find '${form}' to submit`);
  }
  (form as HTMLElement).dispatchEvent(new Event('submit'));
  await wait(waitms);
}

export function content(element: string | HTMLElement): string {
  if (typeof element === 'string') {
    element = telement(element);
  }
  if (!element) {
    throw new Error(`Cannot find '${element}' to get content`);
  }
  return (element as HTMLElement).textContent.trim();
}

export function contentAll(selector: string | NodeListOf<Element> | HTMLElement[]): string[] {
  if (typeof selector === 'string') {
    selector = telementAll(selector);
  }
  return Array.from(selector).map(item => item.textContent.trim());
}

export function classes(element: string | HTMLElement): DOMTokenList {
  if (typeof element === 'string') {
    element = telement(element);
  }
  if (!element) {
    throw new Error(`Cannot find '${element}' to get classes`);
  }
  return (element as HTMLElement).classList;
}
