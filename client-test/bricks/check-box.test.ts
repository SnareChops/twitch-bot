import '../../client/elements/bricks/check-box';
import * as assert from 'assert';
import { html } from 'lit-html';
import { onEvent } from '../../client/lib';
import { trender, telement, click } from '../_lib';

describe('check-box', () => {
  it('should display the check when true', async () => {
    await trender(
      html`
        <check-box .value=${true}></check-box>
      `
    );
    assert(telement('svg path'), 'should have the check');
  });

  it('should not display the check when false', async () => {
    await trender(
      html`
        <check-box .value=${false}></check-box>
      `
    );
    assert(!telement('svg path'), 'should not have the check');
  });

  it('should emit change when clicked', done => {
    let count = -1;
    async function check(value: boolean) {
      count++;
      if (count === 0) {
        assert.strictEqual(value, false, 'should be false');
        click('check-box');
      } else {
        assert(value, 'should be true');
        done();
      }
    }
    trender(
      html`
        <check-box .value=${true} @change=${onEvent(value => check(value))}></check-box>
      `
    ).then(() => click('check-box'));
  });
});
