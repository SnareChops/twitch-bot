import '../../client/elements/bricks/number-edit';
import * as assert from 'assert';
import { html } from 'lit-html';
import { trender, content, click, telement, wait } from '../_lib';
import { onEvent } from '../../client/lib';

describe('number-edit', () => {
  it('should show value if not editing', async () => {
    await trender(
      html`
        <number-edit .value=${3}></number-edit>
      `
    );
    assert.strictEqual(content('number-edit'), '3', 'should be the value');
  });

  it('should show hint if no value and not editing', async () => {
    await trender(
      html`
        <number-edit hint="test-hint"></number-edit>
      `
    );
    assert.strictEqual(content('number-edit'), 'test-hint', 'should be the value');
  });

  it('should be empty if no hint and not editing', async () => {
    await trender(
      html`
        <number-edit></number-edit>
      `
    );
    assert.strictEqual(content('number-edit'), '', 'should be the value');
  });

  it('should enter edit mode on click of element', async () => {
    await trender(
      html`
        <number-edit></number-edit>
      `
    );
    await click('number-edit');
    assert(telement('input[type="number"]'));
  });

  it('should commit a value using ENTER', done => {
    async function callback(value: number) {
      assert.strictEqual(value, 20, 'should be 20');
      await wait();
      assert.strictEqual(content('number-edit'), '20', 'should be the display');
      done();
    }

    trender(
      html`
        <number-edit .editing=${true} @commit=${onEvent(value => callback(value))}></number-edit>
      `
    )
      .then(() => click('number-edit'))
      .then(() => assert(telement('input[type="number"]')))
      .then(() => {
        const input = telement<HTMLInputElement>('input');
        input.value = '20';
        input.dispatchEvent(new Event('input'));
        const event = new KeyboardEvent('keyup', { bubbles: true, cancelable: true, key: 'Enter', shiftKey: false });
        input.dispatchEvent(event);
      });
  });

  it('should commit a value commit button', done => {
    async function callback(value: number) {
      assert.strictEqual(value, 20, 'should be 20');
      await wait();
      assert.strictEqual(content('number-edit'), '20', 'should be the display');
      done();
    }

    trender(
      html`
        <number-edit .editing=${true} @commit=${onEvent(value => callback(value))}></number-edit>
      `
    )
      .then(() => click('number-edit'))
      .then(() => assert(telement('input[type="number"]')))
      .then(() => {
        const input = telement<HTMLInputElement>('input');
        input.value = '20';
        input.dispatchEvent(new Event('input'));
        click('button');
      });
  });
});
