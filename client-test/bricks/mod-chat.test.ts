import '../../client/elements/bricks/mod-chat';
import * as assert from 'assert';
import { html } from 'lit-html';
import { trender, telement, wait, click } from '../_lib';
import { stubHandleError } from '../_stubs';
import { ModChatService } from '../../client/services/ModChatService';
import { ModChatMessage } from '../../src/entities';
import { ChatMessage } from '../../client/elements/bricks/chat-message';

describe('mod-chat', () => {
  let service: ModChatService;

  beforeEach(() => {
    service = {} as ModChatService;
    Neon.bind('ModChatService').toConstantValue(service);
  });

  it('should load chat messages', async () => {
    const expected = [
      new ModChatMessage('#channel', '3', 'u3'),
      new ModChatMessage('#channel', '2', 'u2'),
      new ModChatMessage('#channel', '1', 'u1')
    ];
    const clone = expected.slice(0);

    let fetched = false;
    service.history = async () => {
      fetched = true;
      return expected;
    };
    service.onMessage = () => {};
    await trender(
      html`
        <mod-chat></mod-chat>
      `
    );

    assert.strictEqual(telement<ChatMessage>('chat-message:nth-of-type(1)').message, clone[2]);
    assert.strictEqual(telement<ChatMessage>('chat-message:nth-of-type(2)').message, clone[1]);
    assert.strictEqual(telement<ChatMessage>('chat-message:nth-of-type(3)').message, clone[0]);
    assert(fetched);
  });

  it('should handle an incoming message', async () => {
    const expected = new ModChatMessage('#channel', '1', 'u1');
    service.history = async () => [];
    service.onMessage = cb => {
      cb(expected);
    };
    await trender(
      html`
        <mod-chat></mod-chat>
      `
    );

    assert.strictEqual(telement<ChatMessage>('chat-message').message, expected);
  });

  it('should handle an error thrown on fetching history', async () => {
    service.history = async () => {
      throw new Error('error-message');
    };

    let errored = false;
    stubHandleError((error: Error) => {
      assert.strictEqual(error.message, 'error-message', 'should be the error');
      errored = true;
      return error.message;
    });
    await trender(
      html`
        <mod-chat></mod-chat>
      `
    );
    assert(errored);
  });

  it('should handle an error thrown on message registration', async () => {
    service.history = async () => [];
    service.onMessage = () => {
      throw new Error('error-message');
    };

    let errored = false;
    stubHandleError((error: Error) => {
      assert.strictEqual(error.message, 'error-message', 'should be the error');
      errored = true;
      return error.message;
    });

    await trender(
      html`
        <mod-chat></mod-chat>
      `
    );

    assert(errored);
  });

  it('should submit a message on enter key', async () => {
    service.history = async () => [];
    service.onMessage = () => {};

    await trender(
      html`
        <mod-chat></mod-chat>
      `
    );

    let sent = false;
    service.send = async message => {
      assert.strictEqual(message, 'test-message', 'should be the message');
      sent = true;
      return new ModChatMessage('#channel', message, '');
    };

    const textarea = telement<HTMLTextAreaElement>('textarea');
    textarea.value = 'test-message\n';
    textarea.dispatchEvent(new Event('input'));
    const event = new KeyboardEvent('keyup', { bubbles: true, cancelable: true, key: 'Enter', shiftKey: false });
    textarea.dispatchEvent(event);
    await wait(5);
    assert(sent);
  });

  it('should submit a message on enter key', async () => {
    service.history = async () => [];
    service.onMessage = () => {};

    await trender(
      html`
        <mod-chat></mod-chat>
      `
    );

    let sent = false;
    service.send = async message => {
      assert.strictEqual(message, 'test-message', 'should be the message');
      sent = true;
      return new ModChatMessage('#channel', message, '');
    };

    const textarea = telement<HTMLTextAreaElement>('textarea');
    textarea.value = 'test-message\n';
    textarea.dispatchEvent(new Event('input'));
    await click('button');
    assert(sent);
  });
});
