import '../../client/elements/bricks/toolbar';
import * as assert from 'assert';
import { html } from 'lit-html';
import { trender, click, telement } from '../_lib';
import { stubNavigate } from '../_stubs';
import { Toolbar } from '../../client/elements/bricks/toolbar';

describe('toolbar-', () => {
  it('should show and hide', async () => {
    await trender(
      html`
        <toolbar-></toolbar->
      `
    );
    assert(!telement('toolbar-item'), 'should not show contents');
    Neon.get<Toolbar>('Toolbar').show();
    assert(telement('toolbar-item'), 'should now have contents');
    Neon.get<Toolbar>('Toolbar').hide();
    assert(!telement('toolbar-item'), 'should no longer have contents');
  });

  it('should navigate to home-', async () => {
    let navigated = false;
    stubNavigate(element => {
      assert.strictEqual(element, 'home-');
      navigated = true;
    });

    await trender(
      html`
        <toolbar-></toolbar->
      `
    );
    Neon.get<Toolbar>('Toolbar').show();
    await click('toolbar-item:nth-of-type(1)');
    assert(navigated);
  });

  it('should navigate to command-home', async () => {
    let navigated = false;
    stubNavigate(element => {
      assert.strictEqual(element, 'command-home');
      navigated = true;
    });

    await trender(
      html`
        <toolbar-></toolbar->
      `
    );
    Neon.get<Toolbar>('Toolbar').show();
    await click('toolbar-item:nth-of-type(2)');
    assert(navigated);
  });

  it('should navigate to config-home', async () => {
    let navigated = false;
    stubNavigate(element => {
      assert.strictEqual(element, 'config-home');
      navigated = true;
    });

    await trender(
      html`
        <toolbar-></toolbar->
      `
    );
    Neon.get<Toolbar>('Toolbar').show();
    await click('toolbar-item:nth-of-type(3)');
    assert(navigated);
  });
});
