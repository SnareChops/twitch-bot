import '../../client/elements/bricks/text-edit';
import * as assert from 'assert';
import { html } from 'lit-html';
import { trender, content, click, telement, wait } from '../_lib';
import { onEvent } from '../../client/lib';

describe('text-edit', () => {
  it('should show value if not editing', async () => {
    await trender(
      html`
        <text-edit .value=${'test-value'}></text-edit>
      `
    );
    assert.strictEqual(content('text-edit'), 'test-value', 'should be the value');
  });

  it('should show hint if no value and not editing', async () => {
    await trender(
      html`
        <text-edit hint="test-hint"></text-edit>
      `
    );
    assert.strictEqual(content('text-edit'), 'test-hint', 'should be the value');
  });

  it('should be empty if no hint and not editing', async () => {
    await trender(
      html`
        <text-edit></text-edit>
      `
    );
    assert.strictEqual(content('text-edit'), '', 'should be the value');
  });

  it('should enter edit mode on click of element', async () => {
    await trender(
      html`
        <text-edit></text-edit>
      `
    );
    await click('text-edit');
    assert(telement('input[type="text"]'));
  });

  it('should commit a value using ENTER', done => {
    async function callback(value: number) {
      assert.strictEqual(value, 'test-value', 'should be test-value');
      await wait();
      assert.strictEqual(content('text-edit'), 'test-value', 'should be the display');
      done();
    }

    trender(
      html`
        <text-edit .editing=${true} @commit=${onEvent(value => callback(value))}></text-edit>
      `
    )
      .then(() => click('text-edit'))
      .then(() => assert(telement('input[type="text"]')))
      .then(() => {
        const input = telement<HTMLInputElement>('input');
        input.value = 'test-value';
        input.dispatchEvent(new Event('input'));
        const event = new KeyboardEvent('keyup', { bubbles: true, cancelable: true, key: 'Enter', shiftKey: false });
        input.dispatchEvent(event);
      });
  });

  it('should commit a value commit button', done => {
    async function callback(value: number) {
      assert.strictEqual(value, 'test-value', 'should be test-value');
      await wait();
      assert.strictEqual(content('text-edit'), 'test-value', 'should be the display');
      done();
    }

    trender(
      html`
        <text-edit .editing=${true} @commit=${onEvent(value => callback(value))}></text-edit>
      `
    )
      .then(() => click('text-edit'))
      .then(() => assert(telement('input[type="text"]')))
      .then(() => {
        const input = telement<HTMLInputElement>('input');
        input.value = 'test-value';
        input.dispatchEvent(new Event('input'));
        click('button');
      });
  });
});
