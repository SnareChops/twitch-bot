import '../../client/elements/bricks/chat-message';
import * as assert from 'assert';
import { html } from 'lit-html';
import { trender, content, telement } from '../_lib';
import { ModChatMessage } from '../../src/entities';

describe('chat-message', () => {
  it('should display', async () => {
    const message = {
      name: 'testuser',
      message: 'test-message'
    } as ModChatMessage;
    await trender(
      html`
        <chat-message .message=${message}></chat-message>
      `
    );
    assert.strictEqual(content('username'), 'testuser', 'should be the name');
    assert.strictEqual(telement('chat-message').innerText, 'testuser: test-message', 'should be the message');
  });
});
