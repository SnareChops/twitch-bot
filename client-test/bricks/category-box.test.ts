import '../../client/elements/bricks/category-box';
import * as assert from 'assert';
import { html } from 'lit-html';
import { trender, content, telement } from '../_lib';

describe('category-box', () => {
  it('should display the title and content', async () => {
    await trender(
      html`
        <category-box title="test-title"><test-content></test-content></category-box>
      `
    );

    assert.strictEqual(content('title-area'), 'test-title', 'should be the title');
    assert(telement('content-area test-content'), 'should have the content');
  });
});
