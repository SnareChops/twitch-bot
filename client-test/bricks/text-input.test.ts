import '../../client/elements/bricks/text-input';
import * as assert from 'assert';
import { html } from 'lit-html';
import { trender, content, telement } from '../_lib';
import { onEvent } from '../../client/lib';

describe('text-input', () => {
  it('should use value of empty string if undefined', async () => {
    await trender(
      html`
        <text-input></text-input>
      `
    );
    assert.strictEqual(content('text-input'), '', 'should be empty');
  });

  it('should use display', async () => {
    await trender(
      html`
        <text-input .value=${'test-value'} placeholder="test-placeholder"></text-input>
      `
    );
    const input = telement<HTMLInputElement>('input[type="text"]');
    assert.strictEqual(input.value, 'test-value', 'should be the value');
    assert.strictEqual(input.placeholder, 'test-placeholder', 'should be the placeholder');
  });

  it('should emit change on change', done => {
    function callback(value: string) {
      assert.strictEqual(value, 'changed-value', 'should be the changed value');
      done();
    }

    trender(
      html`
        <text-input .value=${'test-value'} @change=${onEvent(value => callback(value))}></text-input>
      `
    ).then(() => {
      const input = telement<HTMLInputElement>('input[type="text"]');
      input.value = 'changed-value';
      input.dispatchEvent(new Event('change'));
    });
  });
});
