// import '../../client/elements/config/config-home';
// import * as assert from 'assert';
// import { trender, telement, wait } from '../_lib';
// import { html } from 'lit-html';
// import { Config } from '../../src/entities';
// import { stub } from '../_stubs';
// import { NumberEdit } from '../../client/elements/bricks/number-edit';
// import { CheckBox } from '../../client/elements/bricks/check-box';

// describe('config-home', () => {
//   let service: ConfigService;
//   let config: Config;

//   beforeEach(() => {
//     config = {
//       channel: '#channel',
//       fightEnabled: true,
//       fightCooldown: 60,
//       rollEnabled: true,
//       rollCooldown: 60
//     } as Config;
//     service = stub('ConfigService');
//   });

//   it('should disable fights', async () => {
//     let saved = false;
//     service.update = async item => {
//       assert.strictEqual(item.fightEnabled, false, 'should be false');
//       saved = true;
//       return item;
//     };

//     service.get = async () => config;

//     await trender(
//       html`
//         <config-home></config-home>
//       `
//     );

//     telement<CheckBox>('category-box:nth-of-type(1) section:nth-of-type(1) check-box').trigger('change', false);
//     await wait();
//     assert(saved, 'should have saved');
//   });

//   it('should set fight cooldown', async () => {
//     let saved = false;
//     service.update = async item => {
//       assert.strictEqual(item.fightCooldown, 22, 'should be 22');
//       saved = true;
//       return item;
//     };

//     service.get = async () => config;

//     await trender(
//       html`
//         <config-home></config-home>
//       `
//     );

//     telement<NumberEdit>('category-box:nth-of-type(1) section:nth-of-type(2) number-edit').trigger('commit', 22);
//     await wait();
//     assert(saved);
//   });

//   it('should disable rolls', async () => {
//     let saved = false;
//     service.update = async item => {
//       assert.strictEqual(item.rollEnabled, false, 'should be false');
//       saved = true;
//       return item;
//     };

//     service.get = async () => config;

//     await trender(
//       html`
//         <config-home></config-home>
//       `
//     );

//     telement<CheckBox>('category-box:nth-of-type(2) section:nth-of-type(1) check-box').trigger('change', false);
//     assert(saved);
//   });

//   it('should set roll cooldown', async () => {
//     let saved = false;
//     service.update = async item => {
//       assert.strictEqual(item.rollCooldown, 22, 'should be 22');
//       saved = true;
//       return item;
//     };

//     service.get = async () => config;

//     await trender(
//       html`
//         <config-home></config-home>
//       `
//     );

//     telement<NumberEdit>('category-box:nth-of-type(2) section:nth-of-type(2) number-edit').trigger('commit', 22);
//     await wait();
//     assert(saved);
//   });
// });
