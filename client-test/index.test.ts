import 'reflect-metadata';
import '../client/lib/Neon';
import '../client/lib/dom';

import './bricks/category-box.test';
import './bricks/chat-message.test';
import './bricks/check-box.test';
import './bricks/mod-chat.test';
import './bricks/number-edit.test';
import './bricks/text-edit.test';
import './bricks/text-input.test';
import './bricks/toolbar.test';

import './commands/command-home.test';
import './commands/command-item.test';
import './commands/command-list.test';
import './commands/command-new.test';

import './config/config-home.test';

import './login/choose-channel.test';

import { treset } from './_lib';
import { unstub } from './_stubs';

mocha.checkLeaks();
mocha.run();

beforeEach(() => {
  unstub();
  Neon.unbindAll();
});
afterEach(async () => treset());
