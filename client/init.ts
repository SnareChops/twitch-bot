import { WampClient } from './services/WampClient';

if (!Neon.bound('WampClient')) {
  Neon.bind('WampClient').toConstantValue(new WampClient());
}
