import { render, html } from 'lit-html';
import { Element, NeonElement } from '../lib';
import { channel } from '../helpers/channel';

// prettier-ignore
const template = (e: Home) => html`
	<h1>${!!e.channel ? e.channel.replace('#', '') : ''}</h1>
  <mod-chat></mod-chat>
`;

@Element('home-')
export class Home extends NeonElement {
  public channel = channel();
  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }
}
