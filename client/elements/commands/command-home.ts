import { render, html } from 'lit-html';
import { Element, NeonElement, Redraw } from '../../lib';
import { CommandList } from './command-list';

const template = (e: CommandHome) => html`
  ${e.creating
    ? html`
        <command-new @done=${() => e.refresh((e.creating = false))}></command-new>
      `
    : html`
        <button type="button" @click=${() => (e.creating = true)}>New Command</button>
      `}
  <hr />
  <command-list></command-list>
`;

@Element('command-home')
export class CommandHome extends NeonElement {
  @Redraw()
  public creating = false;

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public refresh(...any: any) {
    this.querySelector<CommandList>('command-list').refresh();
  }
}
