import { render, html } from 'lit-html';
import { Element, NeonElement, Attribute } from '../../lib';
import { contents } from '../../helpers/contents';

const template = (e: CategoryBox) => html`
  <title-area>${e.title}</title-area>
  <content-area>${e.contents}</content-area>
`;

@Element('category-box')
export class CategoryBox extends NeonElement {
  @Attribute()
  public title: string;
  public contents: Node[];

  public connectedCallback() {
    this.contents = contents(this);
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }
}
