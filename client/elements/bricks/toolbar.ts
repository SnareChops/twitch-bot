import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';

const template = (e: Toolbar) => html`
  <avatar>
    <img src="/assets/avatar.png" />
  </avatar>
  <toolbar-item @click=${() => Neon.navigate('home-')}>Home</toolbar-item>
  <toolbar-item @click=${() => Neon.navigate('command-home')}>Commands</toolbar-item>
  <toolbar-item @click=${() => Neon.navigate('config-home')}>Config</toolbar-item>
`;

@Element('toolbar-')
export class Toolbar extends NeonElement {
  public connectedCallback() {
    Neon.bind('Toolbar').toConstantValue(this);
  }

  public draw() {
    render(template(this), this);
  }

  public show() {
    this.draw();
  }

  public hide() {
    render(html``, this);
  }
}
