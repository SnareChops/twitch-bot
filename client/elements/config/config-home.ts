import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';

const template = (e: ConfigHome) => html`
  <fight-options></fight-options>
  <roll-options></roll-options>
  <url-scan-options></url-scan-options>
`;

@Element('config-home')
export class ConfigHome extends NeonElement {
  public async connectedCallback() {
    render(template(this), this);
  }
}
