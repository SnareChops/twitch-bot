import { render, html } from 'lit-html';
import { Element, NeonElement, onEvent } from '../../lib';
import { RollService, RollConfig } from '../../services/RollService';

const template = (e: RollOptions) => html`
  <category-box title="Roll Options">
    <section>
      <label>Roll Enabled</label>
      <check-box .value=${e.config.enabled} @change=${onEvent(value => e.save((e.config.enabled = value)))}></check-box>
    </section>
    <section>
      <label>Roll Cooldown:</label>
      <number-edit
        .value=${e.config.cooldown}
        @commit=${onEvent(cooldown => e.save((e.config.cooldown = cooldown || 0)))}
        hint="None"
        min="0"
      ></number-edit>
    </section>
  </category-box>
`;

@Element('roll-options')
export class RollOptions extends NeonElement {
  public config: RollConfig;
  public service = Neon.get<RollService>('RollService');

  public async connectedCallback() {
    try {
      this.config = await this.service.get();
      this.draw();
    } catch (e) {
      Neon.handleError(e);
    }
  }

  public draw() {
    render(template(this), this);
  }

  public async save(_?: any) {
    await this.service.update(this.config);
    this.draw();
  }
}
