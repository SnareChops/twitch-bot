import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';
import { channels, channel } from '../../helpers/channel';
import { Toolbar } from '../bricks/toolbar';

// prettier-ignore
const template = (e: ChooseChannel) => html`
  <h1>Choose channel to moderate</h1>
  <button-group>
    ${channels().map(channel => html`<button type="button" @click=${e.setChannel.bind(e, channel)}>${channel.replace('#', '')}</button>`)}
  </button-group>
`;

@Element('choose-channel')
export class ChooseChannel extends NeonElement {
  public get toolbar(): Toolbar {
    return Neon.get<Toolbar>('Toolbar');
  }

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public setChannel(name: string) {
    channel(name);
    this.toolbar.show();
    Neon.navigate('home-');
  }
}
