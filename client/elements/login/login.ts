import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';
import { LoginService } from '../../services/LoginService';

const template = (e: Login) => html`
  <login-box>
    <h2>Login with your Twitch account</h2>
    <button type="button" @click=${e.login.bind(e)}>Login</button>
  </login-box>
`;

@Element('login-')
export class Login extends NeonElement {
  private service = Neon.get<LoginService>('LoginService');

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public async login() {
    if (!!this.service.token) {
      try {
        if (!!(await this.service.validate())) {
          await this.service.login();
        }
      } catch (e) {
        Neon.handleError(e);
      }
    } else {
      await this.service.authorize();
    }
  }
}
