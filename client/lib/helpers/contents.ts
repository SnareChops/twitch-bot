export function contents(node: Node): Node[] {
  const nodes: Node[] = [];
  node = node.firstChild;
  while (!!node) {
    nodes.push(node);
    node = node.nextSibling;
  }
  return nodes;
}
