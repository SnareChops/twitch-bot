export function onEvent<T = any>(handler: (data: T) => any): (event: CustomEvent) => any {
  return ({ detail }: CustomEvent) => {
    handler(detail);
  };
}
