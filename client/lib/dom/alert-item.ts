import { render, html } from 'lit-html';
import { AlertType } from '../AlertType';
import { Element } from '../decorators/Element';
import { NeonElement } from '../elements/NeonElement';
import { Redraw } from '../decorators/Redraw';
import { OnSet } from '../decorators/OnSet';

const template = (e: AlertItem) => html`
  <alert-title>${e.title}</alert-title>
  <alert-message>${e.message}</alert-message>
  <neon-icon>X</neon-icon>
`;

@Element('alert-item')
export class AlertItem extends NeonElement {
  @Redraw()
  public title: string;
  @Redraw()
  public message: string;
  @Redraw()
  public type: AlertType;
  @OnSet('startTimer')
  public timeout: number;

  constructor() {
    super();
    this.addEventListener('click', () => this.remove());
  }

  public draw() {
    this.className = '';
    switch (this.type) {
      case AlertType.Success:
        this.classList.add('success');
        break;
      case AlertType.Error:
        this.classList.add('error');
        break;
      case AlertType.Warn:
        this.classList.add('warn');
        break;
      default:
        this.classList.add('info');
    }
    render(template(this), this);
  }

  public startTimer() {
    if (!!this.timeout) {
      setTimeout(() => this.remove(), this.timeout);
    }
  }
}
