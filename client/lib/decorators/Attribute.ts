import { registerGetSetProperty, appendGet, appendSet, getDecoratorData, ensureDecoratorData } from './helpers';
import { NeonElement } from '../elements/NeonElement';

export const Attributes = Symbol('Attributes');

export function Attribute(name?: string): PropertyDecorator {
  return (target: any, prop: string) => {
    const data = ensureDecoratorData<Map<string, string>>(target.constructor, Attributes, new Map());
    if (data.has(prop)) {
      throw new Error('A property can only be linked to one attribute.');
    }
    data.set(prop, name);
  };
}

export function registerAttributes(instance: NeonElement) {
  const attributes = getDecoratorData<Map<string, string>>(instance, Attributes);
  if (!attributes) {
    return;
  }
  [...attributes.entries()].forEach(([prop, name]) => {
    registerGetSetProperty(instance, prop);
    appendGet(instance, prop, () => instance.getAttribute(name || prop));
    appendSet(instance, prop, (value: any) => instance.setAttribute(name || prop, value));
  });
}
