import { registerGetSetProperty, appendSet, getDecoratorData, ensureDecoratorData } from './helpers';
import { NeonElement } from '../elements/NeonElement';

export const OnSets = Symbol('OnSets');

export function OnSet(...methods: string[]): PropertyDecorator {
  return (target: any, prop: string) => {
    const data = ensureDecoratorData<Map<string, string[]>>(target.constructor, OnSets, new Map());
    if (data.has(prop)) {
      throw new Error(
        "Only one @OnSet decorator can be used on a property. If you need to call more than one method use a single @OnSet decorator with multiple methods: @OnSet('method1', 'method2')"
      );
    }
    data.set(prop, methods);
  };
}

export function registerOnSets(instance: NeonElement) {
  const onSets = getDecoratorData<Map<string, string[]>>(instance, OnSets);
  if (!onSets) {
    return;
  }
  [...onSets.entries()].forEach(([prop, methods]) => {
    registerGetSetProperty(instance, prop);
    appendSet(
      instance,
      prop,
      ...methods.map(method => {
        return () => {
          setTimeout(() => instance[method](), 0);
        };
      })
    );
  });
}
