import { registerGetSetProperty, appendGet, appendSet, getDecoratorData, ensureDecoratorData } from './helpers';
import { NeonElement } from '../elements/NeonElement';

export const Classes = Symbol('Classes');

export function Class(name?: string): PropertyDecorator {
  return (target: any, prop: string) => {
    const data = ensureDecoratorData<Map<string, string>>(target.constructor, Classes, new Map());
    if (data.has(prop)) {
      throw new Error('A property can only be linked to one CSS class.');
    }
    data.set(prop, name);
  };
}

export function registerClasses(instance: NeonElement) {
  const classes = getDecoratorData<Map<string, string>>(instance, Classes);
  if (!classes) {
    return;
  }
  [...classes.entries()].forEach(([prop, name]) => {
    registerGetSetProperty(instance, prop);
    appendGet(instance, prop, () => instance.classList.contains(name || prop));
    appendSet(instance, prop, (value: boolean) =>
      value ? instance.classList.add(name || prop) : instance.classList.remove(name || prop)
    );
  });
}
