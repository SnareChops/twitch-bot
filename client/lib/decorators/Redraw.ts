import { registerGetSetProperty, appendSet, getDecoratorData, ensureDecoratorData } from './helpers';
import { NeonElement } from '../elements/NeonElement';

export const Redraws = Symbol('Redraws');

export function Redraw(): PropertyDecorator {
  return (target: any, prop: string) => {
    const data = ensureDecoratorData<string[]>(target.constructor, Redraws, []);
    data.push(prop);
  };
}

export function registerRedraws(instance: NeonElement) {
  const redraws = getDecoratorData<string[]>(instance, Redraws);
  if (!redraws) {
    return;
  }
  redraws.forEach(prop => {
    registerGetSetProperty(instance, prop);
    appendSet(instance, prop, () => (!!instance['draw'] ? setTimeout(() => instance['draw'](), 0) : void 0));
  });
}
