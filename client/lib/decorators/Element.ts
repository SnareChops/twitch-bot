import { NavigationGuard } from '../types';

export function Element(tag: string, guard?: NavigationGuard): ClassDecorator;
export function Element(tag: string, options?: ElementDefinitionOptions, guard?: NavigationGuard): ClassDecorator;
export function Element(
  tag: string,
  options?: ElementDefinitionOptions | NavigationGuard,
  guard?: NavigationGuard
): ClassDecorator {
  if (typeof options === 'function') {
    guard = options;
    options = void 0;
  }
  return (target: Function) => {
    if (!customElements.get(tag)) {
      customElements.define(tag, target, options as ElementDefinitionOptions);
    }
    if (!!guard) {
      Neon.get<Map<string, NavigationGuard>>('NAVIGATION_GUARDS').set(tag, guard);
    }
  };
}
