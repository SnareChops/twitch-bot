export function subscribe(event: string, handler: (detail: any) => any): { unsubscribe(): void } {
  function internal(e: CustomEvent) {
    handler(e.detail);
  }
  document.addEventListener(event, internal);
  return { unsubscribe: () => document.removeEventListener(event, internal) };
}

export function emit(event: string, data: any): void {
  document.dispatchEvent(new CustomEvent(event, { detail: data }));
}

export function once(event: string, handler: (detail: any) => any): { unsubscribe(): void } {
  function internal(e: CustomEvent) {
    handler(e.detail);
    document.removeEventListener(event, internal);
  }
  document.addEventListener(event, internal);
  return { unsubscribe: () => document.removeEventListener(event, internal) };
}
