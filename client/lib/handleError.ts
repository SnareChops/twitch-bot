export interface MessageTokens {
  default: string;
  [key: string]: string;
}

function getToken(message: any, tokens?: string | MessageTokens): string {
  if (!tokens) {
    return;
  }
  if (typeof tokens === 'string') {
    return tokens;
  }
  const def = !!tokens && !!tokens.default ? tokens.default : void 0;
  return (typeof message === 'string' ? tokens[message] : def) || def;
}

function handleString(error: string, tokens?: string | MessageTokens): string {
  const token = getToken(error, tokens);
  return Neon.alert.error(!!token ? token : error);
}

function _handleError(error: Error, tokens?: string | MessageTokens): string {
  const token = getToken(error.message, tokens);
  return Neon.alert.error(!!token ? token : error.message);
}

function handleUnknown(error: any, tokens?: string | MessageTokens): string {
  const inferred = inferError(error);
  const token = getToken(inferred, tokens);
  return Neon.alert.error(!!token ? token : inferred);
}

function inferError(error: any): string {
  if (typeof error.message === 'object') {
    if (error.message.error && error.message.error === 'wamp.error.runtime_error') {
      return error.message.args[0];
    }
    return JSON.stringify(error.message);
  }
  if ('message' in error) {
    return error['message'].toString();
  }
  return JSON.stringify(error);
}

export function handleError(error: any, messages?: string | MessageTokens): string {
  Neon.logger.error(error);
  if (typeof error === 'string' || typeof error === 'number' || typeof error === 'boolean') {
    return handleString(error.toString(), messages);
  } else if (error instanceof Error) {
    return _handleError(error, messages);
  } else {
    return handleUnknown(error, messages);
  }
}
