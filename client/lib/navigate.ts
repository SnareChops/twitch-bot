import { NavigationArgs, NavigationGuard } from './types';

export function navigate(element: string, args: NavigationArgs = {}): void {
  if (check(element, args)) {
    const attrs = Object.keys(args)
      .map(attr => `${attr}=${encodeURIComponent(args[attr].toString())}`)
      .join('&');
    location.hash = `#${element}${!!attrs ? `?${attrs}` : ''}`;
  }
}

function check(element: string, args: NavigationArgs): boolean {
  if (!Neon.bound('NAVIGATION_GUARDS')) {
    Neon.bind('NAVIGATION_GUARDS').toConstantValue(new Map<string, NavigationGuard>());
  }
  const map = Neon.get('NAVIGATION_GUARDS') as Map<string, NavigationGuard>;
  return map.has(element) ? map.get(element)(element, args) : true;
}
