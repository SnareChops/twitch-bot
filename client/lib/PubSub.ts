import * as EventEmitter from './EventEmitter';

export interface PubSub<T> {
  publish(data?: T): void;
  subscribe(handler: (data: T) => any): { unsubscribe(): void };
}
export function pubSub<T>(name: string): PubSub<T> {
  return {
    publish: (data?: T) => EventEmitter.emit(name, data),
    subscribe: (handler: (data: T) => any): { unsubscribe(): void } => {
      return EventEmitter.subscribe(name, handler as any);
    }
  };
}
