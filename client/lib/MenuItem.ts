import { NavigationArgs } from './types';

export type MenuGuard = (item: MenuItem) => boolean;
export type MenuCallback<T> = (item: MenuItem) => T;

export class MenuItem<T = void> {
  public name: string;
  public textToken: string;
  public icon?: string;
  public weight?: number = 0;
  public badge?: number;
  public guard?: MenuGuard;
  public element?: string;
  public args?: NavigationArgs;
  public callback?: MenuCallback<T>;

  constructor(
    name: string,
    textToken: string,
    element: string,
    icon?: string,
    weight?: number,
    guard?: MenuGuard,
    badge?: number
  );
  constructor(
    name: string,
    textToken: string,
    element: string,
    args: NavigationArgs,
    icon?: string,
    weight?: number,
    guard?: MenuGuard,
    badge?: number
  );
  constructor(
    name: string,
    textToken: string,
    // tslint:disable-next-line
    callback: MenuCallback<T>,
    icon?: string,
    weight?: number,
    guard?: MenuGuard,
    badge?: number
  );
  constructor(
    name: string,
    textToken: string,
    elementOrCallback: string | MenuCallback<T>,
    iconOrArgs?: string | NavigationArgs,
    iconOrWeight?: string | number,
    guardOrWeight?: number | MenuGuard,
    badgeOrGuard?: number | MenuGuard,
    badge?: number
  ) {
    this.name = name;
    this.textToken = textToken;
    typeof elementOrCallback === 'string' ? (this.element = elementOrCallback) : (this.callback = elementOrCallback);
    typeof iconOrArgs === 'string' ? (this.icon = iconOrArgs) : (this.args = iconOrArgs);
    typeof iconOrWeight === 'string' ? (this.icon = iconOrWeight) : (this.weight = iconOrWeight);
    typeof guardOrWeight === 'number' ? (this.weight = guardOrWeight) : (this.guard = guardOrWeight);
    typeof badgeOrGuard === 'number' ? (this.badge = badgeOrGuard) : (this.guard = badgeOrGuard);
    if (!!badge) {
      this.badge = badge;
    }
    if (this.weight === undefined) {
      this.weight = 0;
    }
  }

  public handle(): T | void {
    if (!!this.element) {
      return Neon.navigate(this.element, this.args);
    }
    if (!!this.callback) {
      return this.callback(this);
    }
  }
}
