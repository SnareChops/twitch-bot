import { Container, interfaces } from 'inversify';
import { error as alertError, warn as alertWarn, info as alertInfo, success as alertSuccess } from './Alert';
import { error as logError, warn as logWarn, info as logInfo, debug as logDebug, toConsole } from './Logger';
import * as EventEmitter from './EventEmitter';
import * as popup from './popup/Popup';
import { pubSub } from './PubSub';
import { handleError } from './handleError';
import { navigate } from './navigate';

const container = new Container();

function get<T = any>(token: string, name?: string): T {
  return !!name ? container.getNamed<T>(token, name) : container.get<T>(token);
}

function bind<T = any>(token: string): interfaces.BindingToSyntax<T> {
  return container.bind<T>(token);
}

function bound(token: string, name?: string): boolean {
  return !!name ? container.isBoundNamed(token, name) : container.isBound(token);
}

function unbind(token: string): void {
  return container.unbind(token);
}

function unbindAll(): void {
  return container.unbindAll();
}

function href(url: string): void {
  location.href = url;
}

function back(): void {
  history.back();
}

window.Neon = {
  get,
  bind,
  bound,
  unbind,
  unbindAll,
  container,
  navigate,
  pubSub,
  alert: {
    error: alertError,
    warn: alertWarn,
    info: alertInfo,
    success: alertSuccess
  },
  logger: {
    error: logError,
    warn: logWarn,
    info: logInfo,
    debug: logDebug
  },
  popup,
  handleError,
  href,
  back,
  emit: EventEmitter.emit,
  listen: EventEmitter.subscribe,
  once: EventEmitter.once,
  logs: toConsole
};
