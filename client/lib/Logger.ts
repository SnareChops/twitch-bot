export enum LogLevel {
  DEBUG,
  INFO,
  WARN,
  ERROR
}
export interface LogItem {
  level: LogLevel;
  items: any[];
}
let logs: LogItem[] = [];

export function debug(...args: any[]): any {
  return store(LogLevel.DEBUG, args);
}

export function info(...args: any[]): any {
  return store(LogLevel.INFO, args);
}

export function warn(...args: any[]): any {
  return store(LogLevel.WARN, args);
}

export function error(...args: any[]): any {
  return store(LogLevel.ERROR, args);
}

function store(level: LogLevel, items: any[]): any {
  const item = { level, items };
  logs.push(item);
  return items.length > 1 ? items : items[0];
}

export function toConsole(items: LogItem[] = logs): void {
  let args: any[];
  for (const logItem of items) {
    args = logItem.items.splice(0);
    args.splice(0, 0, `${LogLevel[logItem.level]}:`);
    if (logItem.level === LogLevel.WARN) {
      console.warn.apply(console, args);
      continue;
    }
    if (logItem.level === LogLevel.ERROR) {
      console.error.apply(console, args);
      continue;
    }
    // tslint:disable-next-line
    console.log.apply(console, args);
  }
  logs = [];
}
window['logs'] = toConsole;
