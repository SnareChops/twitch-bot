export type PopupCompleter<T> = (result?: T) => void;
export type PopupCanceler<T> = (result?: T) => void;

export class PopupResult<CompleteResult extends any, CancelResult extends any> {
  private completeHandlers: ((result: CompleteResult) => any)[] = [];
  private cancelHandlers: ((result: CancelResult) => any)[] = [];

  constructor(handlers: (completer: PopupCompleter<CompleteResult>, canceler: PopupCanceler<CancelResult>) => void) {
    const complete = (result?: CompleteResult) => {
      this.completeHandlers.forEach(handler => {
        handler(result);
      });
      this.completeHandlers = [];
    };

    const cancel = (result?: CancelResult) => {
      this.cancelHandlers.forEach(handler => {
        handler(result);
      });
      this.cancelHandlers = [];
    };

    setTimeout(() => handlers(complete, cancel), 0);
  }

  public complete(handler: (result?: CompleteResult) => any): this {
    this.completeHandlers.push(handler);
    return this;
  }

  public cancel(handler: (result?: CancelResult) => any): this {
    this.cancelHandlers.push(handler);
    return this;
  }
}
