import { render, html } from 'lit-html';
import { Element, NeonElement, PopupElement, PopupOptions, PopupResult } from '../index';

const template = (e: PopupContainer) => html`
  <popup-backdrop class=${e.visible ? 'visible' : 'none'} @click=${() => e.backdropClick()}></popup-backdrop>
  <popup-content> ${e.element} </popup-content>
`;

@Element('popup-container')
export class PopupContainer extends NeonElement {
  public visible = false;
  public options: PopupOptions;
  public element: PopupElement;

  public complete: (result?: any) => void;
  public cancel: (result?: any) => void;

  constructor() {
    super();
    Neon.bind('PopupContainer').toConstantValue(this);
  }

  public draw() {
    render(template(this), this);
  }

  public show<EType extends PopupElement, CompleteResult = any, CancelResult = any>(
    element: string,
    args: { [T in keyof EType]?: EType[T] } = {},
    options: PopupOptions = {}
  ): PopupResult<CompleteResult, CancelResult> {
    this.options = this.parseOptions(options);
    return new PopupResult<CompleteResult, CancelResult>((completer, canceler) => {
      this.visible = true;
      this.style.display = 'block';
      this.element = (document.createElement(element) as any) as EType;
      Object.keys(args).forEach(key => (this.element[key] = args[key]));
      this.complete = result => {
        this.close(false);
        completer(result);
      };
      this.cancel = result => {
        this.close(false);
        canceler(result);
      };
      this.draw();
    });
  }

  public backdropClick() {
    if (this.options.cancelOnBackgroundClick) {
      this.close();
    }
  }

  public close(cancel: boolean = true) {
    this.visible = false;
    this.style.display = 'none';
    if (cancel) {
      this.cancel(void 0);
    }
    this.complete = void 0;
    this.cancel = void 0;
    this.element = void 0;
    this.draw();
  }

  private parseOptions(options: PopupOptions): PopupOptions {
    if (options.cancelOnBackgroundClick === void 0) {
      options.cancelOnBackgroundClick = true;
    }
    return options;
  }
}
