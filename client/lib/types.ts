export interface NavigationArgs {
  [key: string]: string | number | boolean;
}

export type NavigationGuard = (element: string, args: NavigationArgs) => boolean;
