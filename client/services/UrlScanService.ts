import { UrlScan } from '../../src/entities/UrlScan';
import { channel } from '../helpers/channel';
import { WampClient } from './WampClient';
import { injectable } from 'inversify';

@injectable()
export class UrlScanService {
  private wampClient = Neon.get<WampClient>('WampClient');

  public async get(): Promise<UrlScan> {
    return this.wampClient.call('public.urlscan.getByChannel', [channel()]);
  }

  public async update(config: UrlScan): Promise<UrlScan> {
    return this.wampClient.call('public.urlscan.update', [config]);
  }
}
