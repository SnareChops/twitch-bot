// import { injectable, inject } from 'inversify';
// import { WampClient } from './WampClient';
// import { Config } from '../../src/entities';
// import { channel } from '../helpers/channel';

// @injectable()
// export class ConfigService {
//   constructor(@inject('WampClient') private wampClient: WampClient) {}

//   public async get(): Promise<Config> {
//     return this.wampClient.call('public.config.getByChannel', [channel()]);
//   }

//   public async update(config: Config): Promise<Config> {
//     return this.wampClient.call('public.config.update', [config]);
//   }
// }
