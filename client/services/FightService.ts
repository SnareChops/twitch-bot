import { injectable, inject } from 'inversify';
import { WampClient } from './WampClient';
import { channel } from '../helpers/channel';

export interface FightConfig {
  channel: string;
  enabled: boolean;
  cooldown: number;
}

@injectable()
export class FightService {
  constructor(@inject('WampClient') private wampClient: WampClient) {}

  public async get(): Promise<FightConfig> {
    return this.wampClient.call('public.fight.config.getByChannel', [channel()]);
  }

  public async update(config: FightConfig): Promise<FightConfig> {
    return this.wampClient.call('public.fight.config.update', [config]);
  }
}
