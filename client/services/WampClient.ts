import { injectable } from 'inversify';
import { WampClient as _WampClient } from 'mumba-wamp';

@injectable()
export class WampClient extends _WampClient {
  public onConnectionStatusChange = Neon.pubSub<boolean>('WampClient:onConnectionStatusChange');
  public hasConnectionBeenLost = false;
  private connectionCallbacks: [Function, Function];

  constructor() {
    super();
    this.onOpen(() => {
      this.hasConnectionBeenLost = false;
      this.onConnectionStatusChange.publish(true);
      if (!!this.connectionCallbacks && !!this.connectionCallbacks[0]) {
        this.connectionCallbacks[0](this.getAuthExtra());
      }
      Neon.alert.success('Authenticated socket connection open');
    });

    this.onClose((details: any) => {
      if (!details.wasClean) {
        this.hasConnectionBeenLost = true;
      }
      this.onConnectionStatusChange.publish(false);
      if (!!this.connectionCallbacks && !!this.connectionCallbacks[1]) {
        this.connectionCallbacks[1](this.getAuthExtra());
      }
    });
  }

  public async connect(authid?: string, challenge?: string, extras?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.setOptions({
        url: `wss://${location.hostname}/ws`,
        realm: 'chatbot',
        authmethods: ['ticket'],
        authid,
        authextra: extras,
        onchallenge: () => challenge
      });
      this.connectionCallbacks = [resolve, reject];
      super.openConnection();
    });
  }

  public getExtras<T = any>(): T {
    return (this.getAuthExtra() as unknown) as T;
  }
}
