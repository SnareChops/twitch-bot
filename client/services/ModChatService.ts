import { inject, injectable } from 'inversify';
import { WampClient } from './WampClient';
import { channel } from '../helpers/channel';
import { ModChatMessage } from '../../src/entities/ModChatMessage';

export type ModChatMessageHandler = (message: ModChatMessage) => any;

@injectable()
export class ModChatService {
  private onMessages: ModChatMessageHandler[] = [];

  constructor(@inject('WampClient') private wampClient: WampClient) {
    this.wampClient.subscribe(`public.modchat.message.${channel().replace('#', '')}`, ([message]: [ModChatMessage]) =>
      this.onMessages.forEach(x => x(message))
    );
  }
  public onMessage(handler: ModChatMessageHandler) {
    this.onMessages.push(handler);
  }

  public async history(offset: number = 0, limit: number = 50): Promise<ModChatMessage[]> {
    return this.wampClient.call('public.modchat.history', [channel(), offset, limit]);
  }

  public async send(message: string): Promise<ModChatMessage> {
    return this.wampClient.call('public.modchat.send', [
      new ModChatMessage(channel(), message, this.wampClient.getAuthId())
    ]);
  }
}
