import { injectable, inject } from 'inversify';
import { client_id, redirect_uri } from '../config';
import { WampClient } from './WampClient';
import { channels, channel } from '../helpers/channel';
import { Toolbar } from '../elements/bricks/toolbar';

const state = 'kjhsdf76234GFUH3947734sf';

@injectable()
export class LoginService {
  public get token(): string {
    return localStorage.getItem('access_token');
  }

  public set token(token: string) {
    if (!token) {
      localStorage.removeItem('access_token');
    }
    localStorage.setItem('access_token', token);
  }

  public get state(): string {
    return localStorage.getItem('login_state');
  }

  public set state(value: string) {
    if (!value) {
      localStorage.removeItem('login_state');
    }
    localStorage.setItem('login_state', value);
  }

  public get toolbar(): Toolbar {
    return Neon.get<Toolbar>('Toolbar');
  }

  constructor(@inject('WampClient') private socket: WampClient) {}

  public async validate(): Promise<boolean> {
    return fetch('https://id.twitch.tv/oauth2/validate', { headers: { Authorization: `OAuth ${this.token}` } }).then(
      res => {
        if (!res.ok) {
          throw new Error('Invalid access token');
        }
        return res.json().then(x => x.login);
      }
    );
  }

  public authorize() {
    location.href = `https://id.twitch.tv/oauth2/authorize?client_id=${client_id}&redirect_uri=${encodeURIComponent(
      redirect_uri
    )}&response_type=token&scope=&state=${state}`;
    this.state = state;
  }

  public async login(token: string = this.token, state: string = this.state) {
    if (state !== this.state) {
      throw new Error('State Mismatch');
    }

    this.token = token;
    const extras = await this.socket.connect('access_token', this.token);
    channels(extras.channels);
    if (channels().length === 1) {
      channel(channels()[0]);
      this.toolbar.show();
      Neon.navigate('home-');
    } else {
      Neon.navigate('choose-channel');
    }
  }
}
