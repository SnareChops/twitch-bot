import { injectable, inject } from 'inversify';
import { WampClient } from './WampClient';
import { channel } from '../helpers/channel';

export interface RollConfig {
  channel: string;
  enabled: boolean;
  cooldown: number;
}

@injectable()
export class RollService {
  constructor(@inject('WampClient') private wampClient: WampClient) {}

  public async get(): Promise<RollConfig> {
    return this.wampClient.call('public.roll.config.getByChannel', [channel()]);
  }

  public async update(config: RollConfig): Promise<RollConfig> {
    return this.wampClient.call('public.roll.config.update', [config]);
  }
}
