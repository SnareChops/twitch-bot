import { CommandService } from './CommandService';
import { LoginService } from './LoginService';
import { ModChatService } from './ModChatService';
import { FightService } from './FightService';
import { RollService } from './RollService';
import { UrlScanService } from './UrlScanService';

Neon.bind('CommandService').to(CommandService);
Neon.bind('LoginService').to(LoginService);
Neon.bind('ModChatService').to(ModChatService);
Neon.bind('FightService').to(FightService);
Neon.bind('RollService').to(RollService);
Neon.bind('UrlScanService').to(UrlScanService);
